<?php
namespace Models;

class User extends Model{

    public function __construct($request = null){
        parent::__construct($request);
        /**
         * todo добавить общую соль
         * клиенту отдавать в ответе на вход хэшированный этой солью пароль - минимум защиты
         * */
    }

    public function register( $name, $hash, $salt, $time ){
        $sql = "insert into tbl_users (u_login, u_pass, u_salt, u_date_created)
            values (?s, ?s, ?s, ?s)";
        $this->connect->query( $sql, $name, $hash, $salt, $time );
    }

    public function getUser( $name, $hash ){
        $sql = "select u_id as uid, u_login as name, ifnull(u_win, 0) as win, ifnull(u_lose, 0) as lose, 
ifnull(u_hint, 0) as hint from tbl_users where u_login = ?s AND u_pass=?s";
        return $this->connect->getRow( $sql, $name, $hash );
    }
    /**
     *
     * @param $name string
     * @param $hash string
     *
     * @return bool
     * */
    public function checkUser( $name, $hash ){
        $sql = "select u_id as uid from tbl_users where u_login = ?s AND u_pass=?s";
        return (bool)$this->connect->getOne( $sql, $name, $hash );
    }

    public function getSalt( $username ){
        $sql = "select u_salt from tbl_users where u_login = ?s";
        return $this->connect->getOne($sql, $username);
    }

    public function isExistUser( $username ){
        $sql = "select u_login as name from tbl_users where u_login = ?s";
        return $this->connect->getOne( $sql, $username );
    }

//    хэширует пароль, возврашает хэш и соль
    public function passwordHash($password, $salt = null, $iterations = 10){
        $salt || $salt = uniqid();
        $hash = md5(md5($password . md5(sha1( $salt ))));
        for ($i = 0; $i < $iterations; ++$i) {
            $hash = md5(md5(sha1($hash)));
        }
        return array('hash' => $hash, 'salt' => $salt);
    }
    // удалим пользователя из tbl_users_online
    public function logout($uid){
        $sql = "delete from tbl_users_online where uol_uid=?s";
        return $this->connect->query($sql, $uid);
    }
}

