<?php
namespace Models;
class Model
{
    public $config;
    public $connect;

    public function __construct( $db_cnf, $request = null ){
        $this->config = $db_cnf;
        $this->connect = $this->connect();
    }

    private function connect(){
        return new SafeMySQL( $this->config );
    }
// валидация данных в запросе
    public function getLimits( $formName ){
        $sql = "SELECT tfl_name as name, tfl_regexp as regex, tfl_min_len as min, tfl_max_len as max 
FROM tbl_fields_limit where tfl_form =?s";
        return $this->connect->getAll($sql, $formName);
    }

// ответ на запрос ограничений на поля, в данном случае исп-ся на клиенте только для отображения в подсказке
    public function getAllLimits(){
        $sql = "SELECT tfl_name as name, tfl_form as form, tfl_min_len as min, tfl_max_len as max,
tfl_regexp as regex, tfl_caption as caption FROM tbl_fields_limit";
        return $this->connect->getAll( $sql );
    }

    /**
     * вернет настройки пользователя
     *
     * @param $uid int
     *
     * @return array
     *
     * приведение типов в классе не работает
     * select cast(cfg_id as unsigned) as uid, cast(cfg_wlog as unsigned) as wlog, cast(cfg_users as unsigned) as ulist from tbl_cfg where cfg_id=297
     *
     * */
    public function _getConf_( $uid ){
        $sql = "select cfg_wlog as wlog, cfg_users as uslist from tbl_cfg where cfg_id=?i";
        return $this->connect->getRow( $sql, $uid );
    }

    /**
     * обновляет запись в tbl_users_online
     *
     * @param $uid int
     * @param $time
     *
     * @return bool
     */
    public function updOnLine($uid, $time){
        $sql = "update tbl_users_online set uol_time=?s where uol_uid=?i";
        // now() в классе не работает -?
//        $sql = "insert into tbl_users_online (uol_uid, uol_time)
//values (?i, now()) on duplicate key update uol_uid=uol_uid, uol_time=now()";
        return $this->connect->query($sql, $time, $uid);
    }

    /**
     * вносит запись в tbl_users_online
     *
     * @param $uid int
     * @param $time
     *
     * @return bool
     */
    public function setOnLine($uid, $time){
        $sql = "insert into tbl_users_online (uol_uid, uol_time)
values (?i, ?s) on duplicate key update uol_uid=uol_uid, uol_time=?s";
        return $this->connect->query($sql, $uid, $time, $time);
    }

    /**
     * вернет список users online from tbl_users_online
     *
     * @return array
     *
     * */
    public function getAllUsersOnLine(){
        $sql = "select u_login as uname from tbl_users, tbl_users_online where u_id=uol_uid order by uname";
        return $this->connect->getAll($sql);
    }
    /**
     * отвечает на вопрос are users online? from tbl_users_online
     *
     * @param $uid int
     * @return bool
     *
     * */
    public function isUserOnLine($uid){
        $sql = "select uol_uid from tbl_users_online where uol_uid=?i";
        return $this->connect->getOne($sql, $uid);
    }
    /**
     * проверка наличия колонки в таблице
     *
     * @param $db string
     * @param $tbl string
     * @param $fld string
     * @return bool
     *
     * */
    public function isColumnExist( $db, $tbl, $fld ){
        $sql = "SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema=?s and 
TABLE_NAME=?s and Column_Name=?s";
        return $this->connect->getOne($sql, $db, $tbl, $fld);
    }
}
