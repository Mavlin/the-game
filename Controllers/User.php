<?php
namespace Controllers;
use Models;

class User extends Controller
{
    public $prefix = 'user';
    protected $User;

    public function __construct( $request = null ){
        parent::setCfg();
        $this->User = new Models\User( $this->config );
        parent::__construct( $request );
    }

    /**
     * авторизация
    */
    protected function _login_(){
        $this->getRequest($this->request[(string)__FUNCTION__]['data']);
        $username = $this->getRequestParam($this->request['form'],"username");
        $password = $this->getRequestParam($this->request['form'],"password");

        $salt = $this->User->getSalt( $username );
        $hash = $this->User->passwordHash( $password, $salt )[ 'hash' ];

        $user = $this->User->getUser( $username, $hash );
        if ( !$user ){
            $rc = $this->WrongUserName;
            throw new \Exception( $rc['message'], $rc['code'] );
        }
        $this->setOnLine( $user['uid'] );
        $user['pass'] = $password; // добавим в массив, что отдается клиенту
        $rc = $this->success;
        $this->setResponse('user', $user);
        $this->setResponse('status', "ok");
        $this->setResponse('code', $rc['code']);
        $this->encodeResponse();
        $this->showResponse();
    }

    // зарегистрируемся в on-line
    public function setOnLine($uid){
        $time = (new myDateTime())->formatDB();
        $this->User->setOnLine($uid, $time);
    }

    // удалим пользователя из списка онлайн
    protected function _logout_(){
        $this->getRequest($this->request[(string)__FUNCTION__]['data']);
        $jwt = $this->getRequestParam( $this->request, "jwt" );
        $uid = $this->getRequestParam($jwt, "id");
        $userCheck = $this->isAuthorizedUser( $jwt );
        if (!$userCheck) {
            $rc = $this->userNotAuthorized;
            throw new \Exception($rc['message'], $rc['code']);
        }
        $gm = new Game( $this->config );
        usleep($gm::INTERVAL * 2 );
        $result = $this->User->logout( $uid );
        // выходим и из лобби

        if ( $result ){
            $userData = $this->getRequestParam( $this->request, 'userCfg' );
            $gm->saveCfg( $uid, $userData );
            $lobby = new Lobby( $this->config );
            $gmMod = new Models\Game( $this->config );
            $lid = $gmMod->getLobbyId( $uid );
            if ( $lid ){
                $lobby->leave( $uid, $lid );
            }
            $rc = $this->sseOff;
            $this->setResponse('code', $rc['code']);
        }
        $this->setResponse('status', "ok");
        $this->encodeResponse();
        $this->showResponse();
    }

    /*
     * метод исп-ся другими классами
     * запросим инд соль пользователя из БД (login) вернет соль
     * получим хэш (login, salt) вернет массив [hash, salt]
     * выберем строку из таблицы пользователей (login, hash) вернет скаляр
     * вернем (true или исключение)
     * */
    public function isAuthorizedUser( $jwt ){
        $login = $this->getRequestParam($jwt, "login");
        $pass = $this->getRequestParam($jwt, "pass");
        $salt = $this->User->getSalt( $login );
        $hash = $this->User->passwordHash( $pass, $salt )['hash'];
        if ( !$this->User->checkUser( $login, $hash ) ){
            $rc = $this->WrongUserName;
            throw new \Exception( $rc['message'], $rc['code'] );
        }
        return true;
    }

    /**
     * регистрация
     */
    protected function _register_(){
        $this->getRequest($this->request[(string)__FUNCTION__]['data']);
        $username = $this->getRequestParam($this->request['form'],"username");
        $password1 = $this->getRequestParam($this->request['form'],"password1");
        $password2 = $this->getRequestParam($this->request['form'],"password2");
        if ($password1 !== $password2) {
            $rc = $this->PassNotMatch;
            throw new \Exception( $rc['message'], $rc['code'] );
        }
        $this->request[ 'password' ] = $this->request['form'][ 'password1' ];
        $this->validateData( $this->prefix, $this->request['form'] );
        if ( $this->User->isExistUser( $username )){
            $rc = $this->UserExist;
            throw new \Exception( $rc['message'], $rc['code']);
        }
        $tm = new MyDateTime();
        $time = $tm->formatDB();
        $hashes = $this->User->passwordHash( $password1 ); // готовим хеши
        $this->User->register( $username, $hashes['hash'], $hashes['salt'], $time );
        $this->setResponse('data', sprintf ("Hello, %s!</br> 
                Registration is over, you can log in now.", $username));
        $rc = $this->userRegister;
        $this->setResponse( "code", $rc['code'] );
        $this->setResponse( 'status', "ok" );
        $this->encodeResponse();
        $this->showResponse();
    }
}

