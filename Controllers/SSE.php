<?php
namespace Controllers;
/**
 * класс для Server Side Events
 */
class SSE extends Controller
{
    protected $header = "Content-Type: text/event-stream; charset=UTF-8; Cache-Control: no-cache";
    public $retry = 10; // Задержка переподключения для клиента, мсек
    public function __construct( $request = null ){
        header( $this->header );
        parent::__construct( null );
    }

    public function off(){
        $event = 'response';
        $rc = $this->sseOff;
        $data = [
            'code' => $rc[ 'code' ],
            'message' => $rc[ 'message' ]
        ];
        $this->setResponseSSE( $event, $data );
        $this->showResponseSSE();
    }

    public function err( $e )
    {
        $event = 'response';
        if ( $e ){
            $rc = $this->sseOff;
            $data = [
                'code' => $rc['code'],
                'message' => $rc['message'],
                'data' => $e
            ];
            $this->setResponseSSE($event, $data);
            $this->showResponseSSE();
        }
    }

    /**
     *
     * @param $response array
     *
     * */
    public function run( $response ){
        $response['AppState'] = 'on-line';
        $event = 'response';
        $this->setResponseSSE( $event, $response );
        $this->showResponseSSE();
    }

    protected function setResponseSSE( $event, $data ){
        $data = json_encode( $data );
        $this->response = "event:$event" . PHP_EOL . "retry:$this->retry" . PHP_EOL . "data:$data" . PHP_EOL . PHP_EOL;
    }

    public function showResponseSSE(){
        echo $this->response;
        ob_flush();
        flush();
    }
}
