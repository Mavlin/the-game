<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/SplClassLoader.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/handler.php';
if ( $_SERVER['REMOTE_ADDR'] !== '127.0.0.1' ){
    ini_set('display_errors','off');
}
$req = null; $query = null; $arg = [];
if ( array_key_exists('req', $_GET )){
    $req = $_GET['req'];
} else {
    $req = $_POST['req'];
}
$query = json_decode($req, true );
$ns = 'Controllers';

$data = []; // массив данных для передачи в целевой класс
foreach ($query as $key => $value){
    if ( $key == 'where' || $key == 'data' )
        $data[$key] = $value;
}
$arg['_'.$query['mode'].'_'] = $data; // искомый метод в классе
if (array_key_exists( 'jwt', $query )) $arg['jwt'] = $query['jwt']; // токен авторизации
$full_path = $ns . '\\' . $query['select'];
$ajaxRequest = new $full_path( $arg );
