<?php
namespace Controllers;

class Lobby extends Controller
{
    public $prefix = 'lobby'; // for validate metod
    protected $gc; // объект Controllers/Game
    const PENALTY = '1M'; // one minute

    public function __construct($request = null){
        if ($request !== null) {
            parent::__construct($request);
        }
    }

    /**
     * Создать лобби:
     * - проверяем наличие лобби с таким же именем
     * - если нет, то
     * - авторизуемся и создаем
     *
     */
    public function _create_(){

        $this->getRequest($this->request[(string)__FUNCTION__]['data']);
        $jwt = $this->getRequestParam( $this->request, "jwt" );
        $ln = $this->getRequestParam( $this->request['form'], "name" );
        $lt = $this->getRequestParam( $this->request['form'], "timer" );
        $uid = $this->getRequestParam($jwt, "id");
        $this->gc = new Game( $this->config );
        $lm = new \Models\Lobby( $this->config );
        $result = false;
        $this->validateData( $this->prefix, $this->request['form'] );
        $timer = $this->request['form']['timer'];
        if ($timer<5 || $timer>60){
            $rc = $this->dataNotValid( 'timer' );
            throw new \Exception ($rc['message'], $rc['code']);
        }
        $lobbyCheck = $lm->isExistLobby( $ln );
        if ($lobbyCheck) {
            $rc = $this->LobbyExist;
            throw new \Exception( $rc['message'] , $rc['code'] );
        }
        $userCheck = (new User())->isAuthorizedUser( $jwt );
        if ($userCheck) {
            $result = $lm->create( $uid, $ln, $lt );
        }
        if ( $result ){
            // войдем в лобби сразу после создания, это логично, да и удобно
            $this->select($uid, $result);
            $rc = $this->success;
        } else {
            $data = null;
            $rc = $this->SomethingWrong; // 41
        }
//        $data = $this->gc->getGameInAjax( $this->request );
//        $this->setResponse('data', $data);
        $this->setResponse('status', "ok");
        $this->setResponse('code', $rc['code']);
        $this->encodeResponse();
        $this->showResponse();
    }

    /**
     * Выбрать лобби:
     * - проверяем наличие запрошенного лобби
     * - проверяем кол-во посетителей лобби
     * - проверяем не присутствует ли пользователь уже в лобби
     * - если менее 2-х, то
     * - авторизуемся, входим
     * добавляем запись в tbl_games, устанавливаем статус в 0 для автора
     *
     */

    public function _select_(){

        $this->getRequest($this->request[(string)__FUNCTION__]['data']);
        $jwt = $this->getRequestParam( $this->request, "jwt" );
        $lid = $this->getRequestParam( $this->request, "id" );
        $uid = $this->getRequestParam($jwt, "id");
        $result = false;
        $this->gc = new Game( $this->config );
        $userCheck = (new User())->isAuthorizedUser( $jwt );
        if ($userCheck) {
            $result = $this->select( $uid, $lid);
        }
        if ( $result ){
            (new User($this->config))->setOnLine( $uid );
            $rc = $this->success;
            // сохраним пользовательские настройки
            $userData = $this->getRequestParam( $this->request, 'userCfg' );
            $this->gc->saveCfg( $uid, $userData );
        } else {
            $rc = $this->SomethingWrong; // 41
        }
//        $data = $this->gc->getGameInAjax( $this->request );
        $this->setResponse('status', "ok");
        $this->setResponse('code', $rc['code']);
//        $this->setResponse('data', $data);
        $this->encodeResponse();
        $this->showResponse();
    }

    public function select($uid, $lid){

        $gm = new \Models\Game( $this->config );
        $lm = new \Models\Lobby( $this->config );
        $tm = new MyDateTime();
        $tp = $lm->getPenalty($uid);
        if ( $tp ){
            $penalty = new MyDateTime($tp);
        }
        if (isset($penalty) && $tm < $penalty){
            $interval = ($penalty->diff($tm))->format('%i min %s sec');
            $rc = $this->penaltyNotExpire;
            throw new \Exception('penalty to finish through<br>'.$interval, $rc['code']);
        }
        $lobbyCheck = $lm->isExistLobbyID($lid);
        if (!$lobbyCheck) {
            $rc = $this->LobbyNotExist;
            throw new \Exception($rc['message'], $rc['code']);
        }
        $lbvcnt = $lm->HowManyVisitors($lid);
        if ($lbvcnt > 1) {
            $rc = $this->LobbyIsBusy;
            throw new \Exception($rc['message'], $rc['code']);
        }
        $uil = $lm->isUserInLobby($uid, $lid);
        if ($uil) {
            $rc = $this->AlreadyInLobby;
            throw new \Exception($rc['message'], $rc['code']);
        }
        $gm->setGameId($uid, $lid, 0); //  set new game
        $gm->setGameState($uid, $lid,  $this->gc::GAME_IN_START ); //  set state new game
        return $lm->toEnterInLobby($uid, $lid);
    }



    /**
     * Покинуть лобби:
     * авторизуем запрос ( uid )
     * проверим наличие лобби (lid)
     * находиться ли автор в лобби (uid, lid)
     * не идет ли игра с участием автора в этом лобби (tbl_games, uid, lid)
     * если да, то какое кол-во ходов сделал автор в этой игре (tbl_game, uid)
     * если да и кол-во ходов автора менее 3-х
     * то регистрируем проигрыш автора в hist
     * удалим запись из таблицы tbl_lobby_use
     *
     */
    public function _leave_(){

        $this->getRequest($this->request[(string)__FUNCTION__]['data']);
        $jwt = $this->getRequestParam( $this->request, "jwt" );
        $ls = $this->getRequestParam( $this->request, "LobbyState" );
        $lid = $this->getRequestParam( $ls, 'id');
        $ln = $this->getRequestParam( $ls, 'name');
        $uid = $this->getRequestParam($jwt, "id");
        $userCheck = (new User())->isAuthorizedUser( $jwt );
        $result = false;
        if ($userCheck) {
            $this->gc = new Game( $this->config );
            $result = $this->leave($uid, $lid, $ln);
        }
        if ( $result ){
            $rc = $this->success;
        } else {
            $rc = $this->SomethingWrong;
        }
//        $data = $this->gc->getGameInAjax( $this->request );
//        $this->setResponse('data', $data);
        $this->setResponse('status', "ok");
        $this->setResponse('code', $rc['code']);
        $this->encodeResponse();
        $this->showResponse();
    }

    public function leave($uid, $lid, $ln=null){
        $lm = new \Models\Lobby( $this->config );
        if (!empty($ln)){
            $lm->isExistLobby($ln);
        }
        // сохраним пользовательские настройки
        if ( !$this->gc ){
            $this->gc = new Game( $this->config );
        }
        if ( !empty($this->request) && array_key_exists('userCfg', $this->request)) {
            $userData = $this->getRequestParam($this->request, 'userCfg');
            $this->gc->saveCfg($uid, $userData);
        }
        $uilCheck = $lm->isUserInLobby($uid, $lid);
        if ($uilCheck) {
            $gm = new \Models\Game( $this->config );
            $game = $gm->getGameInLobby($uid, $lid);
            if ( $game ){
                // противник на этот момент может уже покинуть поле боя
                if ( $lm->HowManyVisitors($lid)>1 ){
                    $eid = $gm->getEnemyId($uid, $lid);
                    $gid = $gm->getGameId($uid, $lid);
                    $gm->setCanMove( $uid, $this->gc::CAN_NOT_MOVE );
                    $gm->setCanMove( $eid, $this->gc::CAN_NOT_MOVE );
                    if ((int)$game['gst'] === $this->gc::GAME_IS_ON &&
                        isset($eid) && isset($gid) && $gid && $eid){
                        $gm->setGameLoser( $uid, $gid );
                        $str = 'You left the game ahead of time and will be able to enter another lobby after a minute. Game over.<br>Loser: <span>%s</span>';
                        $event = sprintf($str, $gm->getNameByID($uid));
                        $gm->addWarLog($uid, $gid, $event);
                        $event = 'Enemy left the game ahead of time.<br>No winner in this game. Game over.';
                        $gm->addWarLog($eid, $gid, $event);
                        $lm->setPenalty($uid, (
                        (new MyDateTime())->add(new \DateInterval('PT'.$this::PENALTY)))
                            ->formatDB());
                    }
                    $gm->setGameState( $uid, $lid, $this->gc::GAME_OVER ); // завершим игру
                    $gm->setGameState($eid, $lid, $this->gc::GAME_OVER ); // завершим игру и для соперника
                }
            }
            return $gm->setLobbyLeave($uid, $lid);
        }
        return false;
    }

}

