<?php
namespace Models;

class Lobby extends Model
{
    public function __construct( $request = null ){
        parent::__construct($request);
    }

    /**
     * возвращает id владельца лобби
     *
     * @param $lid int
     *
     * @return int
     * */
    public  function getIdOwner( $lid ){
        $sql = "select lb_owner as id from tbl_lobby where lb_id=?i";
        return (integer)$this->connect->getOne($sql, $lid);
    }
    /**
     * создает лобби, вернет id созданного лобби
     *
     * @param $uid int
     * @param $name string
     * @param $timer int
     *
     * @return int
     * */
    public function create( $uid, $name, $timer ){
        $sql = "insert LOW_PRIORITY  into tbl_lobby (lb_name, lb_timer, lb_owner)
            values (?s, ?s, ?s)";
        $this->connect->query($sql, $name, $timer, $uid);
        return $this->connect->insertId();
    }


    /**
     * регистрирует посетителя в лобби
     *
     * @param $uid int
     * @param $lid int
     *
     * @return bool
     * */
    public function toEnterInLobby( $uid, $lid ){
        $sql = "insert LOW_PRIORITY  into tbl_lobby_use (lu_id_lb, lu_id_pl) values (?i, ?i) on duplicate key update lu_id_lb=?i, lu_id_pl=?i";
        return (bool)$this->connect->query($sql, $lid, $uid, $lid, $uid );
    }

    /**
     * вернет кол-во посетителей в лобби
     *
     * @param $lid int
     *
     * @return int
     * */
    public  function HowManyVisitors( $lid ){
        $sql = "select count(*) from tbl_lobby_use where lu_id_lb=?i";
        return (int)$this->connect->getOne($sql, $lid);
    }

    /**
     * есть ли такое лобби, по lid
     *
     * @param $lid int
     *
     * @return bool
     * */
    public function isExistLobbyID( $lid ){
        $sql = "select lb_id from tbl_lobby where lb_id = ?s";
        return (bool)$this->connect->getOne( $sql, $lid );
    }
    /**
     * есть ли такое лобби, по имени
     *
     * @param $name string
     *
     * @return bool
     * */
    public function isExistLobby( $name ){
        $sql = "select lb_id from tbl_lobby where lb_name = ?s";
        return (bool)$this->connect->getOne( $sql, $name );
    }
    /**
     * находиться ли данный пользователь в данном лобби?
     *
     * @param $uid int
     * @param $lid int
     *
     * @return bool
     */
    public function isUserInLobby( $uid, $lid ){
        $sql = "select u_id from tbl_users, tbl_lobby_use where lu_id_pl=u_id and u_id=?i and lu_id_lb=?i";
        return (bool)$this->connect->getOne( $sql, $uid, $lid );
    }

    /**
     * в каком лобби находиться данный пользователь?
     *
     * @param $uid int
     *
     * @return int
     */
    public function getUsersLobby( $uid ){
        $sql = "select lu_id_lb from tbl_lobby_use where lu_id_pl=?i";
        return (int)$this->connect->getOne( $sql, $uid );
    }
    /**
     * установит время истечения штрафа за досрочный выход
     *
     * @param $uid int
     * @param $time string
     *
     * @return bool
     */
    public function setPenalty( $uid, $time ){
        $sql = "insert LOW_PRIORITY  into tbl_penalty (pn_pid, pn_time) values (?i, ?s) 
on duplicate key update pn_pid=?i, pn_time=?s";
        return $this->connect->query( $sql, $uid, $time, $uid, $time );
    }

    /**
     * вернет разницу во времени м/у текущим и последним штрафом за досрочный выход
     *
     * @param $uid int
     *
     * @return string
     */
    public function getPenalty( $uid ){
        $sql = "select pn_time from tbl_penalty where pn_pid=?i";
        return $this->connect->getOne( $sql, $uid );
    }
}
