"use strict";
/**
 * События
 */
(function () {
    /**
     * содержит ID элементов
     *
     * @return {string}
     */
    let ID = Object.create( null );
    ID = {
        header: 'header',
        body: 'body',
        content: 'content',
        loginForm: 'lf',
        notice: 'notice',
        title: 'title',
        logout: 'logout',
        register: 'register',
        acc: 'acc',
        reset: 'reset',
        cnt: 'cnt',
        img: 'img',
        select: 'select',
        fill: 'fill',
        response: 'response',
        case: 'case',
        newForm: 'newForm',
        sendButton: 'sendButton',
        place: 'place',
        war_log: 'war_log',
        log: 'log',
        game_state: 'game_state',
        game: 'game',
        case_auth: 'case_auth',
        weapon_case: 'weapon_case',
        auth_acc: 'auth_acc',
        lobby_acc: 'lobby_acc',
        auth_form: 'auth_form',
        lobby_select: 'lobby_select',
        lobby_form: 'lobby_form',
        lobby_list: 'lobby_list',
        lobby_list_item: 'lobby_list_item',
        wi: 'wi',
        run: 'run',
        hint: 'hint',
        move: 'move',
        timer: 'timer',
        btn: 'btn',
        score: 'score',
        WarLog: 'WarLog',
        WarLogItem: 'WarLogItem',
        choose: 'choose',
        UserListCase: 'UserListCase',
        UserList: 'UserList',
        LobbiListItem: 'LobbiListItem',
        LobbiListItemQuest: 'LobbiListItemQuest',
        rounds: 'rounds',
        ContdwnInf: 'ContdwnInf'
    };

// --------------------------------------------------------------------------------------------
    let
        Ets = {},
        Views = A.Views;

    /**
     * рендеринг компонентов
     *
     * */

    // Ets.renderDOMtst = function (arg, resolve){
    //     console.time("renderTST");
    //     let i = 0;
    //     while (i<5000){
    //         new Views.Tst({
    //             id: 'tst' + i++
    //         }, 'resp').render();
    //     }
    //     console.timeEnd("renderTST");
    //     A.My.renderDOM();
    //     resolve( arg );
    // };

    Ets.renderDOM = function (arg, resolve){
        console.time("renderDOM");
        new Views.Place({
            id: ID.header,
            html: "<div id='case_auth' class='top_user_form'></div>" +
            "<div id='lobby_select'></div><div id='lobby_form'></div>" +
            "<div id='run'></div><div id='RunInf'></div><div id='hint'></div>",
            class: 'header'
        }, ID.body).render();

        // new Views.AuthForm({
        new Views.AuthForm({
            id: ID.case_auth,
            data: arg,
            limit_name: function(){ return ( arg.limits ) ? arg.limits['user'].username.caption : ''},
            limit_password: function(){ return ( arg.limits ) ? arg.limits['user'].password.caption : ''},
            parent: ID.header
        }).render();

        new Views.LobbySelect({
            id: ID.lobby_select,
            lobby: arg.LobbyState,
            user: arg.UserState,
            parent: ID.header,
        }).render();

        new Views.LobbyForm({
            id: ID.lobby_form,
            user: arg.UserState,
            lobby: arg.LobbyState,
            LList: arg.LobbyList,
            limit_name: function(){ return ( arg.limits ) ? arg.limits['lobby'].name.caption : ''},
            limit_timer: function(){ return ( arg.limits ) ? arg.limits['lobby'].timer.caption : ''},
        },ID.header).render();

        new Views.LobbyList({
            id: ID.lobby_list,
            parent:  ID.lobby_form
        }).render();

        // header
        {
            let arr = arg.LobbyList, item, LList = '';
            let rec = function (a) {
                let result = {}, id, n = 1;
                for (let i = 0; i < a.length; i++) {
                    item = a[i];
                    if (!result[item.id]) result[item.id] = {};
                    if (a[i]['player']) {
                        if (id && a[i].id === id) {
                            n++;
                            result[item.id]['player' + n] = a[i]['player'];
                        }
                        if (!id || a[i].id !== id) {
                            result[item.id]['player' + n] = a[i]['player'];
                        }
                    }
                    id = item.id;
                    result[id]['id'] = a[i]['id'];
                    result[id]['name'] = a[i]['name'];
                    result[id]['timer'] = a[i]['timer'];
                    result[id]['owner'] = a[i]['owner'];
                    result[id]['state'] = a[i]['state'];
                }
                return result;
            };
            if (arr) LList = rec(arr);
            for (let i in LList) {
                if (LList.hasOwnProperty(i)) {
                    item = LList[i];
                    new Views.LobbyListItem({
                        id: item.id,
                        name: item.name,
                        timer: item.timer,
                        owner: item.owner,
                        player1: item['player1'],
                        player2: item['player2'],
                        state: item.state
                    }, ID.lobby_list).render();
                }
            }

            let prefix = A.SEL.userCfg;
            let obj = A.Common.prototype.getLocStorData(prefix);
            let i = 1;
            while (i < 3) {
                new Views.Button({
                    id: ID.btn,
                    ind: i,
                    game: arg.GameState,
                    move: arg.MoveState,
                    hint: arg.Hint,
                    UserCfg: obj
                }, ID.body).render();
                i++;
            }
        }
        new Views.Hint({
            id: ID.hint,
            game: arg.GameState,
            move: arg.MoveState,
            hint: arg.Hint,
            parent: ID.header
        }).render();

        new Views.Run({
            id: ID.run,
            game: arg.GameState,
            lobby: arg.LobbyState,
            parent: ID.header
        }).render();

        new Views.RunInf({
            id: 'RunInf',
            game: arg.GameState,
            lobby: arg.LobbyState,
            parent: ID.header
        }).render();

        // weapon
        {
            let i = 1;
            while (i < 6) {
                new Views.Weapon({
                    id: ID.wi + i,
                    i: i,
                    data: arg.MoveState,
                    game: arg.GameState,
                    hint: function () {
                        if (arg.Hint && arg.Hint !== 0 && arg.Hint !== 255) {
                            let arr = arg.Hint;
                            for (let k = 0; k < arr.length; k++) {
                                if (arr[k] === i) {
                                    return 1
                                }
                            }
                        }
                    }(),
                    disable: 'disable',
                    parent: ID.header
                }).render();
                i++;
            }
        }
        new Views.Score({
            id: ID.score,
            game: arg.GameState,
            user: arg.UserState,
            lobby: arg.LobbyState,
        }, ID.body).render();

        new Views.Rounds({
            id: ID.rounds,
            game: arg.GameState,
        }, ID.body).render();

        new Views.Move({
            id: ID.move,
            move: arg.MoveState,
            game: arg.GameState
        }, ID.body).render();

        new Views.GameState({
            id: ID.game_state,
            app: arg.AppState,
            user: arg.UserState,
            lobby: arg.LobbyState,
            game: arg.GameState,
            move: arg.MoveState,
            const: arg.Const,
            parent: ID.game_state
        },ID.game_state).render();

        new Views.WarLog({
            id: ID.WarLog,
            game: arg.GameState,
            move: arg.MoveState,
            hint: arg.Hint,
            UserCfg: arg.UserCfg
        }, ID.game).render();

        new Views.Log({
            id: ID.log,
            data: arg.GameState,
            class: ID.log,
            parent: ID.WarLog
        }).render();
        // WarLogItem
        {
            let data = arg.WarLog, item;
            for (let i in data) {
                if (data.hasOwnProperty(i)) {
                    item = data[i];
                    new Views.WarLogItem({
                        id: item.id,
                        time: item.time,
                        event: item.event,
                        gid: item.gid
                    }, ID.log).render();
                }
            }
        }
        new Views.ContdwnInf({
            id: ID.ContdwnInf,
            data: arg.Const,
            game: arg.GameState,
        }, ID.game).render();

        new Views.UserListCase({
            id: ID.UserListCase,
            game: arg.GameState,
            move: arg.MoveState,
            hint: arg.Hint,
            UserCfg: arg.UserCfg
        }, ID.game).render();

        new Views.UserList({
            id: ID.UserList,
            data: arg.UserList,
        }, ID.UserListCase).render();

        A.My.renderDOM();
        console.timeEnd("renderDOM");
        console.log ( arg );
        console.log ( A.My.VDom );

        // console.log ( Object.getPrototypeOf(A.My.VDom[ID.case_auth]) );

        arg.int = 0; // счетчик для анимации хода
        resolve( arg );
    };
    /**
     * анимируем таймер в интервале равном 1sec/frequency, если оставить как есть и перерисовывать
     * только исходя из ответов сервера, то
     * поскольку минимальный интервал ответов сервера здесь принят равным 0,5 сек, анимация получается
     * некрасивая (при значении таймера лобби менее 10 сек) - убывание дуги происходит слишком большими частями, появляются скачки.
     *
     * Поэтому принято решение анимировать таймер дополнительно м/у ответами сервера.
     *
     * кроме этого в текущей реализации таймеры могут быть рассинхронизированы на величину угла в радианах
     * равной 2пи*интервал(сек)/таймер(сек), например при таймере = 5 сек и интервале ответов сервера 1 сек,
     * угол расхождения может достигать 6,28/5 = 1,25 рад = 72 градуса.
     *
     * Возникает это по той причине, что в данной реализации игры клиентские запросы не синхронизированы по времени и
     * разность может достигать величины интервала ответов сервера
     *
     * Кроме того здесь могут и возникают ситуации, когда у второго игрока времени на ход будет меньше на время до 1 сек,
     * это конечно может быть досадно, но зато верятность удачного использования подсказки возрастает на 1/таймер,
     * тк для игрока начавшего игру возможность сделать ход открылась раньше.
     *
     * */
    let
        frequency = 20, // частота анимации, раз в сек
        delay = 1000/frequency;
    A.Timers.timerId = setTimeout(function tick() {
        new A.Views.Timer({
            id: ID.timer,
            move: A.arg.MoveState,
            lobby: A.arg.LobbyState,
            time: function () {
                if (A.arg.TimeState) {
                    return A.arg.TimeState['timePassedMove'] / 1000000 + A.arg.int++ / frequency
                } else {
                    clearTimeout(A.Timers.timerId);
                }
            }(),
            freq: frequency
        }, ID.weapon_case).render();
        // console.log('timer');
        // debugger
        A.My.renderDOM();
        A.Timers.timerId = setTimeout(tick, delay, A.arg);
    }, delay, A.arg);

    /**
     * показать прелоадер
     *
     * @param {object} arg
     */
    Ets.showLoader = function (arg, resolve){
        console.log('showLoader');
        A.Common.prototype.loader( arg.target ).show();
        resolve( arg );
    };

    Ets.testDS = function (arg, resolve){
        console.log('testDS');
        let el = A.gid(ID.weapon_case);
        el.dataset.outline = String(1);
        resolve( arg );
    };

    Ets.addClass = function (arg, resolve){
        console.log ( 'addClass' );
        let el = A.gid(arg.req.obj.data.id);
        if (el){
            el.classList.add(arg.event.d);
        }
        resolve( arg );
    };

    Ets.remClass = function (arg, resolve){
        console.log ( 'remClass' );
        let el = document.querySelector('.'+arg.event.d);
        if (el) {
            el.classList.remove(arg.event.d);
        }
        resolve( arg );
    };

    Ets.getLobbyList = function (arg, resolve){
        console.log ( 'getLobbyList' );
        arg.req.obj.select = 'sse';
        arg.req.obj.mode = 'getGameState';
        arg.req.obj.data = 'LobbyList';
        resolve( arg );
    };

    let eventSource;
    /**
     * событие, запуск транспорта
     *
     * */
    Ets.sse_on = function (arg, resolve){
        console.log ( 'sse_on' );
        arg.es = {};
        arg.es.req = JSON.stringify( arg.req.obj );

        arg.response = function( e ){
            // console.log ( e.data );
            try {
                // console.log ( e );
                arg.resp = JSON.parse(e.data);
                console.log( arg.resp );
                // resolve( arg );
                Ets.getResp( arg, resolve );
                // observer.publish( 'getResp', arg );
            } catch ( e ){
                // debugger
                console.log ( e );
                // console.log ( e.data );
                this.onerror( e );
            }
        };
        ES( arg );
    };

    /**
     * собственно транспорт
     *
     * */
    let ES = function ( arg ){
        arg.onerror = function(e) {
            if (this.readyState === EventSource.CONNECTING) {
                console.log ( e );
                console.log("Соединение порвалось, пересоединяемся...");
            } else {
                console.log("Ошибка, состояние: " + this.readyState);
            }
        };
        arg.error = function(e) {
            try {
                console.log ( e );
                // https://stackoverflow.com/questions/17262833/what-causes-eventsource-to-trigger-an-error-in-my-chrome-extension
                let resp = JSON.parse( e.data );
                if ( eventSource ) eventSource.close();
                console.log ( resp );
                // можно использовать в отладке
                let dbg = resp['xdebug_message'];
                if (dbg){
                    // document.querySelector('main').insertAdjacentHTML('afterend', "<div>"+ dbg + "</div>");
                }
            } catch (e){
                console.log( e );
            }
        };

        if ( !window.EventSource ){
            alert('В этом браузере нет поддержки EventSource.');
            return;
        }
        if ( !eventSource ) eventSource = new EventSource('init.php?req=' + arg.es.req);
        eventSource.onerror = arg.onerror;
        eventSource.addEventListener( 'response', arg.response );
        eventSource.addEventListener( 'error', arg.error );
    };

    Ets.sseOff = function ( arg, resolve ){
        console.log ( 'sseOff' );
        if ( eventSource ){
            eventSource.close();
            eventSource = null; // как оказалось может работать одновременно несколько ES
        }
        resolve( arg )
    };

    /**
     * получить от сервера ограничения на поля
     *
     * @param {object} arg
     *
     * @param resolve
     * */
    Ets.getLimits = function (arg, resolve){
        console.log('getLimits');
        if ( !arg['limits'] ){
            A.User.prototype['delJwt'](arg);
            arg['limits'] = {};
            arg.sm = function () {
                let arr = arg.resp.data;
                for (let i=0; i < arr.length; i++){
                    if ( !arg[ 'limits' ][ arr[ i ].form] ) arg['limits'][arr[i].form] = {};
                    arg[ 'limits' ][ arr[ i ].form][arr[i].name] = arr[ i ];
                }
            };
            arg.req.obj.select = 'common';
            arg.req.obj.mode = 'getLimits';
            arg.cs.cln.runReq = 1;
        }
        resolve( arg )
    };

    Ets.saveUserData = function (arg, resolve){
        console.log ( 'saveUserData' );
        if (arg.resp.user) A.User.prototype.save( arg.resp.user, A.SEL.userData );
        resolve( arg );
    };

    Ets.saveUserCfg = function (arg, resolve){
        console.log( 'saveUserCfg' );
        if (!arg.UserCfg) arg.UserCfg = {};
        arg.UserCfg[arg.event.d] = arg.event.v;
        A.User.prototype.saveItem(A.SEL.userCfg, arg.event.d, arg.event.v);
        resolve( arg );
    };

    /**
     * получить настройки пользователя
     *
     * @param {object} arg
     *
     * */
    Ets.getUserConf = function (arg, resolve){
        console.log('getUserConf');
        arg.sm = function () {
            arg.UserCfg = arg.resp.data;
            A.User.prototype.save( arg.UserCfg, A.SEL.userCfg );
        };
        arg.req.obj.select = 'common';
        arg.req.obj.mode = 'getUserConf';
        arg.cs.cln.runReq = 1;
        resolve( arg );
    };

    Ets.purgeComp = function (arg, resolve){
        console.log ( 'purgeComp' );
        let log = A.gid(arg.event.d);
        if (log) log.innerHTML = '';
        resolve( arg );
    };

    Ets.purgeLog = function (arg, resolve){
        console.log ( 'purgeLog' );
        let els = A.gid(ID.log).querySelectorAll('.'+ID.WarLogItem), el;
        for (let i = 0; i<els.length; i++){
            el = els[i];
            if (arg.GameState) {
                if (+el.dataset.gid !== +arg.GameState.gid) {
                    el.remove();
                    // debugger
                    /**
                     * todo нужен метод в My для удаления неактуальных компонентов и блоков
                     * */
                    delete A.My.VDom[el.id];
                    delete A.My.VDom.log.prop.nested[el.id];
                }
            }
        }
        resolve( arg );
    };

    Ets.close_popup = function (arg, resolve){
        console.log ( 'close_popup' );
        $.magnificPopup.close();
        resolve( arg );
    };
    /**
     * обновляет JWT - токен авторизации
     *
     * @param {object} arg - глобальный массив данных в области видимости модуля А
     * @param resolve
     * @return {action} - save JWT in LS
     * */
    Ets.addUserData = function (arg, resolve){
        console.log('addUserData');
        A.User.prototype.addUserData( arg );
        if ( !arg.req.obj.data || !arg.req.obj.data.jwt ) throw new UserException( 10, 'Пользователь не авторизован');
        resolve( arg );
    };

    function UserException(code, message) {
        this.code = code;
        this.message = message;
        this.name = "Исключение, определенное пользователем";
    }

    Ets.addLobbyData = function (arg, resolve){
        console.log('addLobbyData');
        if ( !arg.req.obj.where ) arg.req.obj.where = {};
        arg.req.obj.where.lobby = arg.LobbyState;
        resolve( arg );
    };
    /**
     * подготовка запроса данных для вывода списка объявлений
     *
     * @param {object} arg - глобальный массив данных в области видимости модуля А
     * @return {object} arg
     * */
    Ets.getState = function (arg, resolve){
        console.log ( 'getState' );
        arg.req.obj.select = 'game';
        arg.req.obj.mode = 'get_state';
        arg.cs.cln.runReq = 1;
        resolve( arg );
    };
    /**
     * подготовка основного запроса - нерекомендован - неочевиден
     *
     * @param {object} arg - глобальный массив данных в области видимости модуля А
     * @return {object} arg
     * */
    Ets.setReqFromURL = function ( arg, resolve ){
        console.log ( 'setReqFromURL' );
        if ( !arg.req.obj.data ) arg.req.obj.data = {};
        arg.req.obj.data.UserState = arg.UserState;
        arg.req.obj.data.LobbyState = arg.LobbyState;
        arg.req.obj.data.GameState = arg.GameState;

        arg.cs.cln.runReq = 1;
        resolve( arg );
    };

    Ets.getGameState = function (arg, resolve){
        console.log ( 'getGameState' );
        arg.req.obj.select = 'Game';
        arg.req.obj.mode = 'getGameState';
        arg.req.obj.data.AppState = arg.AppState;
        arg.req.obj.data.UserState = arg.UserState;
        arg.req.obj.data.LobbyState = arg.LobbyState;
        arg.req.obj.data.GameState = arg.GameState;
        resolve( arg );
    };

    Ets.saveResp = function ( arg, resolve ){
        console.log ( 'saveResp' );
        arg[ arg.event.d ] = arg.resp.data;
        resolve( arg );
    };
    /**
     * remove JWT from request
     *
     * @param {object} arg
     * @param resolve
     * @return {object} arg
     * */
    Ets.delJWT = function (arg, resolve){
        A.User.prototype.delJwt( arg );
        resolve( arg );
    };
    /**
     * Fetch request
     *
     * @param {object} arg
     * @param resolve
     * @param reject
     * @return {object} arg
     * */
    Ets.fetch = function (arg, resolve, reject){
        if ( arg.cs.cln.runReq ){
            console.log( 'fetch' );
            arg.reqURL = '/init.php';
            arg.reqData = JSON.stringify( arg.req.obj );
            arg.fetchInit = {};
            if ( arg.req.metod || !arg.req.metod ) {
                arg.fetchInit.metod = 'get';
                // arg.headers = new Headers();
                // arg.headers.append('Content-Type', 'charset=UTF-8');
                // arg.headers.append('Content-Type', 'application/json');
                // arg.headers.append('Content-Type', 'multipart/form-data');
                arg.reqURL += '?req=' + arg.reqData;
                A.Fetch( arg, resolve );
            }
            else if ( arg.req.metod === 'post' ){
                let data = new FormData();
                data.append( 'req', arg.reqData );
                if (arg.file) {
                    data.append( arg.file.name, arg.file );
                }
                arg.fetchInit.body = data;
                A.Fetch( arg, resolve );
            }
        } else {
            resolve( arg );
        }
    };
    /**
     * AJAX request
     * используется только как временная замена fetch
     * @param {object} arg
     * @param resolve
     * @return {object} arg
     * */
    Ets.xhr = function (arg, resolve){
        if ( arg.cs.cln.runReq ){
            console.log( 'xhr' );
            if (!arg.onerror || typeof(arg.onerror) !== "function") {
                arg.onerror = function (e) {
                    arg.err = e;
                    this.abort();
                    err.run( arg );
                };
            }
            arg.loadend = function (){
                try {
                    //console.log ( this.readyState +' --- '+ this.status);
                    arg.req.type = null; // обнулим, иначе последний тип запроса сохраниться
                    if ( this.readyState === 4 && this.status === 200 ){
                        console.log ( this.response );
                        console.log ( JSON.parse(this.response) );
                        console.log ( arg );
                        arg.req.obj.data = {};
                        if ( !this.response ){
                            arg.err = {};
                            arg.err.data = 'сервер не ответил :-(, попробуйте позже';
                            arg.resp.data = arg.err.data;
                            return this.onerror( arg.err );
                        }
                        try {
                            if ( this.response === 'null'){
                                arg.resp.data = 'ответ получен, но данных к загрузке нет :-(';
                                arg.resp.code = 1024;
                            } else {
                                arg.resp = JSON.parse( String( this.response ));
                                // debugger
                                // можно определить действие внутри модуля или события
                                if (arg.sm && typeof(arg.sm) === "function"){
                                    arg.sm( arg );
                                    arg.sm = false;
                                }
                            }
                            arg.cs.cln.runReq = 0;
                            resolve( arg );
                        }
                        catch ( e ){
                            //console.log ( this.response );
                            if ( e.code === 10 ){
                                console.warn( e.message );
                                return;
                            }
                            arg.resp = this.response;
                            arg.cs.cln.err = 1;
                            this.onerror( e );
                            this.abort();
                        }
                    }
                    else if ( this.readyState === 4 && this.status !== 200 ){
                        this.onerror(arg);
                    }
                } catch ( e ){
                    this.onerror( e );
                }
            };

            if ( !arg.req.type ) arg.req.type = 'get';
            arg.dataXHR = JSON.stringify( arg.req.obj );
            if ( arg.req.type === 'post' ) {
                let data = new FormData();
                data.append('req', JSON.stringify( arg.req.obj ));
                if (arg.file) {
                    data.append( arg.file.name, arg.file );
                }
                arg.dataXHR = data;
            }
            xhr.run( arg );
        } else {
            arg.cs.cln.runReq = 0;
            resolve( arg );
        }
    };
    /**
     * пауза, иногда, для отладки
     *
     * @param {object} arg
     * @param resolve
     * @return {object} arg
     * */
    Ets.pause = function (arg, resolve){
        console.log ( 'pause' );
        console.log ( A.My.VDom );
        console.log ( arg );
        debugger;
        resolve( arg );
    };
    /**
     * logout и очистка пользовательских данных в LS
     *
     * @param {object} arg
     * @param resolve
     * @return {object} arg
     * */
    Ets.logout = function ( arg, resolve ){
        console.log ( 'logout' );
        localStorage.clear();
        arg.AppState = false;
        arg.UserState = false;
        arg.LobbyState = false;
        arg.LobbyList = false;
        arg.GameState = false;
        arg.MoveState = false;
        arg.data = false;
        arg.UserCfg = false;
        resolve( arg );
    };

    Ets.showElem = function (arg, resolve){
        console.log ( 'showElem' );
        let el = A.gid(arg.event.d);
        A.slide.down(el, null, 550, 'cubic-bezier(1,.2,.9,1.7)');
        resolve( arg );
    };

    Ets.hideElem = function (arg, resolve){
        console.log ( 'hideElem' );
        let el = A.gid(arg.event.d);
        A.slide.up(el, 550, 'cubic-bezier(1,.2,.9,1.7)');
        resolve( arg );
    };
    /**
     *
     *
     * @param {object} arg
     * @param resolve
     * @return {object} arg
     * */
    Ets.closePopUp = function (arg, resolve){
        console.log('closePopUp');
        if (window.jQuery && $.magnificPopup) {
            let magnificPopup = $.magnificPopup.instance; // save instance in magnificPopup variable
            magnificPopup.close(); // Close popup that is currently opened
        }
        resolve( arg );
    };

    /**
     * вставка элементов на страницу
     *
     * @param {object} params
     * @return {HTMLElement}
     * */
    let ins = (function (){
        let run = function (params){
            let el = document.createElement(params.tag || 'p'), cls;
            if (params.id) {
                el.id = params.id;
            }
            if (typeof params.html === 'function') {
                el.innerHTML = params.html();
            } else {
                let html = params.html;
                if (params['plch']) {
                    for (let name in params['plch']) {
                        if ( params['plch'].hasOwnProperty(name) ){
                            params.html = html.replace( new RegExp("{"+name+"}", 'g'), params['plch'][name] );
                        }
                    }
                }
                el.innerHTML = params.html || null;
            }
            if (params.name) {
                el.name = params.name;
            }
            if (params.value) {
                el.value = params.value;
            }
            if (params.clas) {
                if (typeof params.clas === 'function') {
                    cls = params.clas();
                } else {
                    cls = params.clas;
                }
                if (cls !== undefined) {
                    let arr = cls.split(' ');
                    for (let i = 0; i<arr.length; ++i){
                        el.classList.add( arr[i] );
                    }
                }
            }
            if (params.attr) {
                for (let attr in params.attr) {
                    if (params.attr.hasOwnProperty(attr)) {
                        el.setAttribute(attr, params.attr[attr]);
                    }
                }
            }
            for (let arg in params){
                if (params.hasOwnProperty(arg) && params[arg] === true){
                    el[arg] = true;
                }
            }

            if (params.style) {
                for (let style in params.style) {
                    if (params.style.hasOwnProperty(style)) {
                        if (typeof params.style[style] === 'function') {
                            el.style[style] = params.style[style]();
                        } else {
                            el.style[style] = params.style[style];
                        }
                    }
                }
            }
            let parent = params.parent;
            if (params['orderInParent'] !== undefined) {
                params.result = parent.insertBefore(el, params.parent.children[params['orderInParent']]);
                return;
            }
            return parent.insertBefore(el, null);
        };
        return {
            run: run
        }
    })();

    /**
     * обработка ответов сервера
     *
     * @param {object} arg
     *
     * @param resolve
     * */
    Ets.getResp = function (arg, resolve){
        console.log ( 'getResp' );
        let resSel = 'response', r, res;
        // todo неприемлемо, что элемент вставляется в любом случае
        if ( arg.form && (arg.resp.data || arg.resp.message )) {
            r = arg.form.form.querySelector('.' + resSel);
            if (r) r.remove();
            res = ins.run({
                tag: 'div',
                hidden: true,
                clas: resSel,
                html: arg.resp.data || arg.resp.message,
                parent: arg.target
            });
        }
        function down() {
            slide.down( res );
        }
        function up() {
            setTimeout(function () {
                slide.up( res );
            }, 3000);
        }
        function setAppState() {
            arg.AppState = arg.resp.AppState;
            arg.UserState = arg.resp.UserState;
            arg.LobbyState = arg.resp.LobbyState;
            arg.LobbyList = arg.resp.LobbyList;
            arg.GameState = arg.resp.GameState;
            arg.MoveState = arg.resp.MoveState;
            arg.TimeState = arg.resp.TimeState;
            arg.Hint = arg.resp.Hint;
            arg.WarLog = arg.resp.WarLog;
            arg.UserList = arg.resp.UserList;
            arg.Const = arg.resp.Const;
            // console.log ( arg.resp.data || arg.resp.message );
            A.facade.run("#{\"select\":\"set\",\"mode\":\"AppState\"}");
        }
        // debugger
        switch (arg.resp.code) {
            case 0:
                if ( arg.resp.data) {
                    arg.AppState = arg.resp.data.AppState;
                    arg.UserState = arg.resp.data.UserState;
                    arg.LobbyState = arg.resp.data.LobbyState;
                    arg.LobbyList = arg.resp.data.LobbyList;
                    arg.GameState = arg.resp.data.GameState;
                    arg.MoveState = arg.resp.data.MoveState;
                    arg.TimeState = arg.resp.TimeState;
                    arg.hint = arg.resp.data.Hint;
                    arg.WarLog = arg.resp.data.WarLog;
                    arg.UserList = arg.resp.data.UserList;
                    if (arg.resp.data.UserCfg) A.User.prototype.save( arg.data.UserCfg, A.SEL.userCfg );
                }
                arg.req.act = arg.req.act.concat( A.actions[ 'set_AppState' ]);
                arg.req.act = arg.req.act.concat( A.actions[ 'sse_run' ]);
                break;
            case 103: // SSE off
                arg.AppState = 'off-line';
                let dbg = arg.resp.data;
                if ( dbg && dbg['xdebug_message']){
                    // можно включить на время отладки
                    document.querySelector('main').insertAdjacentHTML('afterend', "<div>"+ arg.resp.data['xdebug_message'] + "</div>");
                }
                // debugger
                return A.facade.run("#{\"select\":\"App\",\"mode\":\"off\"}");
            case 40: // authorized / create or select lobby
            case 20: // a lobby create & select a owner
            case 21: // a lobby select a guest
            case 128: // a lobby select a guest
                setAppState();
                return;
            case 1:
                console.log ( arg.resp.data || arg.resp.message );
                console.log ( arg.req.act );
                let add_arr = A.actions[ 'common_auth' ];
                arg.req.act = arg.req.act.concat( add_arr );
                break;
            case 62: // a lobby is busy
                console.log ( arg.resp.data );
                return down( res );
            case 101: // registered user
                console.log ( arg.resp.data || arg.resp.message );
                down( res );
                arg.target.setAttribute('disabled', '');
                arg.target.style.color = A.Color.text;
                return;
            case 65: // UserNotInLobby
            case 67: // game over
            case 2048:
                console.log ( arg.resp.data || arg.resp.message );
                setAppState();
                return;
            case 41: // something wrong
                console.log ( arg.resp.data || arg.resp.message );
                return A.facade.run("#{\"select\":\"sse\",\"mode\":\"run\"}");
            case 68: // penalty
                let el = arg.target.parentNode.querySelector('.header');
                let temp = el.innerHTML;
                el.innerHTML = arg.resp.data || arg.resp.message;
                setTimeout(function () {
                    el.innerHTML = temp;
                }, 3000);
                return;
            case 32: // column not exist
                console.log ( arg.resp.data || arg.resp.message );
                break;
            default:
                console.log ( arg.resp.data || arg.resp.message );
                down( res );
                up( res );
                let inputs = arg.form.inputs;
                if ( inputs ) inputs[0].focus();
                return;
        }
        resolve( arg );
    };

    /**
     * отправить данные
     *
     * @param {object} arg
     *
     * @param resolve
     * */
    Ets.sendDataForm = function (arg, resolve){
        console.log('sendDataForm');
        let form = arg.form;
        let data = form.getData();
        let frm = arg.form.form;
        arg.data = data;
        arg.req.metod = 'post';
        let req = JSON.parse( arg.req.hash );
        arg.req.obj.select = req.select;
        arg.req.obj.mode = frm.dataset.mode || req.mode;
        if ( !arg.req.obj.data ) arg.req.obj.data = {};
        arg.req.obj.data.form = data;
        arg.cs.cln.runReq = 1;
        arg.cs.cln.show = frm.dataset.show;
        resolve( arg );
    };

    /**
     * отправить данные
     *
     * @param {object} arg
     *
     * @param resolve
     * */
    Ets.sendData = function (arg, resolve){
        console.log('sendData');
        A.User.prototype.addUserData( arg );
        arg.data = 'data';
        arg.req.metod = 'post';
        let req = JSON.parse( arg.req.hash );
        arg.req.obj.select = req.select;
        arg.cs.cln.runReq = 1;
        resolve( arg );
    };

    /**
     * выберем активную форму
     *
     * @param {object} arg
     * @param resolve
     * */
    Ets.selectForm = function (arg, resolve){
        console.log('selectForm');
        arg.form = new A.Form( arg.event.d || arg.target.closest('form') || arg.target.parentNode );
        resolve( arg );
    };
    /**
     * отобразить popup
     *
     * @param {object} arg
     * @param resolve
     * */
    Ets.loadFormInPopUp = function (arg, resolve){
        console.log('loadFormInPopUp');
        if (arg.target.classList.contains('disabled')) return;
        $.magnificPopup.open({
            items: {
                src: '#'+arg.event.d,
            },
            callbacks:{
                close: function() {
                    console.log ( arg );
                    A.facade.run("#{\"select\":\"sse\",\"mode\":\"run\"}");
                },
                open: function () {
                    let el = A.gid( arg.event.d );
                    setTimeout(function(){
                        (el.querySelector('input[name=name]') || el.querySelector('input[name=username]')).focus();
                    }, 100);
                }
            },
            fixedContentPos: false,
            fixedBgPos: true,
            overflowY: 'auto',
            closeBtnInside: true,
            preloader: false,
            removalDelay: function(){ return (arg.event.t === undefined) ? 300 : arg.event.t}(),
            // removalDelay: 300,
            /**
             * эта задержка нарушает работу My.renderDOM(), возможно MagnifPopUp
             * кэширует контент и после задержки восстанавливает уже неактуальное состояние
             * компонента
             * */

            mainClass: 'my-mfp-slide-bottom'
        });
        resolve( arg );
    };
    /**
     * активация аккордеона
     *
     * @param {object} arg
     * @param resolve
     * */
    Ets.setAcc = function (arg, resolve){
        console.log('setAcc');
        let acc = A.gid( arg.event.d );
        acc.removeEventListener('click', slider);
        acc.addEventListener('click', slider);
        resolve( arg );
    };
    /**
     * активация аккордеона
     *
     * @param {object} arg
     * @param resolve
     * */
    Ets.loadAcc = function (arg, resolve){
        console.log('loadAcc');
        if (window.jQuery) {
            let acc = '#'+arg.event.d, accordion = $(acc), active = A.SEL.active,
                accordion_head = accordion['children']('div'),
                accordion_body = accordion_head.nextAll('form');
            let I = 0; // set tab in form
            let arr = document.querySelectorAll(acc + ' > ' + 'div'), item;
            for ( let i = 0; i < arr.length; ++i ){
                item = arr[ i ];
                if (item.classList.contains(active)) I = i;
            }
            accordion_head.eq(I).addClass(active).next().slideDown(0);
            accordion_head.on('click', function (event) {
                event.preventDefault();
                if ($(this).attr('class') !== active) {
                    accordion_body.slideUp('normal');
                    $(this).next().stop(true, true).slideToggle('normal');
                    accordion_head.removeClass(active);
                    $(this).addClass(active);
                }
            });
        }
        resolve( arg );
    };

    /**
     * slide.up
     *
     * @param {string || HTMLElement} elem
     * @param {number} duration
     * @param {string} timing_function
     *
     * slide.down
     *
     * @param {string || HTMLElement} elem
     * @param {html} content
     * @param {number} duration
     * @param {string} timing_function
     *
     * slide.setHeight - если нужно плавно изменить высоту видимого блока
     *
     * @param {string || HTMLElement} elem
     * @param {html} content
     * @param {number} duration
     * @param {string} timing_function
     * */
    let slide = function slide(){
        let far, style;
        let el = function(elem, duration, timing_function){
            if ( typeof elem === 'string' ){
                far = document.getElementById( elem );
            } else if ( typeof elem === 'object') {
                far = elem;
            }
            far.style.transitionProperty = 'height, padding';
            far.style.transitionDuration = duration + 'ms';
            far.style.transitionTimingFunction = timing_function || 'ease-in-out'; //'cubic-bezier(.5, -1, .5, 2)';
            style = getComputedStyle(far);
        };
        let up = function (elem, duration, timing_function){
            duration = duration || 500;
            el( elem, duration, timing_function );
            // console.log ( far.hidden );
            far.style.height = parseFloat(style.height)+'px';
            far.style.overflow = 'hidden';
            far.style.boxSizing = style.boxSizing;
            far.style.height = 0;
            far.style.padding = 0;
            tm( far, duration );
        };
        let tm = function(far, duration){
            setTimeout(function () {
                far.hidden = 'hidden';
                far.style.padding = '';
            }, duration)
        };
        let down = function (elem, content, duration, timing_function){
            duration = duration || 500;
            el( elem, duration, timing_function  );
            setHeight( elem, content );
            setTimeout( function(){
                far.style.height = 'auto';
            }, duration );
        };
        let setHeight = function ( elem, content ){
            /**
             * метод установки высоты блока, отдельно исп-ся тогда когда height-transition установлено в css
             * и высота в css = 0
             */
            let bw = parseFloat(style.borderWidth);
            let bs = style.boxSizing;
            let pdT = parseFloat(style.paddingTop);
            let pdB = parseFloat(style.paddingBottom);
            let pdL = parseFloat(style.paddingLeft);
            let pdR = parseFloat(style.paddingRight);
            far.style.height = parseFloat(style.height)+'px';
            if ( content ) far.innerHTML = content;
            let cloneFar = far.cloneNode(true);
            cloneFar.style.display = 'inline';
            cloneFar.style.position = 'absolute';
            far.style.height = 0;
            far.style.padding = 0;
            far.style.overflow = 'hidden';
            far.hidden = false;
            cloneFar.style.width = far.offsetWidth+'px';
            cloneFar.style.boxSizing = bs;
            cloneFar.style.height = 'auto';
            far.parentNode.appendChild(cloneFar);
            if (bs === 'border-box'){
                far.style.paddingTop = pdT + 'px';
                far.style.paddingBottom = pdB + 'px';
                far.style.paddingLeft = pdL + 'px';
                far.style.paddingRight = pdR + 'px';
                far.style.height = cloneFar.offsetHeight+'px';
            }
            if (bs === 'content-box'){
                far.style.position = 'static';
                far.style.paddingTop = pdT + 'px';
                far.style.paddingBottom = pdB + 'px';
                far.style.height = cloneFar.offsetHeight-(pdT+pdB)-bw*2+'px';
                far.style.width = cloneFar.offsetWidth-(pdL+pdR)-bw*2+'px';
            }
            cloneFar.remove();
        };
        return {
            down: down,
            up: up,
            setHeight: setHeight
        };
    }();

    function slider(e) {
        let target = e.target, clas = 'active';
        if ( !target.classList.contains('acc_header')) return;
        let el = target.nextSibling;
        if ( el.hidden ){
        console.log(el)
            slide.down( el );
            target.classList.add( clas )
        } else {
            slide.up( el );
            target.classList.remove( clas )
        }
    }

    A.slide = slide;
    A.ID = ID;
    A.Ets = Ets;
    A.UserException = UserException;

})();
