<?php
namespace Controllers;
use Models;

class Common extends Controller{

    public function __construct( $request = null )
    {
        if ( $request !== null ){
            parent::__construct( $request );
        }
    }

    /**
     * возвращает все лимиты, tbl_limits
     * */
    public function _getLimits_(){
        $data = ( new Models\Model( $this->config ))->getAllLimits();
        $this->setResponse( 'data', $data );
        $this->setResponse('code', 0);
        $this->setResponse('status', 'ok');
        $this->encodeResponse();
        $this->showResponse();
    }

    /**
     * отдает клиенту настройки пользователя
    */
    public function _getUserConf_(){
        $this->getRequest($this->request[(string)__FUNCTION__]['data']);
        $jwt = $this->getRequestParam( $this->request, "jwt" );
        $uid = (int)$this->getRequestParam($jwt, "id");
        $md = new Models\Game( $this->config );
        $data = $md->getConf($uid);
        $this->setResponse( 'data', $data );
        $this->setResponse('code', 0);
        $this->setResponse('status', 'ok');
        $this->encodeResponse();
        $this->showResponse();
    }

}
