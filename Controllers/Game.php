<?php
declare(strict_types=1);
namespace Controllers;

use Models;

/**
 * @property Models\Game game
 * @property Models\Lobby lobby
 */
class Game extends Controller
{
    protected $GameResp; // массив ответов, отдаем клиенту
    protected $UserState; // информация о пользователе
    protected $LobbyState; // информация о лобби
    protected $GameState; // информация о текущей игре
    protected $MoveState; // информация о текущем ходе
    protected $HintState; // информация о подсказке
    protected $TimeState; // информация о времени
    protected $Const; // информация о константах в игре
    protected $dev; // информация для отладки
    protected $uid; // ид игрока
    protected $uname; // имя
    protected $eid; // ид противника
    protected $lid; // ид лобби
    protected $gid; // если создание и доступ к полю сделать через магические методы _get / _set, то empty() не работает с такими полями
    protected $gst; // состояние игры
    protected $gmh; // сосотояние подсказки
    protected $moveCntMy; // кол-во моих ходов
    protected $moveCntEn; // кол-во ходов противника
    protected $timePassedMove; // время хода
    protected $timePassedGame; // время бездействия в игре
    protected $isTimeRoundExpired; // булево, 1 - время на раунд закончилось; 0 - можно еще играть
    protected $isTimeGameExpired; // то же в отношении игры
    protected $LobbyController; // объект для работы с лобби
    protected $WarLogMy; // строка, запишется в tbl_WarLog
    protected $WarLogEn; // строка, запишется в tbl_WarLog
    protected $Score; // массив, содержит счет в игре
    protected $timeToRec; // время хода

    private $data = array(); // хранит данные магических методов
    const
        INTERVAL = 500000, // microsec
        HINT_NOT_USE = 0, // подсказка не исп-сь;
        HINT_SUCC = 254, // использовалась удачно
        HINT_UNSUCC = 255, // сгорела
        GAME_IN_START = 0, //
        GAME_IS_ON = 1, //
        GAME_OVER = 2,
        CAN_NOT_RUN_GAME = 0,//
        CAN_RUN_GAME = 1,//
        CAN_MOVE = 1,//
        CAN_NOT_MOVE = 0,//
        EMPTY_WEAPON = 0, // выбор оружия в начале игры, до первого хода
        SSE_RESPONSE_NORMAL= 128,
        VER = '1.5.0'; // версия игры

    public function __construct( $request = null ){
        if ( $request !== null ){
            parent::setCfg();
            $this->game = new Models\Game($this->config);
            $this->lobby = new Models\Lobby($this->config);
            $this->LobbyController = new Lobby($this->config);
            parent::__construct( $request );
        }
    }

    public function __get( $property ) {
        if ( array_key_exists( $property, $this->data ) ) {
            return $this->data[$property];
        } else {
            throw new \Exception('property '.$property.' is empty', 2048);
        }
    }

    public function __set( $property, $value ) {
        $this->data[$property] = $value;
    }

    protected function checkUser(){
        $jwt = $this->getRequestParam( $this->request, "jwt" );
        $this->uid = $this->getRequestParam($jwt, "id");
        if (isset($this->uid)) $this->uid = (int)$this->uid;
        $userCheck = (new User())->isAuthorizedUser( $jwt );
        if (!$userCheck) {
            $rc = $this->userNotAuthorized;
            throw new \Exception($rc['message'], $rc['code']);
        }
    }

    /**
     * проверка необходимых условий для начала игры
     *
     */
    protected function checkCond(){
        if ( empty( $this->LobbyState )){
            $rc = $this->UserNotInLobby;
            throw new \Exception($rc['message'], $rc['code']);
        }
        if ( empty( $this->lid )){
            $rc = $this->LobbyNotExist;
            throw new \Exception($rc['message'], $rc['code']);
        }
    }

    /**
     * проверка состояния игры
     *
     */
    protected function checkGame(){
        if ( !$this->eid ){
            $rc = $this->UserNotInLobby;
            throw new \Exception($rc['message'], $rc['code']);
        }
        if ( +$this->gst === $this::GAME_OVER ){
            $rc = $this->GameOver;
            throw new \Exception($rc['message'], $rc['code']);
        }
    }

    /**
     * ПАМЯТКА empty(), с полями определенными магическими методами не работает
     * Следующие значения воспринимаются как пустые:
     * "" (пустая строка)
     * 0 (целое число)
     * 0.0 (дробное число)
     * "0" (строка)
     * NULL
     * FALSE
     * array() (пустой массив)
     * $var; (переменная объявлена, но не имеет значения)
     *
     */

    /**
     *соберем данные игрока
     *
     */
    protected function getUserState(){
        $this->UserState = $this->game->getUserState($this->uid);
        $this->uname = $this->UserState['name'];
    }

    /**
     *получим данные лобби где идет игра
     *
     */
    public function getLobbyState($uid=null){
        if (!$uid){
            $uid = $this->uid;
        }
        $this->LobbyState = $this->game->getLobbyState( $uid );
        $this->lid = (int)$this->LobbyState['id'];
        $this->lidOwn = (int)$this->LobbyState['id_owner'];
        $this->eid = (int)$this->LobbyState['eid'];
        $this->enname = $this->LobbyState['enemy'];
        $this->lobbyTimer = (int)$this->LobbyState['timer']*1000000;
    }

    /**
     * обновляет время хода, исп-ся для переустановки времени в случае бездйствия игрока
     * @param $uid
     */
    protected function updMoveTime($uid){
        $this->setTimeToRec();
        $this->game->updMoveTime($uid, $this->timeToRec);
    }

    /**
     * записывает ход
     * @param $uid
     * @param $mid
     * @return bool
     */
    protected function setMove($uid, $mid){
        $this->setTimeToRec();
        return $this->game->setMove($uid, $mid, $this->timeToRec);
    }

    /**
     * готовит время хода для записи
     *
     */
    protected function setTimeToRec(){
        $this->timeToRec = microtime(true)*1000000;
    }

    /**
     *соберем всю возможную инфу о игре
     *
     */
    protected function getGameState(){

        $this->GameState = $this->game->getGameInLobby( $this->uid, $this->lid );
        if ( !empty( $this->GameState )){
            $this->gid = (int)$this->GameState['gid'];
            $this->gst = (int)$this->GameState['gst'];
            $this->gmh = (int)$this->GameState['hint'];
            if ( $this->gst === $this::GAME_IN_START || $this->gst === $this::GAME_OVER ){
                $this->GameState['isCanRun'] = $this->isCanRun();
            }
            if ( $this->gst === $this::GAME_IS_ON || $this->gst === $this::GAME_OVER ){
                // округлим в большую сторону поскольку раунды с ничьим результатом записываются одной строкой в tbl_rounds
                $this->GameState['totalRounds'] = ceil($this->game->getRounds($this->gid) / 2);
                $this->GameState['MyWins'] = $this->game->getWins($this->gid, $this->uid);
                $this->GameState['EnWins'] = $this->game->getWins($this->gid, $this->eid);
                $this->GameState['Enemy'] = $this->game->getEnemyFromHist($this->uid, $this->gid);
                $this->GameState['thisRound'] = $this->game->getName($this->game->getLastRoundWinner($this->gid));
            }
            if ($this->gst === 2) {
                $this->GameState['Result'] = $this->game->getResultFromHist($this->gid);
            }
        }
    }

    /**
     * получим состояние о подсказках в игре
     *
     */
    protected function getHintState(){
        $enchoice = $this->game->getChoice( $this->eid );
        $probability = $this->game->getHint( $this->uid, $this->lid );
        if ( $probability !== $this::HINT_NOT_USE &&
            $probability !== $this::HINT_SUCC &&
            $probability !== $this::HINT_UNSUCC ){
            $hint = $this->getHint( $enchoice, $probability );
        } else {
            $hint = $probability;
        }
        $this->HintState = $hint;
    }

    /**
     *соберем информацию о ходах и возможности совершить ход
     *
     */
    protected function getMoveState(){
        $this->MoveState = $this->game->getMove($this->uid);
        $this->MoveState['moveCntMy'] = $this->moveCntMy = (int)$this->game->getMoveCnt($this->uid);
        $this->MoveState['moveCntEn'] = $this->moveCntEn = (int)$this->game->getMoveCnt($this->eid);
        $this->MoveState['isCanMove'] = $this->game->getCanMove( $this->uid );

        $this->MoveState['timePassedMove'] = $this->timePassedMove;
        $this->MoveState['timePassedGame'] = $this->timePassedGame;
    }

    /**
     * получим временные данные необходимые для оценки состояния игры
     *
     */
    private function getTimeState(){
        $this->setTimeToRec();
        // время прошедшее с начала раунда
        $this->TimeState['timePassedMove'] = $this->timePassedMove = $this->game->getTmPassMove($this->uid, $this->timeToRec);
        // время прошедшее с момента поледнего действия игрока (время последнего выбора оружия)
        $this->TimeState['timePassedGame'] = $this->timePassedGame = $this->game->getTmPassGame($this->uid, $this->timeToRec);
        // есть ли еще время на ход?
        $this->TimeState['isTimeRoundExpired'] = $this->isTimeRoundExpired = $this->timePassedMove > $this->lobbyTimer;
        // не превышено ли время бездействия в игре?
        $this->TimeState['isTimeGameExpired'] = $this->isTimeGameExpired = $this->timePassedGame > $this->lobbyTimer * 3;
    }

    /**
     * Регистрация хода:
     * Авторизуемся
     * проверим наличие лобби
     * проверим наличие противника в лобби
     * определим наличие и статус игры
     * определим наличие и кол-во своих ходов уже сделанных в этой игре
     * определим наличие и кол-во ходов противника сделанных в этой игре
     * определим владельца лобби
     * если игры нет и автор запроса является хозяином лобби, то начинаем игру и пишем первый ход
     * если игра есть и статус игры не равен 2 и кол-во ходов автора менее или равно ходам противника,
     * то пишем ход в tbl_moves
     * и записываем номер хода в tbl_play
     *
     */
    public function _move_(){
        $this->getRequest($this->request[(string)__FUNCTION__]['data']);
        $this->checkUser();
        $this->getUserState();
        $this->getLobbyState();
        $this->getGameState();
        $this->checkGame();
        $this->getMoveState();
        $this->checkCond();
        // обновим данные для регистрации в on-line
        $time = (new myDateTime())->formatDB();
        $this->game->setOnLine($this->uid, $time);
        // сохраним пользовательские настройки
        $userData = $this->getRequestParam( $this->request, 'userCfg' );
        $this->saveCfg( $this->uid, $userData );

        $mychoice = (int)$this->getRequestParam( $this->request, "choice" ); // выбор игрока
        // есть ли возможность для хода?
        if (!$this->MoveState['isCanMove']){
            $rc = $this->SomethingWrong;
            throw new \Exception('You can\'t move' , $rc['code']);
        }
        $result = $this->setMove( $this->uid, $mychoice ); // запись выбора оружия
        $isCanMove = 0;
        if ($result) {
            $this->game->setMoveCnt( $this->uid, $this->moveCntMy+1, $isCanMove );
            $this->getMoveState();
            if ( $this->moveCntEn === $this->moveCntMy ) {
                // не первый ход в раунде
                $enchoice = $this->game->getChoice($this->eid);
                if (isset($enchoice)) {
                    if ($mychoice !== $enchoice) {
                        $moves = [$mychoice => $this->uid, $enchoice => $this->eid];
                        $res = $this->getResultRound($mychoice, $enchoice);
                        $roundWinnerId = $moves[$res['winner']]; // результат раунда (id победителя)
                        $roundLoserId = $moves[$res['loser']]; // (id проигравшего)
                    } else {
                        $roundWinnerId = $roundLoserId = 0;
                    }
                    $this->game->setRoundWinner($this->gid, $roundWinnerId); // запись победителя раунда
                    $this->game->setRoundLoser($this->gid, $roundLoserId); // запись проигрыша
                    // определим имя победителя
                    $str = '<span>%s</span> select the %s.<br><span>%s</span> select the %s.<br>%s.';
                    $path = sprintf($str, $this->uname, $this->Weapons($mychoice), $this->enname, $this->Weapons($enchoice),
                        $this->getDescRound($mychoice, $enchoice));
                    $this->setRoundScore($roundWinnerId, $roundLoserId);
                    $this->WarLogMy .= $path . $this->Score['my'];
                    $this->WarLogEn .= $path . $this->Score['en'];
                    // определим факт запроса подсказки, если таковой имеется, то
                    // зафиксируем это чтобы предотвратить повторные запросы
                    $hint = $this->game->getHint($this->uid, $this->lid);
                    if ( $hint !== $this::HINT_NOT_USE && $hint !== $this::HINT_SUCC && $hint !== $this::HINT_UNSUCC ){
                        $this->game->setHint($this->uid, $this->lid, $this::HINT_SUCC);
                    }
                }
                // обновим время на таймере противника
                $this->updMoveTime($this->eid);
                $this->game->setCanMove( $this->uid, 1 );
            } elseif ( $this->moveCntMy > $this->moveCntEn ){
                $str = '<span>%s</span> select the %s.';
                $event = sprintf($str, $this->uname, $this->Weapons( $mychoice ));
                $this->WarLogMy .= $event;
            } else {
                $str = '<span>%s</span> select the %s.';
                $event = sprintf($str, $this->uname, $this->Weapons( $mychoice ));
                $this->WarLogMy .= $event;
            }
            $this->game->setCanMove( $this->eid, 1 );
            $this->setGameOver();
            $this->setWarLog();
            // запрос состояния игры, возможно здесь излишен
            $rc = $this->success;
        } else {
            // если ход совершить не удалось
            $rc = $this->SomethingWrong; // 41
        }
        $data = $this->getGameInAjax( $this->request );
//        $this->setResponse('data', $data);
        $this->setResponse('status', "ok");
        $this->setResponse('code', $rc['code']);
        $this->encodeResponse();
        $this->showResponse();
    }

    /**
     *определим счет игры
     *
     */
    protected function setRoundScore($roundWinnerId, $roundLoserId){

        $roundWinner = $this->game->getNameByID( $roundWinnerId );
        $this->getGameState();
        if ($roundWinnerId !== $roundLoserId) {
            $str = '<br>Winner this round: <span>%s</span>.';
            $path = sprintf($str, $roundWinner);
        } else {
            $path = null;
        }
        $str2 = '<br>Total score: <span>%s</span> vs <span>%s</span>: %s:%s.';
        $myPath = sprintf($str2, $this->uname, $this->enname, $this->GameState['MyWins'] , $this->GameState['EnWins'] );
        $enPath = sprintf($str2, $this->enname, $this->uname, $this->GameState['EnWins'], $this->GameState['MyWins'] );
        $this->Score[ 'my' ] = $path . $myPath;
        $this->Score[ 'en' ] = $path . $enPath;
    }

    /**
     *завершим игру если для этого есть основания
     *
     */
    protected function setGameOver(){
        $gameWinnerId = null;
        $gameWinnerName = null;
        $this->getGameState();
        if ( $this->GameState['MyWins'] > 2){
            $gameWinnerId = $this->uid;
            $gameWinnerName = $this->uname;
        }
        if ( $this->GameState['EnWins'] >2 ){
            $gameWinnerId = $this->eid;
            $gameWinnerName = $this->enname;
        }
        // проверим не пора ли завершить игру
        if ( !empty( $gameWinnerId )){
            $str = '<br>Game over. The winner: <span>%s</span>.';
            $event = sprintf($str, $gameWinnerName);
            $this->WarLogMy .= $event;
            $this->WarLogEn .= $event;

            $this->game->setGameWinner($gameWinnerId, $this->gid);

            $this->game->setGameState($this->uid, $this->lid, $this::GAME_OVER);
            $this->game->setGameState($this->eid, $this->lid, $this::GAME_OVER);

            $this->game->setCanMove( $this->uid, $this::CAN_NOT_MOVE );
            $this->game->setCanMove( $this->eid, $this::CAN_NOT_MOVE );

            $this->setMove( $this->uid, $this::EMPTY_WEAPON );
            $this->setMove( $this->eid, $this::EMPTY_WEAPON );
        }
    }
    /**
     * Пишем историю битвы
     */
    public function setWarLog(){
        if (!empty($this->WarLogMy)){
            $this->game->addWarLog($this->uid, $this->gid, $this->WarLogMy);
        }
        if (!empty($this->WarLogEn)) {
            $this->game->addWarLog($this->eid, $this->gid, $this->WarLogEn);
        }
        $this->WarLogMy = $this->WarLogEn = null;
    }

    /**
     * Начать игру:
     * Добавим запись в tbl_hist_game, получим lastInsertID и запишем его в tbl_games
     */
    public function _run_(){
        $this->getRequest($this->request[(string)__FUNCTION__]['data']);
        $this->checkUser();
        $this->getUserState();
        $this->getLobbyState();
        $this->getGameState();

        $this->checkCond();
        if ($this->GameState['isCanRun']) {
            $this->prepGame();
        }
        $this->setWarLog();
        $rc = $this->success;
//        $data = $this->getGameInAjax( $this->request );
//        $this->setResponse('data', $data);
        $this->setResponse('status', "ok");
        $this->setResponse('code', $rc['code']);
        $this->encodeResponse();
        $this->showResponse();
    }

    /**
     * Установим начальные параметры игры:
     * Установим статус 0 в tbl_games для всех посетителей лобби
     * Обнулим кол-во ходов в таблице tbl_play
     * Обнулим выбор оружия в tbl_move
     *
     */
    public function prepGame(){

        $this->game->setGame($this->uid,$this->lid);
        $this->game->setGame($this->eid,$this->lid);

        $this->game->setGameState($this->uid, $this->lid, $this::GAME_IS_ON);
        $this->game->setGameState($this->eid, $this->lid, $this::GAME_IS_ON);
        $this->setMove( $this->uid,$this::EMPTY_WEAPON );
        $this->setMove( $this->eid,$this::EMPTY_WEAPON );

        $this->game->setMoveCnt( $this->uid, 0, $this::CAN_MOVE );
        $this->game->setMoveCnt( $this->eid, 0, $this::CAN_MOVE );
        // здесь определяется идентификатор (ключ) игры
        // определяет тот кто запускает игру
        $this->gid = $this->game->addGameHist( $this->uid, $this->eid );
        $this->game->setGameId($this->uid, $this->lid, $this->gid);
        $this->game->setGameId($this->eid, $this->lid, $this->gid);
        $this->game->purgeWarLog($this->uid); // ?
        $this->game->purgeWarLog($this->eid); // ?
        $str = '<span>%s</span> run this game.';
        $event = sprintf($str, $this->uname);
        $this->WarLogMy .= $event;
        $this->WarLogEn .= $event;
    }

    /**
     * проверка возможности для запуска игры
     *
     */
    protected function isCanRun(){
        if ( !empty( $this->eid )){
            if ($this->uid === $this->lidOwn) {
                return true;
            }
            if ($this->uid !== $this->lidOwn && $this->eid !== $this->lidOwn) {
                return true;
            }
            if ($this->eid !== $this->lidOwn) {
                return true;
            }
        }
        return false;
    }
    /**
     * возвращает случ. целое не равное выбору соперника
     *
     * @param $enchoice - реальный выбор противника
     *
     * @return int
     */
    private function generateHint(int $enchoice){
        $rnd = random_int(1, 5);
        if ($rnd !== $enchoice){
            return $rnd;
        }
        return $this->generateHint($enchoice);
    }

    /**
     * готовит и отдает массив из двух значений одно из которых верный выбор соперника
     *
     * @param $enchoice
     * @param $probability
     *
     * @return array
     */
    private function getHint($enchoice, $probability){
        $i = 1; $hint = [];
        while ($i<6){
            if ($i===$enchoice || $i===$probability){
                $hint[] = $i;
            }
            $i++;
        }
        return $hint;
    }

    /**
     * Запишем факт запроса подсказки в игре
     *
     */
    public function _getHint_(){
        $this->getRequest($this->request[(string)__FUNCTION__]['data']);
        $this->checkUser();
        $this->getUserState();
        $this->getLobbyState();
        $this->getGameState();
        $this->checkGame();
        $this->getMoveState();
        $this->checkCond();

        // обновим данные для регистрации в on-line
        $time = (new myDateTime())->formatDB();
        $this->game->setOnLine($this->uid, $time);
        // сохраним пользовательские настройки
        $userData = $this->getRequestParam( $this->request, 'userCfg' );
        $this->saveCfg( $this->uid, $userData );

        $result = null;
        $hint = null;
        $isHint = $this->game->getHint( $this->uid, $this->lid );
        if ( $isHint === $this::HINT_NOT_USE ){
            $this->eid = $this->game->getEnemyId($this->uid,$this->lid);
            $this->moveCntMy = $this->game->getMoveCnt( $this->uid ); // номер моего предыдущего хода
            $this->moveCntEn = $this->game->getMoveCnt( $this->eid ); // противника
            if ($this->moveCntEn > $this->moveCntMy){
                $enchoice = $this->game->getChoice($this->eid);
                $probability = $this->generateHint($enchoice);
                $result = $this->game->setHint($this->uid, $this->lid, $probability);
                $isHint = $this::HINT_SUCC;
            } else {
                $result = $this->game->setHint( $this->uid, $this->lid, $this::HINT_UNSUCC );
                $isHint = $this::HINT_UNSUCC;
            }
        } else {
            $hint = 'the hint already was';
        }
        if ( $result ){
            $message = '';
            $hintResult = '';
            if ( $isHint === $this::HINT_SUCC ){
                $message = 'You take the success hint';
                $hintResult = 'successfully';
            } elseif ( $isHint === $this::HINT_UNSUCC ){
                $message = 'oops, enemy is not move, the hint was gone';
                $hintResult = 'unsuccessfully';
            }
            $str = '<span>%s</span> is taken the hint, %s.';
            $event = sprintf($str, $this->uname, $hintResult);

            $this->game->addWarLog($this->uid, $this->gid, $message);
            $this->game->addWarLog($this->eid, $this->gid, $event);
        }
//        $data = $this->getGameInAjax( $this->request );
        $data['hint'] = $hint;
        $rc = $this->success;
        $this->setResponse('status', "ok");
//        $this->setResponse('data', $data);
        $this->setResponse('code', $rc['code']);
        $this->encodeResponse();
        $this->showResponse();
    }

    /**
     * сравниваем выборы,
     * при этом:
     * 1 - камень
     * 2 - бумага
     * 3 - ножницы
     * 4 - ящерица
     * 5 - спок
     *
     * @param $mc int
     * @param $ec int
     *
     * @return array
     */
    protected function getResultRound(int $mc, int $ec){
        $res = [ 'winner' => $mc, 'loser' => $ec ];
        if ($mc===1)
            if ($ec===3 || $ec===4)
                return $res;
        if ($mc===2)
            if ($ec===1 || $ec===5)
                return $res;
        if ($mc===3)
            if ($ec===2 || $ec===4)
                return $res;
        if ($mc===4)
            if ($ec===2 || $ec===5)
                return $res;
        return [ 'winner' => $ec, 'loser' => $mc ];
    }

    /**
     * сохраняет пользовательские настройки
     * @param null $uid
     * @param null $data
     */
    public function saveCfg( $uid, $data ){
        $arr = []; $tbl = 'tbl_cfg'; $prefix = 'cfg_';
        if (isset($data) && is_array($data) && $data){
            foreach ( $data as $key => $value ) {
                if ( $this->isExistTableColumn( $tbl, $prefix.$key )){
                    $arr['cfg_'.$key] = (int)(bool)$value;
                }
            }
            $this->game->setConf( $uid, $arr );
        }
    }

    /**
     * возвращает пользовательские настройки
     *
     */
    protected function getCfg(){
        $this->game->getConf( $this->uid );
    }

    /**
     * метод анализирует время игры и меняет состояние последней при необходимости
     * один из основных методов
     *
     */
    protected function setStateFromTime(){

        // если время хода истекло
        if ( $this->isTimeRoundExpired ){
            // разрешим ходить
            $this->game->setCanMove( $this->uid, 1 );
            $this->game->setCanMove( $this->eid, 1 );
            // обновим время в tbl_move
            $this->updMoveTime( $this->uid );
            $this->updMoveTime( $this->eid );
            // проигравший будет в любом случае
            if ($this->moveCntEn > $this->moveCntMy) {
                $this->game->setRoundLoser( $this->gid, $this->uid );
                // победитель только при условии хода
                $this->game->setRoundWinner($this->gid, $this->eid);
                $str = 'Round time has expired.<br><span>%s</span> is lose.';
                $event = sprintf($str, $this->uname);
                $this->setRoundScore($this->eid, $this->uid);

                $this->WarLogMy .= $event.$this->Score['my'];
                $this->WarLogEn .= $event.$this->Score['en'];
                $this->game->setMoveCnt( $this->uid, $this->moveCntMy+1, $this::CAN_MOVE );
            } else {
                // игру запустили, но никто не ходит
                $event = 'Round time has expired.<br>No one set move.<br>';
                $this->WarLogMy .= $event;
                $this->WarLogEn .= $event;
                $this->game->setRoundLoser( $this->gid, $this->uid );
                $this->game->setRoundLoser( $this->gid, $this->eid );
            }
            $this->setGameOver();
        }
        // если кем-либо превышено время бездействия в игре
        if ( $this->isTimeGameExpired ){

            $this->game->setCanMove( $this->uid, $this::CAN_NOT_MOVE );
            $this->game->setCanMove( $this->eid, $this::CAN_NOT_MOVE );

            $this->game->setGameState($this->uid, $this->lid, $this::GAME_OVER );
            $this->game->setGameState($this->eid, $this->lid, $this::GAME_OVER );

            $event = 'Three consecutive overdue rounds complete the game.<br>';
            $this->WarLogMy .= $event;
            $this->WarLogEn .= $event;
        }
        $this->setWarLog();
    }

    /**
     * авторизуемся
     * запросим у модели:
     * состояние всех лобби,
     * состояние текущего пользователя
     * состояние выбранного лобби (если есть)
     * состояние игры в которой участвует пользователь (если есть)
     * состояние ходов в игре пользователя (если есть)
     * состояние вопросов и ответов пользователей (если есть)
     * состояние истории (если есть)
     *
     * @return array
     */
    protected function getState(){

        // обновим данные для регистрации в on-line
        $time = (new myDateTime())->formatDB();
        $this->game->setOnLine($this->uid, $time);

        $this->LobbyState = $this->GameState = $this->HintState = $this->TimeState = $this->TimeState = null;

        $this->getUserState();
        $this->getLobbyState();
        $this->getGameState();
        $this->getMoveState();
        $this->getHintState();
        // если есть соперник
        if ($this->eid) {
            if (!$this->game->isUserOnLine($this->eid)) {
                // если пользователь не on-line, то выставим его из лобби
                if ( !empty( $this->LobbyState )){
                    // при отладке действие лучше отключать, тк пока XDebug стоит на паузе
                    // система может выставить оппонента из лобби
                    $this->LobbyController->leave($this->eid, $this->LobbyState['id']);
                }
            }
            // к игре все готово
            if ( !empty( $this->GameState ) && $this->gid !== $this::GAME_IN_START ){
                // игра идет
                if ( $this->gst === $this::GAME_IS_ON ){
                    $this->getTimeState();
                    $this->setStateFromTime();
                    $this->setWarLog();
                    // перезапросим данные, тк $this->setStateFromTime() меняет состояние
                    $this->getMoveState();
                    $this->getTimeState();
                    $this->getGameState();
                }
            }
        }
        $this->Const['interval'] = $this::INTERVAL / 1000000;
        $this->Const['ver'] = $this::VER;
        $this->GameResp['MoveState'] = $this->MoveState;
        $this->GameResp['WarLog'] = $this->game->getWarLog( $this->uid );
        $this->GameResp['Hint'] = $this->HintState;
        $this->GameResp['TimeState'] = $this->TimeState;
        $this->GameResp['LobbyList'] = $this->game->getLobbyList();
        $this->GameResp['UserState'] = $this->UserState;
        $this->GameResp['LobbyState'] = $this->LobbyState;
        $this->GameResp['GameState'] = $this->GameState;
        $this->GameResp['UserList'] = $this->game->getAllUsersOnLine();
        $this->GameResp['Const'] = $this->Const;
        return $this->GameResp;
    }

    /**
     * запрос состояния игры в AJAX
     * @param $request
     * @return array
     */
    public function getGameInAjax( $request ){
        $jwt = $this->getRequestParam( $request, "jwt" );
        $this->uid = $this->getRequestParam($jwt, "id");
        return $this->getState();
    }

    public function _getGameState_(){
        try {
            $transport = new SSE();
            $this->getRequest($this->request[(string)__FUNCTION__]['data']);
            $this->checkUser();
            while (1){
                $response = $this->getState();
                $response['code'] = $this::SSE_RESPONSE_NORMAL;
                $transport->run( $response );
                usleep( $this::INTERVAL );
                /**
                 * интервал в 250000:
                 * нагрузка: два клиента
                 * процессор Intel Corei5-6400
                 * загружен процессом mysql на 5-7%
                 *
                 * интервал в 1 сек
                 * загрузка ЦП: до 2,5%
                 *
                 * интервал в 0,5 сек
                 * загрузка ЦП: до 3,5%
                 */
            }
            $transport->off();
        } catch (\Exception $e){
            $transport->err($e);
        }
    }
}
