"use strict";
/**
 * Представления
 */

(function () {
    let
        Views = {},
        My = A.My;
    /**
     * возвращает шаблон компонента
     *
     * @constructor
     * @param {object} arg
     * @param {string} place
     * @return {template}
     * */
    Views.Place = function ( arg, place ){
        My.call( this ); // вызываем конструктор суперкласса
        this.prop = {
            id: arg.id,
            class: arg.class || '',
            html: arg.html || '',
            mark: function () {
                if (arg.mark) {
                    return Date.now()
                } else {
                    return null
                }
            }(),
            disabled: arg.disabled || '',
            parent: arg.parent,
            where: arg.where
        };
        this.place = place;
        this.template = "<div id={{id}} {{mark}} class={{class}}>{{html}}</div>";
    };
    /**
     * компонент что находиться в подвале основной области
     *
     * @constructor
     * @param {object} arg
     * @param {string} place
     *
     * @return {template}
     * */
    Views.GameState = function( arg ){
        My.call( this );
        /**
         * наконец-то, это первый компонент с работающим состоянием: 2017-12
         * */
        this.datasets = {
            show: (()=> arg.app==='on-line' ? 'gs_show' : 'gs_hide')()
        };
        this.prop = {
            id: arg.id || A.ID.game_state,
            app_state: arg.app || 'off-line',
            user_state: arg.user ? 'authorized' : 'log in, please',
            lobby_owner: arg.lobby && arg.lobby.owner ? arg.lobby.owner : 'no data',
            game_state: ()=>{
                if ( arg.game && +arg.game['gst'] === 1 ) return 'the game is on';
                if ( arg.game && +arg.game['gst'] === 2 ) return 'the game is over';
                return 'no game';
            },
            ver: arg.const && arg.const.ver || '',
            hidden: !arg.app ? 'hidden' : '',
            parent: arg.parent || null,
        };
        this.template = "<div id={{id}} class='game_state'>" +
            "<div class='caption'>App:</div><div class='value'>{{app_state}}</div>" +
            "<div class='caption'>User:</div><div class='value'>{{user_state}}</div>" +
            "<div class='caption'>Lobby owner:</div><div class='value'>{{lobby_owner}}</div>" +
            "<div class='caption'>Game:</div><div class='value'>{{game_state}}</div>" +
            "<div class='ver' {{hidden}}>v.{{ver}}</div>"+
            "</div>";
    };
    Views.LobbyListItem = function( arg, place ){
        My.call( this );
        let namePlayer = function (pl) {
            return (pl) ? '<div>'+pl+'</div>' : '<span>empty</span>';
        };
        this.prop = {
            id: A.ID.LobbiListItem + arg.id,
            idQuest: A.ID.LobbiListItemQuest + arg.id,
            idLobby: arg.id,
            name: arg.name,
            timer: arg.timer,
            owner: arg.owner,
            player1: function(){return namePlayer(arg['player1'])}(),
            player2: function(){return namePlayer(arg['player2'])}(),
            disabled: function () { return ( arg['player1'] && arg['player2'] ) ? 'disabled' : ''}(),
            state: (function(){
                if (arg['player1'] && arg['player2'] && arg.state === '1'){
                    return 'the game is on';
                }
                if ((arg['player1'] && !arg['player2']) || (!arg['player1'] && arg['player2'] )){
                    return 'in expectant of opponent';
                }
                if (arg['player1'] && arg['player2'] && (arg.state === '0' || arg.state === '2')){
                    return 'ready to start game';
                }
                return 'in anticipation of players';
            })()
        };
        this.template ="<div id={{id}} class='LobbyItem'>"+
            "<div class='lobby name a' {{disabled}} " +
            "data-a='{\"select\":\"lobby\",\"mode\":\"showQuestion\",\"data\":{\"id\":\"{{idQuest}}\"}}'>{{name}}</div>" +
            "<div id={{idQuest}}  class='quest'>" +
            "<div class='prompt'>" +
            "<div class='header'>Are you sure?</div>" +
            "<button class='btn resp a' data-a='{\"select\":\"lobby\",\"mode\":\"select\",\"data\":{\"id\":\"{{idLobby}}\"}}'>enter</button>" +
            "<button class='btn resp a' data-a='{\"select\":\"rem\",\"mode\":\"Question\"}'>escape</button>" +
            "</div></div>" +
            "<div class='players'>" +
            "<div class='player name'>Player#1</div>" +
            "<div class='player value'>{{player1}}</div>" +
            "<div class='player name'>Player#2</div>" +
            "<div class='player value'>{{player2}}</div>" +
            "</div>" +
            "<div class='state name'>State</div>" +
            "<div class='state value'>{{state}}</div>" +
            "<div class='timer name'>Timer, sec</div>" +
            "<div class='timer value'>{{timer}}</div>" +
            "<div class='owner name'>Owner</div>" +
            "<div class='owner value'>{{owner}}</div></div>";
        this.place = place;
    };
    /**
     * форма создания/выбора лобби
     *
     * @constructor
     * @param {object} arg
     * @param {object} place
     * @return {template}
     * */
    Views.LobbyForm = function( arg, place ){
        My.call( this );
        let cash = {};
        this.prop = {
            id: arg.id,
            limit_name: arg.limit_name,
            limit_timer: arg.limit_timer,
            lobby_list: A.ID.lobby_list,
            state: function () {
                if (arg.LList && arg.LList.length){
                    cash.hidden1 = 'hidden';
                    cash.hidden2 = '';
                    cash.active1 = '';
                    cash.active2 = 'active';
                } else {
                    cash.hidden1 = '';
                    cash.hidden2 = 'hidden';
                    cash.active1 = 'active';
                    cash.active2 = '';
                }
            }(),
            hidden1: cash.hidden1,
            hidden2: cash.hidden2,
            active1: cash.active1,
            active2: cash.active2,
            parent: arg.parent
        };
        this.place = place;
        this.template =
            "<div id='lobby_form' class='zoom-anim-dialog mfp-hide small-dialog width_1 lobby_form'>"+
            "<div id='lobby_acc' class='lobby_set'><div id='create' class='acc_header {{active1}}'>Create LOBBY</div>" +
            "<form {{hidden1}} class='ajax form_1 create_lobby' data-name='lobby' data-validate='1' data-show='1'>" +
            "<div class='lobby_input'><label class='text nolbl loe name'>" +
            "<span><i>&#xe6a0;</i>" +
            "<input type='text' class='textToSend a lobby' autofocus value='Test' name='name' required placeholder='Lobby name'/>" +
            "<span class='hint'>{{limit_name}}</span></span></label><label class='text nolbl loe time'>" +
            "<span>" +
            "<input type='text' name='timer' value='15' class='textToSend time' required placeholder='time limits for motion'/>" +
            "<i>&#xe63f;</i><span class='hint'>{{limit_timer}}</span></span></label></div>" +
            "<button type='submit' class='btn bt_create a resp' data-a='{\"select\":\"lobby\",\"mode\":\"create\"}'>CREATE</button>" +
            "</form>" +
            "<div class='select_lobby acc_header {{active2}}'>Select LOBBY</div>" +
            "<form class='ajax form_1' {{hidden2}}><div class='lobby_list' id={{lobby_list}}></div>" +
            "</div></div></div>";
    };
    /**
     * кнопка вызова формы выбора/создания лобби
     *
     * @constructor
     * @param {object} arg
     * @param {object} place
     * @return {template}
     * */
    Views.LobbySelect = function( arg ){
        My.call( this );
        this.datasets = {
            show: !!arg.user ? 'ls_show' : 'ls_hide'
            // width: '100%'
        };
        this.classes = {
            width: 'w100',
            class1: 'lobby',
            class2: 'lobby_form'
        };
        this.prop = {
            id: arg.id,
            html: (function(){
                let html;
                if (arg.lobby && arg.lobby['name']){
                    html = '<span>You occupied: </span>' + arg.lobby['name'] +
                        "<button class='exit a' " +
                        "data-a='{\"select\":\"lobby\",\"mode\":\"leave\"}'>Exit</button>";
                } else {
                    html = '<div class="l_select a" data-a=\'{\"select\":\"lobby\",\"mode\":\"get\"}\'><span>create / select</span> Lobby</div>';
                }
                return html;
            })(),
            class: function(){ return ( arg.lobby ) ? 'lobby_defined' : '' },
            hidden: function(){ return ( arg.user ) ? '' : 'hidden'}(),
            disabled: function(){ return ( !arg.lobby ) ? '' : 'disabled'}(),
            parent: arg.parent
        };
        this.template = "<div id={{id}} {{hidden}} class='{{disabled}} {{class}}' " +
            ">{{html}}</div>";
        this.setHtml();
    };
    /**
     * иконка с оружием
     *
     * @constructor
     * @param {object} arg
     * @return {template}
     * */
    Views.Weapon = function( arg ){
        My.call( this );
        this.prop = {
            id: arg.id,
            link: arg.i,
            html: function () {
                return "&#xf" + (Number)(254 + arg.i) +";";
            }(),
            clas: function () {
                // debugger
                return (arg.hint) ? 'hintWp' : '';
            },
            disabled: function () { return (arg.data && arg.data[ 'isCanMove' ]) ? '' : 'disabled'}(),
            parent: arg.parent
        };
        this.template = "<div id={{id}} class='wp a {{clas}}' {{disabled}} " +
            "data-a='{\"select\":\"game\",\"mode\":\"move\",\"data\":{\"choice\":\"{{link}}\"}}'><i>{{html}}</i></div>";
    };
    /**
     * кнопка вызова подсказки
     *
     * @constructor
     * @param {object} arg
     * @return {template}
     * */
    Views.Hint = function( arg ){
        My.call( this );
        this.prop = {
            id: arg.id,
            html: function () {
                let html;
                if (arg.hint){
                    if (arg.hint===255){
                        html = 'oops, enemy is not move, the hint was gone';
                    } else {
                        html = 'The hint is used<br><span>color highlighting shows opponent\'s move</span>';
                    }
                } else {
                    html = 'Take a hint';
                }
                return html;
            }(),
            hidden: function () { return ( (arg.move && arg.move['isCanMove'] === true) || (arg.game && arg.game['gst']=== '1' )) ? '' : 'hidden' }(),
            disabled: function () { return ( arg.hint ) ? 'disabled' : ''}(),
            parent: arg.parent
        };
        this.template = "<div id={{id}} class='moveHint a' {{hidden}} {{disabled}} " +
            "data-a='{\"select\":\"game\",\"mode\":\"getHint\"}'>{{html}}</div>";
    };
    /**
     * кнопка запуска игры
     *
     * @constructor
     * @param {object} arg
     * @return {template}
     * */
    Views.Run = function( arg ){
        My.call( this );
        this.prop = {
            id: arg.id,
            hidden: function () { return ( arg.game && arg.game['isCanRun'] === true) ? '' : 'hidden' }(),
            disabled: function () { return ( arg.game && arg.game['isCanRun'] === true) ? '' : 'disabled' }(),
            parent: arg.parent
        };
        this.template = "<div id={{id}} class='run a' {{hidden}} {{disabled}} " +
            "data-a='{\"select\":\"game\",\"mode\":\"run\"}'>START</div>";
    };
    /**
     * блок уведомления о возможности запуска игры только автором лобби
     *
     * @constructor
     * @param {object} arg
     * @return {template}
     * */
    Views.RunInf = function( arg ){
        My.call( this );
        this.prop = {
            id: arg.id,
            hidden: function () {
                return ( arg.lobby && (arg.lobby['id_owner'] === arg.lobby['eid']) && arg.game && arg.game['gst'] !== '1' ) ? '' : 'hidden'
            }(),
            parent: arg.parent
        };
        this.template = "<div id={{id}} class='RunInf' {{hidden}} >Only lobby owner can run this game</div>";
    };
    /**
     * таймер хода
     *
     * @constructor
     * @param {object} arg
     * @param place
     * @return {template}
     * */
    Views.Timer = function( arg, place ){
        My.call( this );
        this.prop = {
            id: arg.id,
            move: arg.move,
            hidden: function () {
                return (arg.time && !arg.time['isTimeRoundExpired']) ? '' : 'hidden'
            }(),
            step: arg.step,
            time: function(){
                return (arg.time) ? arg.time : 0
            }(),
            timer: function(){ return (arg.lobby && arg.lobby.timer) ? arg.lobby.timer : 0 }(),
            runit: function () {
                return (arg.move && arg.move[ 'isCanMove' ]) ?
                    timer( A.gid(arg.id), this.prop.time,
                        +this.prop.timer ) : null }
        };
        this.place = place;
        this.template = "<canvas id={{id}} {{hidden}} data-time={{time}} class='countdown'></canvas>";
    };
    /**
     * форма входа/регистрации пользователей
     *
     * @constructor
     * @param {object} arg
     * @param {object} place
     * @return {template}
     * */
    Views.AuthForm = function( arg ){
        My.call( this );
        this.prop = {
            id: arg.id,
            id_form: A.ID.auth_form,
            html: (function(){
                let html;
                // debugger
                if (localStorage.getItem(A.SEL.userData + 'name')) {
                    html ="<div class='user'><span>Hello, "+localStorage.getItem(A.SEL.userData + 'name')+
                        "!</span><button class='logout a' data-a='{\"select\":\"user\",\"mode\":\"logout\"}'>" +
                        " Exit</button></div>";
                } else {
                    html = "<a class='popup-with-move-anim auth_link a' data-a='{\"select\":\"common\",\"mode\":\"auth\"}'>Log in / Sign up </a>";
                    // console.log ( arg );
                    // debugger
                    // this.classes.tst = 'a333';
                }
                return html;
            }).call( this ),
            limit_name: arg.limit_name,
            limit_password: arg.limit_password,
            parent: arg.parent
        };
        this.template = "<div id={{id}} class='top_user_form'>{{html}}" +
            "<div id={{id_form}} class='login zoom-anim-dialog mfp-hide small-dialog'>"+
            "<div id='auth_acc' class='auth_acc'><div id='enter' class='acc_header active'>LOG IN</div><form class='ajax user-form login form_1' data-name='user' data-show='1'>" +
            "<label class='text nolbl lo'><span><i>&#xe65f;</i>" +
            "<input type='text' class='textToSend a' name='username' value='' " +
            "required placeholder='name' /></span></label><label class='pass nolbl lo'><span><i>&#xe67c;</i>" +
            "<input type='password' name='password' class='textToSend' value='' required placeholder='Password'/></span>" +
            "</label>" +
            "<button type='submit' class='btn bt_login a resp' data-a='{\"select\":\"user\",\"mode\":\"login\"}'>Log in &#xe701;</button>" +
            "</form><div class='acc_header'>SIGN UP</div><form hidden data-name='user' data-validate='1' data-show='1' class='ajax user-form register form_1'>" +
            "<div class='alert display_none'></div><label class='text nolbl lo'><span><i>&#xe65f;</i>" +
            "<input type='text' class='textToSend a' name='username' value='' required placeholder='Name'/>" +
            "<span class='hint'>{{limit_name}}</span></span></label><label class='pass nolbl lo'><span><i>&#xe67c;</i>" +
            "<input class='textToSend a' name='password1' type='password' value='' required placeholder=Password>" +
            "<span class='hint'>{{limit_password}}</span></span></label><label class='pass nolbl lo'><span><i>&#xe67c;</i>" +
            "<input class='textToSend a' name='password2' type='password' value='' required placeholder='Retype pass'/>" +
            "<span class='hint'>retype your password</span></span></label>" +
            "<button type='submit' class='btn bt_register a resp' data-a='{\"select\":\"user\",\"mode\":\"register\"}'>&#xe6a6; Register</button>" +
            "</div></div></div>";
    };
    /**
     * боковые кнопки управляющие видимостью боковых блоков
     *
     * @constructor
     * @param {object} arg
     * @param {object} place
     * @return {template}
     * */
    Views.Button = function( arg, place ){
        My.call( this );
        let cash = {};
        (function(){
            let data = arg.UserCfg;
            cash.isUserCfg = !A.Common.isEmptyObject( arg.UserCfg );
            if (arg.ind === 2){
                // debugger
                if (cash.isUserCfg && +data['wlog'] === 1) {
                    cash.link = '{\"select\":\"hide\",\"mode\":\"WarLog\"}';
                    cash.html = '&#xf10' + (arg.ind-2);
                } else {
                    cash.link = '{\"select\":\"show\",\"mode\":\"WarLog\"}';
                    cash.html = '&#xf10' + (arg.ind-1);
                }
            } else if (arg.ind === 1){
                if (cash.isUserCfg && +data['users'] === 1) {
                    cash.link = '{\"select\":\"hide\",\"mode\":\"UserList\"}';
                    cash.html = '&#xf10' + (arg.ind);
                } else {
                    cash.link = '{\"select\":\"show\",\"mode\":\"UserList\"}';
                    cash.html = '&#xf10' + (arg.ind-1);
                }
            }
        })();
        this.prop = {
            id: arg.id+arg.ind,
            link: cash.link,
            html: cash.html,
            where: 1 + arg.ind,
            clas: function () {
                return 'btn'+arg.ind
            },
            clasCase: function () {
                return 'bc'+arg.ind
            },
        };
        this.place = place;
        this.template = "<div id={{id}} {{disabled}} class='btnCase {{clasCase}} a' data-a='{{link}}'><div hidden class='{{clas}}'><i>{{html}}</i></div></div>";
    };
    /**
     * история игры
     *
     * @constructor
     * @param {object} arg
     * @param {object} place
     * @return {template}
     * */
    Views.WarLog = function( arg, place ){
        My.call( this );
        this.prop = {
            id: arg.id,
            gid: function (){ return (arg.game) ? arg.game.gid : '' }(),
            where: arg.where,
            hidden: function () { return ( arg.UserCfg && +arg.UserCfg['wlog'] === 1) ? '' : 'hidden' }(),
        };
        this.place = place;
        this.template = "<div id={{id}} {{hidden}} class='WarLog'><div>" +
            "<div class='header'>" +
            "<i hidden class='purgeLog a' " +
            "data-a='{\"select\":\"common\",\"mode\":\"purgeLog\",\"data\":{\"gid\":\"{{gid}}\"}}'>&#xe6fd;</i>" +
            "<span class='hint'>purge old log</span>War Log</div>" +
            "<div id='log'></div>" +
            "</div></div>";
    };
    /**
     * контейнер для записей из истории игры, нужен чтобы отделить WarLog от отдельных записей истории,
     * без него, событие скрытия/отображения боковой панели полностью очищает WarLog
     *
     * @constructor
     * @param {object} arg
     * @param {string} place
     * @return {template}
     * */
    Views.Log = function ( arg ){
        My.call( this );
        this.prop = {
            id: arg.id,
            class: arg.class || '',
            mark: function(){
                if (!arg.data || +arg.data.gid === 0){
                    return null;
                } else {
                    return ''
                }
            },
            parent: arg.parent
        };
        this.template = "<div id={{id}} class={{class}} {{mark}}></div>";
    };
    Views.LobbyList = function ( arg ){
        My.call( this );
        this.prop = {
            id: arg.id,
            parent: arg.parent
        };
        this.template = "<div id={{id}} class='lobby_list'></div>";
    };
    /**
     * отдельно взятая запись в истории игры
     *
     * @constructor
     * @param {object} arg
     * @param {object} place
     * @return {template}
     * */
    Views.WarLogItem = function( arg, place ){
        My.call( this );
        this.prop = {
            id: A.ID.WarLogItem + arg.id,
            time: arg.time,
            gid: arg.gid,
            event: arg.event,
            parent: arg.parent
        };
        this.place = place;
        this.template = "<div id={{id}} class='WarLogItem' data-gid='{{gid}}'>" +
            "<div class='item'><span class='time'>{{time}}</span><span class='event'>{{event}}</span></div>" +
            "</div>";
    };
    /**
     * форма пользователей on-line
     *
     * @constructor
     * @param {object} arg
     * @param {object} place
     * @return {template}
     * */
    Views.UserListCase = function( arg, place ){
        My.call( this );
        this.prop = {
            id: arg.id,
            where: arg.where,
            hidden: function () { return ( arg.UserCfg && +arg.UserCfg['users'] === 1) ? '' : 'hidden' }(),
        };
        this.place = place;
        this.template = "<div id={{id}} {{hidden}} class='UserList'><div>" +
            "<div class='header'>Users on-line</div>" +
            "<div id='UserList'></div>" +
            "</div></div>";
    };
    /**
     * компонент, назначение тоже что и у Log
     *
     * @constructor
     * @param {object} arg
     * @param {object} place
     * @return {template}
     * */
    Views.UserList = function( arg, place ){
        My.call( this );
        this.prop = {
            id: arg.id,
            html: function () {
                if (arg.data) {
                    // debugger
                    let arr = arg.data, html = '';
                    for (let i = 0; i < arr.length; i++) {
                        html = html + '<div>'+arr[i]['uname']+'</div>'
                    }
                    return html;
                } else return 'no users';
            },
        };
        this.place = place;
        this.template = "<div id={{id}} class='log'>{{html}}</div>";
    };
    /**
     * отображает счет игры
     *
     * @constructor
     * @param {object} arg
     * @param {object} place
     * @return {template}
     * */
    Views.Score = function( arg, place ){
        My.call( this );
        let cash = {};
        this.prop = {
            id: arg.id,
            state: function () {
                if ( arg.game && (+arg.game['gst'] === 1 || +arg.game['gst'] === 2)){
                    if (arg.game && arg.game.MyWins) cash.MyWins =arg.game.MyWins;
                    if (arg.game && arg.game.EnWins) cash.EnWins = arg.game.EnWins;
                    if (arg.user && arg.user.name) cash.user = arg.user.name;
                    if (arg.game && arg.game['Enemy']) cash.enemy = arg.game['Enemy'];
                }
            }(),
            MyWins: cash.MyWins || 0,
            EnWins: cash.EnWins || 0,
            user: cash.user || '',
            enemy: cash.enemy || '',
            template:  function () {
                let tpl = '';
                if ( arg.game && (+arg.game['gst'] === 1 || +arg.game['gst'] === 2) && cash.enemy ){
                    tpl =
                        "<div class='name'><span class='player'>{{user}}</span>" +
                        "<span class='vs'>vs</span><span class='player'>{{enemy}}</span></div>"+
                        "<div class='digit'><span>{{MyWins}}</span><span class='colon'>:</span><span>{{EnWins}}</span></div>";
                } else {
                    tpl = "<div class='sel_w'>choose a weapon</div>";
                }
                return tpl;
            }()
        };
        this.place = place;
        this.template = "<div id={{id}} class='score'>{{template}}</div>";
        this.setHtml();
    };
    /**
     * отображает номер раунда и итог игры
     *
     * @constructor
     * @param {object} arg
     * @param {object} place
     * @return {template}
     * */
    Views.Rounds = function( arg, place ){
        My.call( this );
        let cash = {};
        this.prop = {
            id: arg.id,
            html: function () {
                if ( arg.game && (+arg.game['gst'] === 1 || +arg.game['gst'] === 2)){
                    let rds = arg.game['totalRounds'];
                    cash.hidden = '';
                    return ( arg.game && rds ) ? rds : 0;
                } else {
                    cash.hidden = 'hidden';
                    return '';
                }
            }(),
            template: function () {
                if (arg.game) {
                    if ((arg.game['MyWins'] > 2 || arg.game['EnWins'] > 2) || arg.game['Result']) {
                        return "<span {{hidden}} class='result'>{{result}}</span>";
                    }
                }
                return '';
            },
            state: function () {
                if (arg.game) {
                    // debugger
                    let res = arg.game['Result'];
                    if (res && res['winner'] !== null) {
                        cash.result = 'The winner is:<span>' + res['winner'] + '</span>';
                        return;
                    }
                    if (res && res['loser'] !== null) {
                        cash.result = 'The loser is:<span>' + res['loser'] + '</span>';
                    }
                }
            }(),
            result: cash.result || 'no winner',
            hidden: cash.hidden
        };
        this.place = place;
        this.template = "<div id={{id}} {{hidden}} class='rounds'>Total rounds:<span class='dig'>{{html}}</span>{{template}}</div>";
        this.setHtml();
    };
    /**
     * отображает текущий ход игрока
     *
     * @constructor
     * @param {object} arg
     * @param {object} place
     * @return {template}
     * */
    Views.Move = function( arg, place ){
        My.call( this );
        this.prop = {
            id: arg.id,
            html:  function () {
                return (arg.move) ? arg.move['moveCntMy'] : 0;
            }(),
            hidden: function () { return ( arg.move && ( arg.game && (+arg.game['gst'] === 1 || +arg.game['gst'] === 2 ))) ? '' : 'hidden' }(),
        };
        this.place = place;
        this.template = "<div id={{id}} {{hidden}} class='move'><span>Move: </span>{{html}}</div>";
    };
    /**
     * отображает информацию о погрешности таймера обратного отсчета
     *
     * @constructor
     * @param {object} arg
     * @param {object} place
     * @return {template}
     * */
    Views.ContdwnInf = function( arg, place ){
        My.call( this );
        this.prop = {
            id: arg.id,
            html:  function () {
                return (arg.data) ? arg.data['interval'] : '';
            }(),
            hidden: function () { return ( arg.game && arg.game['gst']=== '1' ) ? '' : 'hidden' }(),
        };
        this.place = place;
        this.template = "<div id={{id}} {{hidden}} class='ContdwnInf'>" +
            "the permissible error of the countdown timer is <span>{{html}}</span> seconds</div>";
    };
    /**
     * рисует окружность длиной пропорциональной оставшемуся времени для хода
     *
     * */
    function timer (el, time, timer){
        let x, y;
        x = y = 250;
        let color = 'lightblue';
        let draw = new ExpangeCanvas(el, x, y);
        // console.log(time + ' --- ' + timer);
        draw.Circle(x / 2, y / 2, x * 0.4 - 8, 10, color, [5, 5], null, (timer - time) * 100 / timer);
        // draw.Circle(x / 2, y / 2, x * 0.4 - 8, 10, color, [5, 5], null, (timer-time)*100/timer);
    }
    /**
     * рисование
     *
     * @param {HTMLElement} canvasid
     * @param {number} width
     * @param {number} height
     * */
    function ExpangeCanvas( canvasid, width, height ){
        width = width || 300;
        height = height || 150;
        this.canvas = canvasid;
        this.obCanvas = null;
        if ( this.canvas !== null ){
            this.canvas.width = width;
            this.canvas.height = height;
            this.obCanvas = this.canvas['getContext']('2d');
        }
        this.Line = function ( x1, y1, x2, y2, linewidth, strokestyle ){
            if (this.obCanvas === null) return;
            this.obCanvas.beginPath();
            this.obCanvas.lineWidth = linewidth;
            this.obCanvas.strokeStyle = strokestyle;
            this.obCanvas.moveTo(x1, y1);
            this.obCanvas.lineTo(x2, y2);
            this.obCanvas.stroke();
        };
        this.Circle = function ( x, y, radius, linewidth, strokestyle, lineDash, fillstyle, val ){
            if (this.obCanvas === null) return;
            fillstyle = fillstyle || 'transparent';
            this.obCanvas.beginPath();
            this.obCanvas.arc(x, y, radius, 0, 2 * Math.PI * 0.01 * val, false);
            this.obCanvas.lineWidth = linewidth;
            this.obCanvas['setLineDash']( lineDash || [0,0] );
            this.obCanvas.strokeStyle = strokestyle;
            this.obCanvas.fillStyle = fillstyle;
            this.obCanvas.fill();
            this.obCanvas.stroke();
        };
        this.Text = function ( text, x, y, font, color ){
            if (this.obCanvas === null) return;
            this.obCanvas.font = font;
            this.obCanvas.fillStyle = color;
            this.obCanvas.textAlign = 'center';
            this.obCanvas.textBaseline = "middle";
            this.obCanvas.fillText(text, x, y);
        };
        this.Text2 = function ( x, y ){
            if (this.obCanvas === null) return;
            this.obCanvas.font = '25px Glyphicons Halflings';
            this.obCanvas.fillStyle = '#95E2FF';
            this.obCanvas.textAlign = 'center';
            this.obCanvas.textBaseline = "middle";
            console.log('\e013');
            this.obCanvas.fillText('\e013', x, y);
        };
        this.SetBgColor = function ( bgcolor ){
            if (this.obCanvas === null) return;
            this.obCanvas.fillStyle = bgcolor;
            this.obCanvas.fillRect(0, 0, this.obCanvas.canvas.width, this.obCanvas.canvas.height);
        }
    }
    // определим прототип для всех представлений
    for (let i in Views){
        if (Views.hasOwnProperty(i)){
            Views[i].prototype = Object.create( My.prototype );
        }
    }

    A.Views = Views;

})();

