<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8"/>
    <meta name="description" content="Game on-line rock-paper-scissors-lizard-spock">
    <meta name="keywords" content="Game, online, on-line, rock, paper, scissors, lizard, spock">
    <link rel="shortcut icon" href="favicon.png" type="image/png">
    <link rel="stylesheet" href="/css/style.css">
</head>

<main>
    <div id="content" class="content">
        <div id="game" class="game">
            <div id="header"></div>
            <div id="body" class="body">
                <div id="score"></div>
                <div id="weapon_case" class="weapon_case">
                    <div id="move"></div>
                    <div id="ContdwnInf"></div>
                    <div id="wi1"></div>
                    <div id="wi2"></div>
                    <div class="fill"></div>
                    <div id="wi3"></div>
                    <div class="fill"></div>
                    <div id="wi4"></div>
                    <div id="wi5"></div>
                </div>
                <div id="rounds"></div>
            </div>
            <div id="game_state"></div>
        </div>
    </div>
</main>
<script>
    A = {};

    (function(){
        const
            userAgent = navigator.userAgent,
            txt = 'text/javascript',
            app = 'application/javascript;version=1.8';

        (function () {
            if ( ~userAgent.indexOf( 'AppleWebKit' )) return A.typeScript = txt;
            if ( ~userAgent.indexOf( 'Firefox' )) return A.typeScript = app;
        })();
    })();
</script>

<script type="text/javascript" src="/scripts/prime.js"></script>

<link rel="stylesheet" href="/css/fonts-original.css">
<link rel="stylesheet" href="/css/magnific-popup.css">

</html>