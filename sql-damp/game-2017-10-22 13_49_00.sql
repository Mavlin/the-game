-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.23 - MySQL Community Server (GPL)
-- Операционная система:         Win32
-- HeidiSQL Версия:              9.4.0.5174
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных game
CREATE DATABASE IF NOT EXISTS `game` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `game`;

-- Дамп структуры для процедура game.purgeUserList
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `purgeUserList`(
	IN `timeout` INT



)
BEGIN
#declare timeout int;
SET time_zone = '+00:00';
delete from tbl_users_online where TIMESTAMPDIFF(SECOND, uol_time, now())>timeout; 
END//
DELIMITER ;

-- Дамп структуры для событие game.purgeUserList
DELIMITER //
CREATE DEFINER=`root`@`localhost` EVENT `purgeUserList` ON SCHEDULE EVERY 2 SECOND STARTS '2017-10-06 05:34:39' ON COMPLETION PRESERVE ENABLE DO BEGIN
CALL `purgeUserList`(2);
END//
DELIMITER ;

-- Дамп структуры для таблица game.tbl_cfg
CREATE TABLE IF NOT EXISTS `tbl_cfg` (
  `cfg_id` int(11) unsigned NOT NULL,
  `cfg_wlog` tinyint(3) unsigned DEFAULT '0' COMMENT 'состояние панели истории',
  `cfg_users` tinyint(4) unsigned DEFAULT '0' COMMENT 'состояние списка пользователей',
  UNIQUE KEY `Key` (`cfg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='хранит  настройки игроков';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица game.tbl_fields_limit
CREATE TABLE IF NOT EXISTS `tbl_fields_limit` (
  `tfl_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tfl_form` enum('login','user','lobby') DEFAULT NULL COMMENT 'список форм, поле для отбора',
  `tfl_name` tinytext NOT NULL,
  `tfl_regexp` tinytext,
  `tfl_caption` tinytext,
  `tfl_min_len` smallint(5) unsigned DEFAULT NULL COMMENT 'минимальная длина поля',
  `tfl_max_len` smallint(5) unsigned DEFAULT NULL COMMENT 'максимум длина поля',
  `tfl_explain` char(50) DEFAULT NULL COMMENT 'поянение для разработчика',
  KEY `key` (`tfl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='Таблица органичений на поля';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица game.tbl_games
CREATE TABLE IF NOT EXISTS `tbl_games` (
  `gm_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `gms_pl_id` int(11) unsigned DEFAULT '0',
  `gms_lb_id` int(10) unsigned DEFAULT '0',
  `gms_game_id` int(10) unsigned DEFAULT '0' COMMENT 'пишем сюда id из hist',
  `gms_state` tinyint(4) unsigned DEFAULT '0' COMMENT '0 - готова к старту; 1 - игра идет; 2 - завершена',
  `gms_first` int(10) unsigned DEFAULT '0' COMMENT 'id первого походившего игрока',
  `gms_hint` tinyint(3) unsigned DEFAULT '0' COMMENT 'неверная подсказка; 0-не исп-сь; 254-использовалась; 255-сгорела',
  PRIMARY KEY (`gm_id`),
  UNIQUE KEY `key` (`gms_pl_id`,`gms_lb_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5535 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица game.tbl_hint
CREATE TABLE IF NOT EXISTS `tbl_hint` (
  `hnt_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hnt_pl` int(11) unsigned DEFAULT '0',
  `hnt_wid` tinyint(4) unsigned DEFAULT '0' COMMENT 'weapon id',
  PRIMARY KEY (`hnt_id`),
  UNIQUE KEY `key` (`hnt_pl`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='текущий, последний выбор оружия игрока';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица game.tbl_hist_game
CREATE TABLE IF NOT EXISTS `tbl_hist_game` (
  `hgm_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hgm_pl_1` int(11) unsigned DEFAULT '0' COMMENT 'id игрока 1',
  `hgm_pl_2` int(10) unsigned DEFAULT '0' COMMENT 'id игрока 2',
  `hgm_winner` int(10) unsigned DEFAULT '0' COMMENT 'id победителя',
  `hgm_loser` int(10) unsigned DEFAULT '0' COMMENT 'id проигравшего',
  `hgm_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'время хода',
  PRIMARY KEY (`hgm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=878 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='хранит список всех игр, исп-ся для идентефикации  и учета результатов игр';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица game.tbl_limit_data
CREATE TABLE IF NOT EXISTS `tbl_limit_data` (
  `lmd_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lmd_field` int(11) unsigned DEFAULT '0' COMMENT 'id поля',
  `lmd_value` tinytext COMMENT 'значение, используется для валидации',
  PRIMARY KEY (`lmd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='собственно ограничения для полей';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица game.tbl_limit_fields
CREATE TABLE IF NOT EXISTS `tbl_limit_fields` (
  `fld_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fld_name` tinytext NOT NULL COMMENT 'имя поля для валидации',
  PRIMARY KEY (`fld_id`),
  UNIQUE KEY `uniq` (`fld_name`(100))
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='список полей для валидации';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица game.tbl_limit_forms
CREATE TABLE IF NOT EXISTS `tbl_limit_forms` (
  `frm_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `frm_name` tinytext NOT NULL COMMENT 'имя формы для валидации',
  PRIMARY KEY (`frm_id`),
  UNIQUE KEY `uniq` (`frm_name`(100))
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='список форм для валидации';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица game.tbl_limit_hints
CREATE TABLE IF NOT EXISTS `tbl_limit_hints` (
  `lmh_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lmh_form` int(11) unsigned DEFAULT '0' COMMENT 'id формы',
  `lmh_field` int(11) unsigned DEFAULT '0' COMMENT 'id поля',
  `lmh_type` int(11) unsigned DEFAULT '0',
  `lmh_hint` tinytext COMMENT 'подсказка',
  PRIMARY KEY (`lmh_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='подсказки для пользователя об ограничениях на поля';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица game.tbl_limit_types
CREATE TABLE IF NOT EXISTS `tbl_limit_types` (
  `tps_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tps_name` tinytext NOT NULL COMMENT 'Тип данных поля для валидации',
  PRIMARY KEY (`tps_id`),
  UNIQUE KEY `uniq` (`tps_name`(100))
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='типы данных для валидации';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица game.tbl_lobby
CREATE TABLE IF NOT EXISTS `tbl_lobby` (
  `lb_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lb_name` tinytext NOT NULL,
  `lb_timer` smallint(6) NOT NULL DEFAULT '5',
  `lb_owner` int(11) unsigned NOT NULL COMMENT 'u_id',
  `lb_time_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`lb_id`),
  UNIQUE KEY `Key` (`lb_name`(100))
) ENGINE=InnoDB AUTO_INCREMENT=530 DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица game.tbl_lobby_use
CREATE TABLE IF NOT EXISTS `tbl_lobby_use` (
  `lu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lu_id_lb` int(10) unsigned DEFAULT NULL,
  `lu_id_pl` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`lu_id`),
  UNIQUE KEY `Key` (`lu_id_pl`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица game.tbl_move
CREATE TABLE IF NOT EXISTS `tbl_move` (
  `mv_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mv_pl` int(11) unsigned DEFAULT '0',
  `mv_wid` tinyint(4) unsigned DEFAULT '0' COMMENT 'weapon id',
  `mv_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'время записи последнего хода, записывать может getState',
  `mv_chm` timestamp NULL DEFAULT NULL COMMENT 'время для сравнения текущего и времени последней активности игрока',
  `mv_utm` bigint(20) DEFAULT '0' COMMENT 'время записи последнего хода, записывать может getState',
  `mv_chmv` bigint(20) DEFAULT '0' COMMENT 'время для сравнения текущего и времени последней активности игрока',
  PRIMARY KEY (`mv_id`),
  UNIQUE KEY `key` (`mv_pl`)
) ENGINE=InnoDB AUTO_INCREMENT=2778 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='текущий, последний выбор оружия игрока';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица game.tbl_penalty
CREATE TABLE IF NOT EXISTS `tbl_penalty` (
  `pn_pid` int(11) unsigned NOT NULL COMMENT 'id player',
  `pn_time` datetime DEFAULT NULL COMMENT 'окончание времени штрафа',
  UNIQUE KEY `Key` (`pn_pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица game.tbl_play
CREATE TABLE IF NOT EXISTS `tbl_play` (
  `gm_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `gm_pl_id` int(11) unsigned DEFAULT '0' COMMENT 'id игрока',
  `gm_mv_cnt` tinyint(4) unsigned DEFAULT '0' COMMENT 'номер хода',
  `gm_canMove` tinyint(4) unsigned DEFAULT '0' COMMENT 'возможность хода, 0 - нет; 1 - да',
  `gm_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'время хода',
  PRIMARY KEY (`gm_id`),
  UNIQUE KEY `key` (`gm_pl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3286 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица game.tbl_rounds
CREATE TABLE IF NOT EXISTS `tbl_rounds` (
  `rnd_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rnd_game` int(11) unsigned DEFAULT '0' COMMENT 'id игры',
  `rnd_winner` int(10) unsigned DEFAULT '0' COMMENT 'id победителя; 0 ничья',
  `rnd_loser` int(10) unsigned DEFAULT '0' COMMENT 'id проигравшего',
  `hgm_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'время хода',
  PRIMARY KEY (`rnd_id`),
  UNIQUE KEY `key` (`rnd_loser`,`hgm_time`)
) ENGINE=InnoDB AUTO_INCREMENT=5667 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='список раундов с их результатами';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица game.tbl_users
CREATE TABLE IF NOT EXISTS `tbl_users` (
  `u_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `u_login` tinytext NOT NULL,
  `u_pass` varchar(128) DEFAULT NULL,
  `u_salt` varchar(50) DEFAULT NULL,
  `u_win` smallint(5) unsigned DEFAULT NULL COMMENT 'победы',
  `u_lose` smallint(5) unsigned DEFAULT NULL COMMENT 'проигрыши',
  `u_hint` smallint(5) unsigned DEFAULT NULL COMMENT 'подсказки',
  `u_date_created` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`u_id`),
  UNIQUE KEY `KeyUniq` (`u_id`),
  UNIQUE KEY `Uniq2` (`u_login`(100))
) ENGINE=InnoDB AUTO_INCREMENT=331 DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица game.tbl_users_online
CREATE TABLE IF NOT EXISTS `tbl_users_online` (
  `uol_uid` int(11) DEFAULT NULL,
  `uol_time` datetime DEFAULT NULL,
  UNIQUE KEY `key` (`uol_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='список пользователей on-line';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица game.tbl_warlog
CREATE TABLE IF NOT EXISTS `tbl_warlog` (
  `wl_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `wl_pid` int(11) unsigned DEFAULT '0' COMMENT 'id игрока',
  `wl_gid` int(11) unsigned DEFAULT '0' COMMENT 'id game',
  `wl_event` text COMMENT 'id игрока 2',
  `wl_time` datetime DEFAULT NULL COMMENT 'время хода',
  PRIMARY KEY (`wl_id`),
  UNIQUE KEY `key` (`wl_time`,`wl_pid`)
) ENGINE=InnoDB AUTO_INCREMENT=8531 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='хранит список всех игр, исп-ся для идентефикации  и учета результатов игр';

-- Экспортируемые данные не выделены.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
