"use strict";
(function () {
    let
        exp = {},// содержит имена кодов для клиента
        actions,
        arg = {}; // глобальный хранитель и переносчик данных, если это MVC, то это модель

    if ( !window.A ) window.A = {};

    arg.cs = {};// коды состояний
    /**
     * возвращает ссылку на элемент
     *
     * @param {string} a
     * @return {Element}
     */
    function gid( a ){
        return document.getElementById( a )
    }
    /**
     * содержит CSS селекторы
     *
     * @return {string}
     */
    let SEL = Object.create( null );// объект хранит селекторы блоков, что исп-ся в различных модулях ( C = conf )
    SEL = {
        userData: 'user#', // префикс ключей пользов. данных в LocalStorage
        userCfg: 'userCfg#', // префикс ключей пользов. настроек в LocalStorage
        authForm: '.top_user_form',
        lobbyForm: '.lobby_form',
        textToSend: 'textToSend', // класс текстовых полей для отправки
        login: '.login', // селектор формы входа
        active: 'active', // класс, селектор активных ссылок и мехов акктордиона
        transp: '.trans', // селектор прозрачного текста
        err: 'err', // класс уведомлений об ошибках
    };
    /**
     * содержит цвета
     *
     * @return {string}
     */
    let Color = {
        text: '#056ab1'
    };
    /**
     * содержит сокращения для местоположения нового элемента
     *
     * @return {string}
     */
    let Where = Object.create( null );
    Where = {
        1: 'beforeBegin',
        2: 'afterBegin',
        3: 'beforeEnd',
        4: 'afterEnd'
    };

    /**
     *
     * @constructor
     * */
    let Common = function(){};

    /**
     * select element by css
     *
     * @param {string} s
     * @param {html} context
     * @return {object} this
     * */
    Common.prototype.select = function( s, context ){
        if (s) {
            let arr = (context || document).querySelectorAll(s);
            if ( arr.length ) this.div = arr;
            return this;
        }
    };
    /**
     * remove data from LS
     *
     * @param {string} prefics
     * */
    Common.prototype.delLocStorData = function( prefics ){
        let o = {};
        for (let i = 0; i < localStorage.length; i++) {
            if (localStorage.key(i).indexOf( prefics ) === 0) {
                o[localStorage.key(i)] = localStorage.getItem(localStorage.key(i));
            }
        }
        for (let key in o){
            if (o.hasOwnProperty(key))
                localStorage.removeItem(key)
        }
    };
    /**
     * get data from LS
     *
     * @param {string} prefics
     * */
    Common.prototype.getLocStorData = function( prefics ){
        let o = {};
        for (let i = 0; i < localStorage.length; i++) {
            if (localStorage.key(i).indexOf( prefics ) === 0) {
                o[localStorage.key(i).substring(prefics.length)] = localStorage.getItem(localStorage.key(i));
            }
        }
        return o;
    };

    /**
     * проверить объект на пустоту
     *
     * @param {object} obj
     * */
    Common.isEmptyObject = function(obj){
        for (let i in obj) {
            if (obj.hasOwnProperty(i)) {
                return false;
            }
        }
        return true;
    };
    /**
     * show loader здесь не исп-ся
     *
     * param {string} sel
     *
     * */
    Common.prototype.loader = function( sel ){
        let el = gid( sel ) || sel,
            classLoader = 'loader';
        let show = function (){
            let flagLoader = el.dataset.loader;
            let style = getComputedStyle( el );
            el.classList.add(classLoader);
            el.classList.remove('a');
            el.style.minWidth = parseFloat(style.width) + 'px';
            el.style.minHeight = parseFloat(style.height) + 'px';
            el.style.maxWidth = parseFloat(style.width) + 'px';
            el.style.maxHeight = parseFloat(style.height) + 'px';
            el.style.fontFamily = "'Segoe UI', FontAwesome, icomoon, serif";

            el.dataset.temp = el.innerHTML;
            if ( flagLoader === 0 || flagLoader === undefined ){
                el.dataset.loader = 1;
                el.innerHTML = ('<span></span>');
                let char = '&#xe70a;';
                let interval = 300;
                let i = 0;
                let loadr = el.querySelector('span');
                setTimeout(function run() {
                    if (loadr) {
                        loadr.insertAdjacentHTML('beforeEnd', char);
                        i += 1;
                        if (i > 3) {
                            loadr.innerHTML = '';
                            i = 0;
                        }
                        setTimeout( run, interval );
                    }
                }, interval);
            }
        };
        let remove = function (){
            // el.classList.remove(classLoader);
            let e = el.querySelector('span');
            if ( e ) e.remove();
            el.dataset.loader = 0;
            el.style.maxHeight = '';
            el.style.height = '';
            el.innerHTML = el.dataset.temp;
            el.classList.add('a');
        };
        return {
            show: show,
            remove: remove
        };
    };

    /**
     * объект, содержит информацию о пользователе
     */
    let User = function(){};

    /**
     * сохраняет пользовательские данные в LS
     */
    User.prototype.save = function( data, prefix ){
        for ( let i in data ){
            if ( data.hasOwnProperty(i) ){
                localStorage.setItem( prefix + i, data[i]);
            }
        }
    };
    User.prototype.saveItem = function( prefix, item, val ){
        localStorage.setItem( prefix + item, val);
    };
    /**
     * удаляет JWT из запроса
     */
    User.prototype.delJwt = function( arg ){
        delete arg.req.obj['jwt'];
    };
    /**
     * добавляем JWT в запрос
     *
     * @return {boolean}
     */
    User.prototype.addUserData = function( arg ){
        let prefix = SEL.userData;
        let id = localStorage.getItem( prefix+'uid');
        let login = localStorage.getItem( prefix+'name');
        let pass = localStorage.getItem( prefix+'pass');
        if ( id && login && pass ){
            if ( !arg.req.obj.data ) arg.req.obj.data = {};
            arg.req.obj.data.jwt = ({
                'id': id,
                'login': login,
                'pass': pass
            });
            prefix = SEL.userCfg;
            arg.req.obj.data.userCfg = Common.prototype.getLocStorData( prefix );
            return true;
        }
    };

    /**
     * объект для работы с формами
     *
     * @param {string || HTMLElement} el
     * @set {this}
     * */
    let Form = function ( el ) {
        if ( el instanceof HTMLElement ){
            this.form = el;
        } else {
            this.form = A.gid( el );
        }
        this.valid = 0;
        this.submitButt = this.form.querySelector('button[type="submit"]');
        this.inputs = this.form.querySelectorAll('.' + A.SEL.textToSend);
        this.inputsAll = this.form.querySelectorAll('input, textarea, button, select');
        this.data = {};
        this.name = this.form.dataset.name;
    };
    /**
     * формирование массива данных с формы
     *
     * @this Form
     * @set {object} this.data
     * */
    Form.prototype.getData = function (){
        let data = {};
        let arr = this.inputs, item;
        for ( let i = 0; i < arr.length; ++i ) {
            item = arr[ i ];
            //console.log ( item.dataset.name + ' - ' +  + ' == ' + item.innerHTML );
            if ((item.name || item.dataset.name) && item.innerHTML) {
                switch (item.tagName) { // для DOM элементов
                    case 'P':
                    case 'DIV':
                    case 'SPAN':
                        data[item.dataset.name] = item.innerHTML;
                        break;
                    case 'SELECT':
                        data[item.name || item.dataset.name] = item.value;
                        break;
                    case 'UL':
                        /**
                         * чтобы использовать список, нужно ему присвоить класс для отправки данных
                         * датасет name, а элементам списка датасет id
                         * */
                            //console.log ( item.tagName +' ---- '+ item.innerHTML );
                            //console.log ( item.name + ' --- ' + item.value );
                            //console.log ( item.innerHTML );
                        let lis = item.childNodes, ar = [];
                        for (let i = 0; i < lis.length; ++i) {
                            let id = lis[i].dataset.id || lis[i].dataset.name;
                            if (lis.hasOwnProperty(i) && id) ar = ar.concat(id)
                        }
                        if (lis.length) data[item.dataset.name] = ar;
                }
            }
            if (item.type === 'checkbox' && item.checked) data[item.name] = 1;
            if (item.type === 'checkbox' && item.checked === false) data[item.name] = 0;
            if (item.type === 'radio' && item.checked) data[item.name] = item.value;
            switch (item.type) {
                case 'text':
                case 'password':
                case 'email':
                case 'textarea':
                    if (item.dataset.type === 'number') data[item.name] = item.value.replace(/\s+/g, '');
                    else if (item.value) data[item.name] = item.value;
                    break;
                case 'file':
                    break;
            }
        }
        this.data = data;
        return this.data;
    };
    /**
     * remove parent item
     *
     * @param {HTMLElement} el
     * */
    Form.prototype.removeItem = function ( el ) {
        el.parentNode.remove();
    };

// --------------------------------------------------------------------------------------------

    /**
     * Виртуальный ДОМ - основной элемент представлений
     * 1. компонент создается
     * 2. рендериться - создается его полный html
     * 3. вписывается ссылка на него в родительский компонент, если таковой есть
     * 4. определяется необходимость в рендеринге в DOM
     * 5. выполняется полный рендринг
     * */
    /**
     * поля доступны во всех компонентах
     * */
    let My = function (){
        // v.3.0 2017-12 у комп-в появились состояния
        // ниже комментарии просто как примеры возможностей
        // this.classes = {};
        // this.attr = {};
        // this.prop = {}; // список значений для плейсхолдеров в темплэйте
        // this.datasets = null; // значения для изменения состояния компонента
        // this.datasets = {}; // значения для изменения состояния компонента
        this.states = {
            datasets: {},
            classes: {},
            template: 0
        };
        /**
         * это работает, но способ нерекомендован, слишком накладен
         * https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Object/setPrototypeOf
         * Object.setPrototypeOf( this, My.prototype )
         * 1000 экз. Tst = 35 мск
         * 5000 экз. Tst = 215 мск
         * -----------------------------------------------------------
         * метод работает не во всех браузерах и, субъективно медленен
         * this.__proto__ = My.prototype;
         * сохраним конструктор исходного объекта (пока на всякий случай)
         * this.__proto__.constructor = this;
         * 1000 экз. Tst = 35 мск
         * 5000 экз. Tst = 205 мск
         * -----------------------------------------------------------
         * используется здесь и сейчас 2017-12
         * Views.Tst.prototype = Object.create( A.My.prototype );
         * 1000 экз. Tst = 30 мск
         * 5000 экз. Tst = 190 мск
         * */
    };
    /**
     * содержит список компонентов прошедщих предварительный рендеринг
     *
     * @return {object} My.VDom
     */
    My.VDom = {};
    /**
     * содержит список компонентов для текущего URL
     *
     * @return {object} My.Content
     */
    My.Content = {};
    /**
     * метод доступен для любого компонента
     * */
    My.prototype.render = function (){
        this.setHtml(); // получим готовый html компонента
        this.setState(); // сравним с VDom и установим необходимость обновления кода и/или состояния
        this.setParentComp(); // впишем потомков в родителя
    };
    /**
     * определим html и состояние компонента
     *
     * */
    My.prototype.setState = function (){
        let elemInVD = My.VDom[ this.prop.id ], ds;
        if ( !elemInVD || ( elemInVD.template !== this.template )){
            this.states.template = 1;
            My.VDom[ this.prop.id ] = this;
        } else {
            this.states.template = 0;
        }
        // debugger
        ['datasets', 'classes', 'attr'].some((el)=>{
            for ( let i in this[el] ){
                if ( this[el].hasOwnProperty( i )){
                    ds = this[el][ i ];
                    if ( !elemInVD || this.states.template === 1 || !elemInVD[el][i] ||
                        (elemInVD[el][i] !== this[el][i])){
                        this.states[el][i] = 1
                    }
                }
            }
        });
    };
    /**
     * подстановка значений вместо плейсхолдеров в шаблоне компонента
     *
     * @this {My.VDom}
     * @set this.prop.template
     */
    My.prototype.setHtml = function(){
        let template = this.template, token, value;
        function setState( $token, $value ) {
            if ( $value === $token ) {
                value = $token;
            }
            else if ( $value === '' ){
                value = '';
            }
            else {
                // debugger
                value = 'unknown property'
            }
        }
        let replace = function($token, $value){
            return template.replace( new RegExp("{{"+$token+"}}", 'g'), $value )
        };
        if ( this.prop ){
            for (let name in this.prop ){
                if ( this.prop.hasOwnProperty( name ) ){
                    if ( name === 'nested' ){
                        let obj = this.prop.nested;
                        for ( let nm in obj ){
                            token = 'nested.' + nm;
                            if (obj.hasOwnProperty(nm)){
                                value = obj[ nm ];
                            }
                            template = replace( token, value );
                        }
                    } else {
                        token = name;
                        // todo от нижеслед строк можно и нужно избавиться
                        if ( token === 'class' ) {
                            value = '"' + this.prop[name] + '"';
                        }
                        else if( token === 'disabled' ){
                            setState( token, this.prop[name] );
                        }
                        else {
                            value = this.prop[ name ];
                        }
                    }
                    template = replace( token, value );
                }
            }
        }
        this.template = template;
    };
    /**
     * прописывает ссылки на вложенные компоненты в родительском компоненте
     *
     * @this {My.VDom}
     * @return this.VDom.nested
     * */
    My.prototype.setParentComp = function(){
        let parent = this.prop.parent || this.place;
        if ( My.VDom[ parent ] && !My.VDom[ parent ].prop.nested ) My.VDom[ parent ].prop.nested = {};
        if ( My.VDom[ parent ] && My.VDom[ parent ].prop.nested ) My.VDom[ parent ].prop.nested[ this.prop.id ] = this.prop.id;
    };
    /**
     * удаляет компоненты не требующиеся для данного URL из DOM и VDom
     *
     * @this {My.VDom}
     * @delete this[ id ]
     * @remove div
     * */
    My.prototype.removeComp = function(){
        let div;
        for (let id in My.VDom){
            if ( My.VDom.hasOwnProperty(id) && !My.Content[id]){
                delete My.VDom[ id ];
                div = gid( id );
                if ( div ) div.remove();
            }
        }
    };
    /**
     * очищает список не представленных комп-в
     *
     * @this {My}
     * @set this.Content
     * */
    My.prototype.clearCompList = function(){
        My.Content = {};
    };
    /**
     * если родитель получил statusRender = 1, то устанавливает тот же статус для вложенных компонентов
     *
     * @this {My}
     * @set this.statusRender = 1
     */
    My.prototype.setNestedRenderStatus = function(){
        for ( let id in My.VDom ){
            if ( My.VDom.hasOwnProperty( id ) && My.VDom[ id ] && My.VDom[ id ].states.template === 1 ){
                let comp = My.VDom[ id ];
                let nested = comp.prop.nested;
                if ( nested ){
                    for ( let name in nested ){
                        if ( nested.hasOwnProperty( name )){
                            My.VDom[nested[ name ]].states.template = 1;
                        }
                    }
                }
            }
        }
    };
    /**
     * render in DOM
     *
     * @this {My}
     * @set this.outerHTML
     */
    My.renderDOM = function(){
        this.prototype.setNestedRenderStatus();

        let Render = function ( comp ) {
            if ( !comp.place && !comp.prop.parent ){
                throw new Error('empty place and parent property in ' + comp.prop.id + ', check this component');
            }
            if ( comp.place && comp.prop.parent ){
                throw new Error('set place and parent property in ' + id + ', check this component');
            }
            let SR = function () {
                comp.states.template = 0;
            };
            let HTML = function ( comp, template, wrap=1 ){
                let html;
                if (template) {
                    if ( wrap && comp.datasets ){
                        // если есть датасет, обернем компонент
                        html = "<div id="+parentID+">"+template+'</div>'
                    } else {
                        html = template
                    }
                } else {
                    html = ''
                }
                return html
            };
            let div, place, template, parent, parentID = comp.prop.id+'Case';
            template = comp.template;
            if ( comp.states.template === 1 && comp.place ){ // вставка корневого эл-та
                // if (comp.prop.id===ID.lobby_form) debugger
                place = gid( comp.place );
                div = gid( comp.prop.id );
                if ( !place ){
                    Render( My.VDom[ comp.place ]);
                }
                if ( !place ) place = gid( comp.place );
                if ( !div ) div = gid( comp.prop.id );
                if ( div ){
                    div.outerHTML = HTML( comp, template );
                    SR();
                    return;
                }
                if ( place ){
                    place.insertAdjacentHTML( Where[comp.prop.where] || 'beforeEnd', HTML( comp, template ));
                    SR();
                }
            } else if ( comp.states.template === 1 && comp.prop.parent ){ // вставка подэлемента
                div = gid( comp.prop.id );
                if ( !div ){
                    Render( My.VDom[ comp.prop.parent ]);
                }
                div = gid( comp.prop.id );
                parent = gid( parentID );
                if (parent){
                    // если есть контейнер с датасет, обновим контейнер
                    parent.innerHTML = HTML( comp, template, 0 );
                } else {
                    // иначе, обновим компонент
                    div.outerHTML = HTML( comp, template );
                }
                SR();
            }
        };
        let SetState = function ( comp ) {
            // если есть контейнер с датасет, установим его атрибуты
            let parent = gid( comp.prop.id+'Case' ), ds;
            if (comp.datasets && parent){
                for (let i in comp.datasets){
                    if (comp.datasets.hasOwnProperty(i)){
                        ds = comp.datasets[i];
                        // debugger
                        if (comp.states.datasets[i] === 1){
                            parent.dataset[i] = ds;
                        }
                        // предупредим повторное обновление состояния контейнера
                        comp.states.datasets[i] = 0
                    }
                }
            }
            if (comp.classes && parent){
                for (let i in comp.classes){
                    if (comp.classes.hasOwnProperty(i)){
                        ds = comp.classes[i];
                        // debugger
                        if (comp.states.classes[i] === 1){
                            // debugger
                            parent.classList.add(ds)
                        }
                        // предупредим повторное обновление состояния контейнера
                        comp.states.classes[i] = 0
                    }
                }
            }
        };
        let RunIt = function (comp) {
            let runit = comp.prop.runit;
            if ( runit && typeof (runit)==='function' ){
                runit.call( comp );
            }
        };
        for ( let id in My.VDom ){
            if ( My.VDom.hasOwnProperty( id )){
                let comp = My.VDom[id];
                // if (id===ID.lobby_form) debugger
                // воспользуемся Промисом чтобы гарантировать очередность событий
                new Promise((resolve, reject) => {
                    Render(comp);
                    resolve()
                }).then(() => {
                    SetState(comp);
                    RunIt(comp);
                }).catch((e) => {
                    e.eventFunc = arg.event.f;
                    err.run( e );
                })

            }
        }
    };

// --------------------------------------------------------------------------------------------

    /**
     * fetch запрос
     * https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API
     * @param {object} arg
     * @param resolve
     * @return {object} arg
     * */
    function Fetch( arg, resolve ) {
        let myRequest = new Request( arg.reqURL, arg.fetchInit );
        fetch( myRequest )
            .then(function(response) {
                // debugger
                return response.json();
            })
            .then((data)=>{
                console.log ( data );
                arg.resp = data;
                // можно определить действие внутри модуля или события
                if (arg.sm && typeof(arg.sm) === "function"){
                    arg.sm( arg );
                    arg.sm = false;
                }
                resolve( arg )
            })
            .catch( (err)=>console.warn( err ));
    }

    /**
     * используется только в разработке как временная замена fetch
     */
    let xhr = (function (){
        let run = function ( arg ){
            let xhr = new XMLHttpRequest();
            xhr.onerror = arg.onerror;
            // здесь не используется
            // let progress = function(e){
            //     if ( e['lengthComputable'] ){
            //         arg.progress = (e.loaded * 100) / e.total;
            //         if (arg.onprogress instanceof Function) {
            //             arg.onprogress.call(this, Math.round( arg.progress ));
            //         }
            //     }
            // };
            let type = arg.req.type || 'get';
            if ( type === 'get' ){
                xhr.open( 'GET', '/init.php?req=' + arg.dataXHR );
                xhr.onloadend = arg.loadend;
                xhr.send();
            }
            else if ( type === 'post' ){
                xhr.open('POST', '/init.php');
                xhr['onreadystatechange'] = arg['loadend'];
                xhr.onload = arg.onload;
                // xhr.upload.onprogress = progress;
                xhr.send( arg.dataXHR );
            }
        };
        return {
            run: run
        }
    })();

    /**
     * модуль, обработчик исключений
     *
     * @param {object} arg
     * @return {object} arg
     * */
    let err = (function (){
        let run = function ( arg )
        {
            try {
                console.log( arg );
                console.log( arg.eventFunc.toString() );
                // debugger
                // очистим все таймеры, иначе некоторые ошибки не остановить
                for (let i in A.Timers){
                    if (A.Timers.hasOwnProperty(i)){
                        clearTimeout(A.Timers[i]);
                    }
                }
                if (arg.event) console.warn(arg.event);
                if (arg.err) {
                    console.info(arg.err.data);
                    console.warn(arg.err['stack']);
                }
                if ( arg.resp && arg.resp.indexOf('xdebug-error') !== -1 ){
                    let err = document.createElement('div');
                    err.innerHTML = arg.resp;
                    let cont = document.querySelector('.content');
                    cont.insertBefore(err, cont.children[1]);
                }
            } catch( e ) {
                console.warn( e );
                // console.warn(arg.err['stack']);
            }
        };
        return {
            run: run
        };
    })();
    /**
     * посредник м/у событиями
     *
     * @param {object} arg
     * @return {object} arg
     * */

    function Mediator( arg ){
        arg.event = arg.req.act[ arg.step ];
        let promise = new Promise((resolve, reject)=>{
                // debugger
                if (typeof ( arg.event.f ) === 'function') {
                    arg.event.f()(arg, resolve);
                }
            }
        );
        promise.then(
            () => {
                if (typeof ( arg.event.f ) === 'function') {
                    let next = arg.req.act[ ++arg.step ];
                    if ( next ){
                        let action = next.f;
                        if ( action ){
                            arg.event = action;
                            return Mediator( arg );
                        }
                    }
                }
            }
        )
            .catch((e)=>{
                e.eventFunc = arg.event.f;
                arg.step++;
                err.run( e )
            })
        // .catch((err)=>console.warn( err ))
        // .catch(console.log.bind(console))
    }

    /**
     * модуль переключения класса при focus/blur/change/click
     *
     * @param {object} e
     * @return {state}
     * */
    let focus = (function (){
        let run = function ( e ){
            if (e.target === undefined || e === null || e.target === window) return;
            let eventType = e.type;
            let target = e.target;
            let tagType = target.type;
            let tagName = target.tagName;
            switch (eventType) {
                case 'focus':
                    if (tagName === 'A') {
                        target.classList.add('focus');
                    }
                    if (tagName === 'INPUT' || tagName === 'BUTTON') {
                        if (target.closest('label') !== null) {
                            target.closest('label').classList.add('focus');
                        } else {
                            target.classList.add('focus');
                        }
                    }
                    break;
                case 'blur':
                    if (tagName === 'A') {
                        target.classList.remove('focus');
                    }
                    if (tagName === 'INPUT' || tagName === 'BUTTON') {
                        if (target.closest('label') !== null) {
                            target.closest('label').classList.remove('focus');
                        } else {
                            target.classList.remove('focus');
                        }
                    }
                    break;
                case 'change':
                    if (tagType === 'checkbox') {
                        if (target.closest('label') !== null) {
                            if (target.checked) {
                                target.closest('label').classList.add('checked');
                            } else {
                                target.closest('label').classList.remove('checked');
                            }
                        }
                    }
                    if (tagType === 'radio') {
                        let radio_arr = document.getElementsByName(target.name);
                        for (let i = 0, len = radio_arr.length; i < len; i++) {
                            let item = radio_arr[i];
                            if (item.checked) {
                                if (item.closest('label') !== null) {
                                    item.closest('label').classList.add('selected');
                                }
                            } else {
                                if (item.closest('label') !== null) {
                                    item.closest('label').classList.remove('selected');
                                }
                            }
                        }
                    }
                    break;
                case 'click':
                    if (tagName === 'A') {
                        let el_arr = document.querySelectorAll('nav, menu');
                        for (let i = 0, len = el_arr.length; i < len; i++) {
                            let a_arr = el_arr[i].getElementsByTagName(tagName);
                            for (i = 0, len = a_arr.length; i < len; i++) {
                                let item = a_arr[i];
                                if (item === target) {
                                    item.classList.add('selected');
                                    item.classList.add(SEL.active);
                                } else {
                                    item.classList.remove('selected');
                                    item.classList.remove(SEL.active);
                                }
                            }
                        }
                    }
                    break;
            }
        };
        return {
            run: run
        }
    })();

    /**
     * фасад А, все действия пользователя проходят через данный модуль
     *
     * @param {object} event
     * @return {object} arg
     * */
    let facade = (function (){
        let run = function ( event ){
            try {
                let e = event, ac;
                if ( e === null ) return;
                if ( e === undefined ) return;
                if ( !arg.req ) arg.req = {};
                if ( e.type === 'submit' ) {
                    return e.preventDefault();
                }
                if ( e.target && e.target.closest !== undefined ){
                    ac = e.target.closest([ '.a' ]);
                    if (ac && ac.hasAttribute('disabled')) return;
                    if ( ac === null || e.target.disabled || e.target.parentNode.disabled ) return;
                    if ( ac.dataset.a === null || !ac.dataset.a ) return;
                }
                if ( typeof e === 'string' ){
                    arg.req.obj = JSON.parse( decodeURIComponent( e.substring( 1 )));
                }
                else if ( e.type === 'click' ){
                    if ( e.target.tagName === 'A' ) e.preventDefault();
                    if (e.target.tagName === 'INPUT' || e.target.tagName === 'TEXTAREA' ) return;
                    if (e.target.tagName === 'IMG'){
                        e.preventDefault();
                    }
                    if ( ac.tagName === 'LABEL' || ac.tagName === 'INPUT' ) return;
                } else {
                    return;
                }
                if ( ac && ac.dataset['a'] ){
                    arg.req.obj = JSON.parse( ac.dataset.a );
                    arg.target = ac;
                }
                // debugger
                let act = A.actions[ arg.req.obj.select + '_' + arg.req.obj.mode ];
                if ( act && act.length ){
                    arg.req.act = act;
                    arg.req.hash = JSON.stringify( arg.req.obj );
                    if ( !arg.event ) arg.event = {};
                    arg.step = 0;
                    arg.resp = { code:0 };
                    arg.cs.cln = {
                        err: 0,
                        runReq: 0,
                        formValid: 0,
                        srv: 0
                    };
                    Mediator( arg );
                } else {
                    console.log ( 'действий не назначено' );
                }
            } catch (e) {
                if ( e.code === 10 ){ // пользователь не авторизован
                    console.warn( e.message );
                    return;
                }
                arg.cs.err = 1;
                arg.err = e;
                err.run( arg );
            }
        };
        return {
            run: run
        }
    })();

    /**
     * событие, импорт JS скрипта
     *
     * @param {object} arg // arg.event.d - местоположение скрипта
     * @param resolve
     * @return {function} JS()
     * */
    function getScript ( arg, resolve ){
        console.log ( 'getScript' );
        let s = document.querySelector('script[src="' + arg['event'].d +'"]');
        if ( !s ){
            let script = document.createElement('script');
            script.src = arg['event'].d;
            script.type = arg['event'].t || A.typeScript;
            document.body.appendChild( script );
            script.onload = resolve;
        } else {
            resolve( arg )
        }
    }

    A.Color = Color;
    A.My = My;
    A.SEL = SEL;
    A.User = User;
    A.exp = exp;
    A.gid = gid;
    A.arg = arg;
    A.facade = facade;
    A.Common = Common;
    A.Where = Where;
    A.gid = gid;
    A.Form = Form;
    A.Fetch = Fetch;
    A.getScript = getScript;
    A.Views = {}; // Представления
    A.ID = {}; // ИД представлений
    A.Ets = {}; // События
    A.Timers = {}; // Таймеры

    function RunApp() {
        facade.run("#{\"select\":\"App\",\"mode\":\"run\"}");
    }
    /**
     * план-список вызовов, дорожная карта событий
     *
     * @param {string} a
     * */
    actions = {
        App_set: [ // на старте приложения
            {f: () => getScript, d: '/scripts/views.js'},
            {f: () => getScript, d: '/scripts/events.js'},
            {f: () => getScript, d: '/scripts/actions.js'},
            {f: () => RunApp }
            // {f: () => A.Ets.pause }
        ]
    };
    A.actions = actions;

    window.addEventListener('submit', facade.run);
    window.addEventListener('input', facade.run);
    window.addEventListener('click', facade.run, false);
    window.addEventListener('focus', focus.run, true);
    window.addEventListener('blur', focus.run, true);

    facade.run("#{\"select\":\"App\",\"mode\":\"set\"}");


})();
