-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.23 - MySQL Community Server (GPL)
-- Операционная система:         Win32
-- HeidiSQL Версия:              9.4.0.5174
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных game
CREATE DATABASE IF NOT EXISTS `game` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `game`;

-- Дамп структуры для событие game.purgeUserList
DELIMITER //
CREATE DEFINER=`root`@`localhost` EVENT `purgeUserList` ON SCHEDULE EVERY 5 SECOND STARTS '2017-10-06 05:34:39' ON COMPLETION PRESERVE ENABLE DO BEGIN
CALL `purgeUserList`(5);
END//
DELIMITER ;

-- Дамп структуры для процедура game.purgeUserList
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `purgeUserList`(
	IN `timeout` INT



)
BEGIN
#declare timeout int;
SET time_zone = '+00:00';
delete from tbl_users_online where TIMESTAMPDIFF(SECOND, uol_time, now())>timeout; 
END//
DELIMITER ;

-- Дамп структуры для таблица game.tbl_cfg
CREATE TABLE IF NOT EXISTS `tbl_cfg` (
  `cfg_id` int(11) unsigned NOT NULL,
  `cfg_wlog` tinyint(3) unsigned DEFAULT '0' COMMENT 'состояние панели истории',
  `cfg_users` tinyint(4) unsigned DEFAULT '0' COMMENT 'состояние списка пользователей',
  UNIQUE KEY `Key` (`cfg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='хранит  настройки игроков';

-- Дамп данных таблицы game.tbl_cfg: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `tbl_cfg` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_cfg` ENABLE KEYS */;

-- Дамп структуры для таблица game.tbl_fields_limit
CREATE TABLE IF NOT EXISTS `tbl_fields_limit` (
  `tfl_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tfl_form` enum('login','user','lobby') DEFAULT NULL COMMENT 'список форм, поле для отбора',
  `tfl_name` tinytext NOT NULL,
  `tfl_regexp` tinytext,
  `tfl_caption` tinytext,
  `tfl_min_len` smallint(5) unsigned DEFAULT NULL COMMENT 'минимальная длина поля',
  `tfl_max_len` smallint(5) unsigned DEFAULT NULL COMMENT 'максимум длина поля',
  `tfl_explain` char(50) DEFAULT NULL COMMENT 'поянение для разработчика',
  KEY `key` (`tfl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='Таблица органичений на поля';

-- Дамп данных таблицы game.tbl_fields_limit: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `tbl_fields_limit` DISABLE KEYS */;
INSERT INTO `tbl_fields_limit` (`tfl_id`, `tfl_form`, `tfl_name`, `tfl_regexp`, `tfl_caption`, `tfl_min_len`, `tfl_max_len`, `tfl_explain`) VALUES
	(4, 'user', 'username', '^[а-яА-Яa-zA-Z0-9\\s-]{4,20}$', 'any letters of the Russian and English alphabets, numbers and hyphen, from 4 to 20 characters', 4, 20, NULL),
	(5, 'user', 'password', '^[а-яА-Яa-zA-Z0-9\\s+_!#$%^&*:()@-]{4,25}$', 'any letters of the Russian, English alphabet, numbers, as well as symbols: @ - + _! # $% ^ & * :(), from 4 to 25 characters', 4, 25, NULL),
	(6, 'lobby', 'name', '^[а-яА-Яa-zA-Z0-9\\s-]{3,15}$', 'letters of the Russian and English alphabets, numbers and hyphen, from 3 to 15 characters', 3, 15, NULL),
	(7, 'lobby', 'timer', '^[0-9]{1,2}$', 'only numbers from 5 to 99', 1, 2, NULL);
/*!40000 ALTER TABLE `tbl_fields_limit` ENABLE KEYS */;

-- Дамп структуры для таблица game.tbl_games
CREATE TABLE IF NOT EXISTS `tbl_games` (
  `gm_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `gms_pl_id` int(11) unsigned DEFAULT '0',
  `gms_lb_id` int(10) unsigned DEFAULT '0',
  `gms_game_id` int(10) unsigned DEFAULT '0' COMMENT 'пишем сюда id из hist',
  `gms_state` tinyint(4) unsigned DEFAULT '0' COMMENT '0 - готова к старту; 1 - игра идет; 2 - завершена',
  `gms_first` int(10) unsigned DEFAULT '0' COMMENT 'id первого походившего игрока',
  `gms_hint` tinyint(3) unsigned DEFAULT '0' COMMENT 'неверная подсказка; 0-не исп-сь; 254-использовалась; 255-сгорела',
  PRIMARY KEY (`gm_id`),
  UNIQUE KEY `key` (`gms_pl_id`,`gms_lb_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3432 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Дамп данных таблицы game.tbl_games: ~5 rows (приблизительно)
/*!40000 ALTER TABLE `tbl_games` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_games` ENABLE KEYS */;

-- Дамп структуры для таблица game.tbl_hint
CREATE TABLE IF NOT EXISTS `tbl_hint` (
  `hnt_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hnt_pl` int(11) unsigned DEFAULT '0',
  `hnt_wid` tinyint(4) unsigned DEFAULT '0' COMMENT 'weapon id',
  PRIMARY KEY (`hnt_id`),
  UNIQUE KEY `key` (`hnt_pl`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='текущий, последний выбор оружия игрока';

-- Дамп данных таблицы game.tbl_hint: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `tbl_hint` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_hint` ENABLE KEYS */;

-- Дамп структуры для таблица game.tbl_hist_game
CREATE TABLE IF NOT EXISTS `tbl_hist_game` (
  `hgm_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hgm_pl_1` int(11) unsigned DEFAULT '0' COMMENT 'id игрока 1',
  `hgm_pl_2` int(10) unsigned DEFAULT '0' COMMENT 'id игрока 2',
  `hgm_winner` int(10) unsigned DEFAULT '0' COMMENT 'id победителя',
  `hgm_loser` int(10) unsigned DEFAULT '0' COMMENT 'id проигравшего',
  `hgm_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'время хода',
  PRIMARY KEY (`hgm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=262 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='хранит список всех игр, исп-ся для идентефикации  и учета результатов игр';

-- Дамп данных таблицы game.tbl_hist_game: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `tbl_hist_game` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_hist_game` ENABLE KEYS */;

-- Дамп структуры для таблица game.tbl_lobby
CREATE TABLE IF NOT EXISTS `tbl_lobby` (
  `lb_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lb_name` tinytext NOT NULL,
  `lb_timer` smallint(6) NOT NULL DEFAULT '5',
  `lb_owner` int(11) unsigned NOT NULL COMMENT 'u_id',
  `lb_time_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`lb_id`),
  UNIQUE KEY `Key` (`lb_name`(100))
) ENGINE=InnoDB AUTO_INCREMENT=473 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы game.tbl_lobby: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `tbl_lobby` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_lobby` ENABLE KEYS */;

-- Дамп структуры для таблица game.tbl_lobby_use
CREATE TABLE IF NOT EXISTS `tbl_lobby_use` (
  `lu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lu_id_lb` int(10) unsigned DEFAULT NULL,
  `lu_id_pl` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`lu_id`),
  UNIQUE KEY `Key` (`lu_id_pl`)
) ENGINE=InnoDB AUTO_INCREMENT=516 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы game.tbl_lobby_use: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `tbl_lobby_use` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_lobby_use` ENABLE KEYS */;

-- Дамп структуры для таблица game.tbl_move
CREATE TABLE IF NOT EXISTS `tbl_move` (
  `mv_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mv_pl` int(11) unsigned DEFAULT '0',
  `mv_wid` tinyint(4) unsigned DEFAULT '0' COMMENT 'weapon id',
  `mv_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'время записи последнего хода, записывать может getState',
  `mv_chm` timestamp NULL DEFAULT NULL COMMENT 'время для сравнения текущего и времени последней активности игрока',
  PRIMARY KEY (`mv_id`),
  UNIQUE KEY `key` (`mv_pl`)
) ENGINE=InnoDB AUTO_INCREMENT=2014 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='текущий, последний выбор оружия игрока';

-- Дамп данных таблицы game.tbl_move: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `tbl_move` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_move` ENABLE KEYS */;

-- Дамп структуры для таблица game.tbl_penalty
CREATE TABLE IF NOT EXISTS `tbl_penalty` (
  `pn_pid` int(11) unsigned NOT NULL COMMENT 'id player',
  `pn_time` datetime DEFAULT NULL COMMENT 'окончание времени штрафа',
  UNIQUE KEY `Key` (`pn_pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Дамп данных таблицы game.tbl_penalty: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `tbl_penalty` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_penalty` ENABLE KEYS */;

-- Дамп структуры для таблица game.tbl_play
CREATE TABLE IF NOT EXISTS `tbl_play` (
  `gm_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `gm_pl_id` int(11) unsigned DEFAULT '0' COMMENT 'id игрока',
  `gm_mv_cnt` tinyint(4) unsigned DEFAULT '0' COMMENT 'номер хода',
  `gm_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'время хода',
  PRIMARY KEY (`gm_id`),
  UNIQUE KEY `key` (`gm_pl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2018 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Дамп данных таблицы game.tbl_play: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `tbl_play` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_play` ENABLE KEYS */;

-- Дамп структуры для таблица game.tbl_rounds
CREATE TABLE IF NOT EXISTS `tbl_rounds` (
  `rnd_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rnd_game` int(11) unsigned DEFAULT '0' COMMENT 'id игры',
  `rnd_winner` int(10) unsigned DEFAULT '0' COMMENT 'id победителя; 0 ничья',
  `rnd_loser` int(10) unsigned DEFAULT '0' COMMENT 'id проигравшего',
  `hgm_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'время хода',
  PRIMARY KEY (`rnd_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3112 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='список раундов с их результатами';

-- Дамп данных таблицы game.tbl_rounds: ~106 rows (приблизительно)
/*!40000 ALTER TABLE `tbl_rounds` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_rounds` ENABLE KEYS */;

-- Дамп структуры для таблица game.tbl_users
CREATE TABLE IF NOT EXISTS `tbl_users` (
  `u_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `u_login` tinytext NOT NULL,
  `u_pass` varchar(128) DEFAULT NULL,
  `u_salt` varchar(50) DEFAULT NULL,
  `u_win` smallint(5) unsigned DEFAULT NULL COMMENT 'победы',
  `u_lose` smallint(5) unsigned DEFAULT NULL COMMENT 'проигрыши',
  `u_hint` smallint(5) unsigned DEFAULT NULL COMMENT 'подсказки',
  `u_date_created` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`u_id`),
  UNIQUE KEY `KeyUniq` (`u_id`),
  UNIQUE KEY `Uniq2` (`u_login`(100))
) ENGINE=InnoDB AUTO_INCREMENT=322 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы game.tbl_users: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;
INSERT INTO `tbl_users` (`u_id`, `u_login`, `u_pass`, `u_salt`, `u_win`, `u_lose`, `u_hint`, `u_date_created`) VALUES
	(321, 'Test', '06f7b0abbf5ca89a701cd4918f43855f', '59d739cd20432', NULL, NULL, NULL, '2017-10-06 08:07:41');
/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;

-- Дамп структуры для таблица game.tbl_users_online
CREATE TABLE IF NOT EXISTS `tbl_users_online` (
  `uol_uid` int(11) DEFAULT NULL,
  `uol_time` datetime DEFAULT NULL,
  UNIQUE KEY `key` (`uol_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='список пользователей on-line';

-- Дамп данных таблицы game.tbl_users_online: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `tbl_users_online` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_users_online` ENABLE KEYS */;

-- Дамп структуры для таблица game.tbl_warlog
CREATE TABLE IF NOT EXISTS `tbl_warlog` (
  `wl_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `wl_pid` int(11) unsigned DEFAULT '0' COMMENT 'id игрока',
  `wl_gid` int(11) unsigned DEFAULT '0' COMMENT 'id game',
  `wl_event` text COMMENT 'id игрока 2',
  `wl_time` datetime DEFAULT NULL COMMENT 'время хода',
  PRIMARY KEY (`wl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2196 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='хранит список всех игр, исп-ся для идентефикации  и учета результатов игр';

-- Дамп данных таблицы game.tbl_warlog: ~13 rows (приблизительно)
/*!40000 ALTER TABLE `tbl_warlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_warlog` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
