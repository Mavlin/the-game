"use strict";
/**
 * план-список вызовов, дорожная карта событий
 *
 * */
(function () {
        let act = {
            test_ds:[
                {f:()=>A.Ets.testDS},
            ],
            sse_off:[
                {f:()=>A.Ets.sseOff},
            ],
            sse_run:[ // запускается как в конце ajax запросов, так и ES
                {f:()=>A.Ets.addUserData},
                {f:()=>A.Ets.getGameState},
                {f:()=>A.Ets.sse_on},
            ],
            App_off:[
                {f:()=>A.Ets.sseOff},
                {f:()=>A.Ets.logout},
                {f:()=>A.Ets.purgeComp, d:A.ID.UserList},
                {f:()=>A.Ets.renderDOM},
            ],
            App_run: [ // на старте приложения
                {f:()=>A.Ets.sseOff},
                {f:()=>A.Ets.getLimits},
                {f:()=>A.Ets.fetch},
                {f:()=>A.Ets.getResp},
                {f:()=>A.Ets.renderDOM},
                {f:()=>A.Ets.addUserData},
                {f:()=>A.Ets.getUserConf},
                {f:()=>A.Ets.fetch},
                {f:()=>A.Ets.addUserData},
                {f:()=>A.Ets.getGameState},
                {f:()=>A.Ets.sse_on}
            ],
            lobby_create: [
                {f:()=>A.Ets.sseOff},
                {f:()=>A.Ets.addUserData},
                {f:()=>A.Ets.selectForm},
                {f:()=>A.Ets.sendDataForm},
                {f:()=>A.Ets.fetch},
                {f:()=>A.Ets.getResp},
                {f:()=>A.Ets.close_popup},
                {f:()=>A.Ets.addUserData},
                {f:()=>A.Ets.getGameState},
                {f:()=>A.Ets.sse_on},
            ],
            lobby_leave: [
                {f:()=>A.Ets.sseOff},
                {f:()=>A.Ets.addUserData},
                {f:()=>A.Ets.setReqFromURL},
                {f:()=>A.Ets.fetch},
                {f:()=>A.Ets.getResp},
            ],
            lobby_select: [
                {f:()=>A.Ets.sseOff},
                {f:()=>A.Ets.addUserData},
                {f:()=>A.Ets.setReqFromURL},
                {f:()=>A.Ets.fetch},
                {f:()=>A.Ets.getResp},
                {f:()=>A.Ets.close_popup},
                {f:()=>A.Ets.addUserData},
                {f:()=>A.Ets.getGameState},
                {f:()=>A.Ets.sse_on},
            ],
            set_AppState: [ // список публикуется на каждом ответе сервера
                {f:()=>A.Ets.saveUserData},
                {f:()=>A.Ets.renderDOM},
            ],
            game_state103: [ // app start, user unauthorized
                {f:()=>A.Ets.sseOff},
                {f:()=>A.Ets.renderDOM},
            ],
            show_WarLog: [
                {f:()=>A.Ets.saveUserCfg, d: 'wlog', v: 1},
                {f:()=>A.Ets.renderDOM},
            ],
            hide_WarLog: [
                {f:()=>A.Ets.saveUserCfg, d: 'wlog', v: 0},
                {f:()=>A.Ets.renderDOM},
            ],
            show_UserList: [
                {f:()=>A.Ets.saveUserCfg, d: 'users', v: 1},
                {f:()=>A.Ets.renderDOM},
            ],
            hide_UserList: [
                {f:()=>A.Ets.saveUserCfg, d: 'users', v: 0},
                {f:()=>A.Ets.renderDOM},
            ],
            game_move: [ //
                {f:()=>A.Ets.sseOff},
                {f:()=>A.Ets.addUserData},
                {f:()=>A.Ets.setReqFromURL},
                {f:()=>A.Ets.fetch},
                {f:()=>A.Ets.getResp},
            ],
            game_getHint: [ //
                {f:()=>A.Ets.sseOff},
                {f:()=>A.Ets.addUserData},
                {f:()=>A.Ets.setReqFromURL},
                {f:()=>A.Ets.fetch},
                {f:()=>A.Ets.getResp},
            ],
            game_run: [ //
                {f:()=>A.Ets.sseOff},
                {f:()=>A.Ets.addUserData},
                {f:()=>A.Ets.setReqFromURL},
                {f:()=>A.Ets.fetch},
                {f:()=>A.Ets.getResp},
            ],
            user_login: [
                {f:()=>A.Ets.sseOff},
                {f:()=>A.Ets.selectForm},
                {f:()=>A.Ets.sendDataForm},
                {f:()=>A.Ets.fetch},
                {f:()=>A.Ets.getResp},
                {f:()=>A.Ets.closePopUp},
                {f:()=>A.Ets.saveUserData},
                {f:()=>A.Ets.addUserData},
                {f:()=>A.Ets.getUserConf},{f:()=>A.Ets.fetch},
            ],
            user_register: [
                {f:()=>A.Ets.selectForm},
                {f:()=>A.Ets.sendDataForm},{f:()=>A.Ets.fetch},
                {f:()=>A.Ets.getResp},
            ],
            user_logout: [
                {f:()=>A.Ets.showLoader},
                {f:()=>A.Ets.sseOff},
                {f:()=>A.Ets.addUserData},
                {f:()=>A.Ets.setReqFromURL},
                {f:()=>A.Ets.fetch},
                {f:()=>A.Ets.getResp},
            ],
            common_auth: [
                {f:()=>A.getScript, d:'/scripts/jquery-3.2.1.min.js'},
                {f:()=>A.getScript, d:'/scripts/jquery.magnific-popup.min.js'},
                {f:()=>A.Ets.loadFormInPopUp, d: A.ID.auth_form},
                {f:()=>A.Ets.setAcc, d:A.ID.auth_acc},
                // {f:()=>A.Ets.loadAcc, d:A.ID.auth_acc},
            ],
            common_purgeLog: [
                {f:()=>A.Ets.purgeLog},
            ],
            lobby_get: [
                {f:()=>A.getScript, d:'/scripts/jquery-3.2.1.min.js'},
                {f:()=>A.getScript, d:'/scripts/jquery.magnific-popup.min.js'},
                {f:()=>A.Ets.loadFormInPopUp, d: A.ID.lobby_form, t: 0},
                {f:()=>A.Ets.setAcc, d:A.ID.lobby_acc},
            ],
            lobby_showQuestion: [
                {f:()=>A.Ets.remClass, d:'show'},
                {f:()=>A.Ets.addClass, d:'show'},
            ],
            rem_Question: [
                {f:()=>A.Ets.remClass, d:'show'},
            ]
        };

    Object.assign( A.actions, act );

})();
