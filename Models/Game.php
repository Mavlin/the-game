<?php
namespace Models;
/**
 * основная таблица tbl_hist_game, данные в ней не удаляются и модифицируются только
 * по итогам игры, она же определяет идентификатор игры, который используют другие таблицы
 *
 * */
class Game extends Model{
    public function __construct($request = null){
        parent::__construct($request);
    }

    /**
     * записывает количество ходов
     *
     * @param $uid int
     * @param $mcnt int
     *
     * @param $isCanMove
     * @return bool
     */
    public function setMoveCnt( $uid, $mcnt, $isCanMove ){
        $sql = "insert LOW_PRIORITY ignore into tbl_play (gm_pl_id, gm_mv_cnt, gm_canMove) values (?i, ?i, ?i) 
on duplicate key update gm_pl_id=?i, gm_mv_cnt=?i, gm_canMove=?i";
        return $this->connect->query( $sql, $uid, $mcnt, $isCanMove, $uid, $mcnt, $isCanMove );
    }

    /**
     * отвечает на вопрос был ли первый ход за автором? еще актуально?
     *
     * @param $uid int
     * @param $lid int
     *
     * @return int
     * */
    public function isFirst( $uid, $lid ){
        $sql = "select gms_first from tbl_games where gms_pl_id=?i and gms_lb_id=?i";
        return (integer)$this->connect->getOne( $sql, $uid, $lid );
    }

    /**
     * устанавливает или обнуляет первого походившего
     *
     * @param $uid int
     * @param $lid int
     * @param $frst int
     *
     * @return bool
     * */
    public function setFirst( $uid, $lid, $frst=0 ){
        $sql = "update tbl_games set gms_first=?i where gms_pl_id=?i and gms_lb_id=?i";
        return $this->connect->query( $sql, $frst, $uid, $lid );
    }
    /**
     * вернет подсказку или код состояния
     *
     * @param $uid int
     * @param $lid int
     *
     * @return integer
     * */
    public function getHint( $uid, $lid ){
        $sql = "select gms_hint from tbl_games where gms_pl_id=?i and gms_lb_id=?i";
        return (integer)$this->connect->getOne( $sql, $uid, $lid );
    }

    /**
     * вернет настройки пользователя
     *
     * @param $uid int
     *
     * @return array
     *
     * приведение типов в классе не работает
     * select cast(cfg_id as unsigned) as uid, cast(cfg_wlog as unsigned) as wlog, cast(cfg_users as unsigned) as ulist from tbl_cfg where cfg_id=297
     *
     * */
    public function getConf( $uid ){
        $sql = "select cfg_wlog as wlog, cfg_users as users from tbl_cfg where cfg_id=?i";
        return $this->connect->getRow( $sql, $uid );
    }

    /**
     * записывает настройки пользователя
     *
     * @param $uid int
     * @param $data array
     *
     * @return bool
     * */
    public function setConf( $uid, $data ){
        $sql = "insert LOW_PRIORITY into tbl_cfg set cfg_id=?i, ?u ON DUPLICATE KEY UPDATE ?u";
        return $this->connect->query( $sql, $uid, $data, $data );
    }

    /**
     * записывает неверную подсказку
     *
     * @param $uid int
     * @param $lid int
     * @param $hint int
     *
     * @return bool
     * */
    public function setHint( $uid, $lid, $hint ){
        $sql = "update tbl_games set gms_hint=?i where gms_pl_id=?i and gms_lb_id=?i";
        return $this->connect->query( $sql, $hint, $uid, $lid );
    }

    /**
     * вернет имя игрока
     *
     * @param $uid int
     *
     * @return string
     * */
    public function getName( $uid ){
        $sql = "select u_login from tbl_users where u_id=?i";
        return $this->connect->getOne( $sql, $uid );
    }
    /**
     * вернет кол-во сыгранных раундов, rounds
     *
     * @param $gid int
     *
     * @return int
     * */
    public function getRounds( $gid ){
        $sql = "select count(rnd_id) from tbl_rounds where rnd_game=?i";
        return (int)$this->connect->getOne( $sql, $gid );
    }
    /**
     * вернет победителя последнего раунда, rounds
     *
     * @param $gid int
     *
     * @return int
     * */
    public function getLastRoundWinner( $gid ){
        $sql = "select rnd_winner from tbl_rounds where rnd_game=?i order by rnd_id desc limit 1";
        return (int)$this->connect->getOne( $sql, $gid );
    }
    /**
     * вернет статус игры из games
     *
     * @param $gid int
     * @param $lid int
     *
     * @return int
     * */
    public function getGameState( $gid, $lid ){
        $sql = "select gms_state from tbl_games where gms_pl_id=?i and gms_lb_id=?i";
        return (int)$this->connect->getOne( $sql, $gid, $lid );
    }
    /**
     * вернет кол-во побед, rounds
     *
     * @param $gid int
     * @param $uid int
     *
     * @return int
     * */
    public function getWins( $gid, $uid ){
        $sql = "select count(rnd_id) from tbl_rounds where rnd_game=?i and rnd_winner=?i";
        return (integer)$this->connect->getOne( $sql, $gid, $uid );
    }
    /**
     * определяет победителя раунда - добавляет запись в таблицу rounds
     *
     * @param $gid int
     * @param $wid int
     *
     * @return bool
     * */
    public function setRoundWinner($gid, $wid ){
        $sql = "insert LOW_PRIORITY  into tbl_rounds (rnd_game, rnd_winner) values (?i, ?i)";
        return $this->connect->query( $sql, $gid, $wid );
    }
    /**
     * определяет проигравшего в раунде - добавляет запись в таблицу rounds
     *
     * @param $gid int
     * @param $losid int
     *
     * @return bool
     * */
    public function setRoundLoser($gid, $losid ){
        $sql = "insert LOW_PRIORITY ignore into tbl_rounds (rnd_game, rnd_loser) values (?i, ?i)";
        return $this->connect->query( $sql, $gid, $losid );
    }
    /**
     * добавляет запись в таблицу hist game
     *
     * @param $uid int
     * @param $eid int
     *
     * @return int id созданной игры
     * */
    public function addGameHist( $uid, $eid ){
        $sql = "insert LOW_PRIORITY  into tbl_hist_game (hgm_pl_1, hgm_pl_2) values (?i, ?i)";
        $this->connect->query( $sql, $uid, $eid );
        return $this->connect->insertId();
    }

    /**
     * обновляет запись в таблице hist game
     *
     * @param $wid int
     * @param $gid int
     *
     * @return bool
     * */
    public function setGameWinner($wid, $gid ){
        $sql = "update tbl_hist_game set hgm_winner=?i where hgm_id=?i";
        return $this->connect->query( $sql, $wid, $gid );
    }

    /**
     * обновляет запись в таблице hist game
     *
     * @param $lid int
     * @param $gid int
     *
     * @return bool
     * */
    public function setGameLoser($lid, $gid ){
        $sql = "update tbl_hist_game set hgm_loser=?i where hgm_id=?i";
        return $this->connect->query( $sql, $lid, $gid );
    }

    /**
     * возвращает id второго посетителя лобби - противника
     *
     * @param $uid int
     * @param $lid int
     *
     * @return int || bool
     * */
    public function getEnemyId( $uid, $lid ){
        $sql = "select lu_id_pl from tbl_lobby_use where lu_id_pl<>?i and lu_id_lb=?i";
        return $this->connect->getOne( $sql, $uid, $lid );
    }
    /**
     * удалит пользователя из списка занимаемых лобби, в таблице tbl_lobby_use
     *
     * @param $uid int
     * @param $lid int
     *
     * @return bool
     * */
    public function setLobbyLeave( $uid, $lid ){
        $sql = "delete from tbl_lobby_use where lu_id_pl=?i and lu_id_lb=?i";
        return $this->connect->query( $sql, $uid, $lid );
    }
    /**
     * установит статус игры в таблице tbl_games
     *
     * @param $uid int
     * @param $lid int
     * @param $state int
     *
     * @return bool
     * */
    public function setGameState( $uid, $lid, $state ){
        $sql = "insert LOW_PRIORITY  ignore into tbl_games (gms_pl_id, gms_lb_id, gms_state) values (?i, ?i, ?i) 
on duplicate key update gms_pl_id=?i, gms_lb_id=?i, gms_state=?i";
        return $this->connect->query( $sql, $uid, $lid, $state, $uid, $lid, $state );
    }
    /**
     * создает игру в таблице tbl_games
     *
     * @param $uid int
     * @param $lid int
     *
     * @return bool
     * */
    public function setGame( $uid, $lid ){
        $sql = "insert LOW_PRIORITY  ignore into tbl_games 
(gms_pl_id, gms_lb_id, gms_game_id, gms_state, gms_first, gms_hint) values (?i, ?i, 0,0,0,0) 
on duplicate key update gms_pl_id=?i, gms_lb_id=?i, gms_game_id=0, gms_state=0, gms_first=0, gms_hint=0";
        return $this->connect->query( $sql, $uid, $lid, $uid, $lid );
    }

    /**
     * записывает id игры в таблице tbl_games
     *
     * @param $uid int
     * @param $lid int
     * @param $gid int
     *
     * @return bool
     * */
    public function setGameId( $uid, $lid, $gid=0 ){
        $sql = "insert LOW_PRIORITY ignore into tbl_games (gms_pl_id, gms_lb_id, gms_game_id ) values (?i, ?i, ?i) 
on duplicate key update gms_pl_id=?i, gms_lb_id=?i, gms_game_id=?i";
        return $this->connect->query( $sql, $uid, $lid, $gid, $uid, $lid, $gid );
    }
    /**
     * вернет номер хода (кол-во ходов в игре) игры с участием пользователя из tbl_play
     *
     * @param $uid int
     *
     * @return int
     * */
    public function getMoveCnt( $uid ){
        $sql = "select gm_mv_cnt from tbl_play where gm_pl_id=?i";
        return (int)$this->connect->getOne( $sql, $uid );
    }
    /**
     * вернет возможность сделать ход
     *
     * @param $uid int
     *
     * @return bool
     * */
    public function getCanMove( $uid ){
        $sql = "select gm_canMove from tbl_play where gm_pl_id=?i";
        return (bool)$this->connect->getOne( $sql, $uid );
    }

    /**
     * управляет возможностью сделать ход
     *
     * @param $uid int
     *
     * @param $isCan
     * @return bool
     */
    public function setCanMove( $uid, $isCan ){
        $sql = "update tbl_play set gm_canMove=?i where gm_pl_id=?i";
        return $this->connect->query( $sql, $isCan, $uid );
    }

    /**
     * определяет наличие игры с участием пользователя в tbl_games
     *
     * @param int $uid id пользователя
     * @param int $gid id игры
     *
     * @return bool
     * */
    public function isGame( $uid, $gid ){
        $sql = "select gm_id from tbl_games where gms_pl_id=?i and gm_id=?i";
        return (bool)$this->connect->getOne( $sql, $uid, $gid );
    }
    /**
     * вернет id игры с участием пользователя в данном лобби tbl_games
     *
     * @param int $uid id пользователя
     * @param int $lid id лобби
     *
     * @return int
     * */
    public function getGameId( $uid, $lid ){
        $sql = "select gms_game_id from tbl_games where gms_pl_id=?i and gms_lb_id=?i";
        return (integer)$this->connect->getOne( $sql, $uid, $lid );
    }
    /**
     * вернет id и статус игры с участием пользователя в данном лобби tbl_games
     *
     * @param int $uid id пользователя
     * @param int $lid id лобби
     *
     * @return array
     * */
    public function getGameInLobby($uid, $lid ){
        $sql = "select gms_game_id as gid, gms_state as gst, gms_hint as hint 
                from tbl_games where gms_pl_id=?i and gms_lb_id=?i";
        return $this->connect->getRow( $sql, $uid, $lid );
    }
    /**
     * возвращает имя и id владельца, посетителя и таймер занятого лобби
     *
     * @param $uid int
     *
     * @return array
     * */
    public function getLobbyState( $uid ){
        $sql = "select lb_id as id, u_id as uid, lb_name as name, lb_timer as timer, tbl_users.u_login as owner, 
lb_owner as id_owner, (select lu_id_pl from tbl_lobby_use, tbl_users where u_id=lu_id_pl and lu_id_lb=id and lu_id_pl<>?i) as eid,
 (select tbl_users.u_login from tbl_lobby_use, tbl_users where u_id=lu_id_pl and lu_id_lb=id and lu_id_pl<>?i) as enemy
  from tbl_lobby, tbl_lobby_use, tbl_users where lu_id_pl=?i and lu_id_lb=lb_id and u_id=tbl_lobby.lb_owner";
        return $this->connect->getRow( $sql, $uid, $uid, $uid );
    }
    /**
     * возвращает таймер занятого лобби, c
     *
     * @param $uid int
     *
     * @return int
     * */
    public function getLobbyTimer( $uid ){
        $sql = "select lb_timer from tbl_lobby inner join tbl_lobby_use on lb_id=lu_id_lb 
inner join tbl_users  on u_id=lu_id_pl where u_id=?i";
        return (int)$this->connect->getOne( $sql, $uid );
    }

    /**
     * возвращает id занятого лобби
     *
     * @param $uid int
     *
     * @return int
     * */
    public function getLobbyId( $uid ){
        $sql = "select lu_id_lb from tbl_lobby_use where lu_id_pl=?i";
        return (int)$this->connect->getOne( $sql, $uid );
    }

    /**
     * возвращает время прошедшее со времени последнего хода игрока, с
     *
     * @param $uid int
     *
     * @return int
     * */
    public function getTmPassMove( $uid, $time ){
        // переменные в классе не работают
//        $sql = "select if(@p>=0, @p, 0) as tm from tbl_move inner join
//tbl_lobby_use on mv_pl=lu_id_pl inner join tbl_lobby on lu_id_lb=lb_id where
// mv_pl=?i and @p:=lb_timer-TIMESTAMPDIFF(SECOND, mv_time, now())";
        $sql = "select (?i - mv_utm) as tm from tbl_move inner join
tbl_lobby_use on mv_pl=lu_id_pl inner join tbl_lobby on lu_id_lb=lb_id where mv_pl=?i";
        return (int)$this->connect->getOne( $sql, $time, $uid );
    }

    /**
     * возвращает время прошедшее со времени последнего выбора оружия игрока, с
     *
     * @param $uid int
     *
     * @return int
     * */
    public function getTmPassGame( $uid, $time ){
        $sql = "select (?i - mv_chmv) as tm from tbl_move where mv_pl=?i";
        return (int)$this->connect->getOne( $sql, $time, $uid );
    }


    /**
     * записывает ход
     *
     * @param $uid int
     * @param $mid int
     *
     * @return bool
     * */

    public function setMove( $uid, $mid, $time ){
        $sql = "insert LOW_PRIORITY ignore into tbl_move (mv_pl, mv_wid, mv_utm, mv_chmv) values (?i, ?i, ?i, ?i) 
on duplicate key update mv_pl = ?i, mv_wid = ?i, mv_time=now(), mv_chm=now(), mv_utm=?i, mv_chmv=?i";
        return $this->connect->query($sql, $uid, $mid, $time, $time, $uid, $mid, $time, $time);
    }

    /**
     * обновляет время хода, исп-ся для переустановки времени в случае бездйствия игрока
     *
     * @param $uid int
     *
     * @return bool
     * */
    public function updMoveTime($uid, $time){
        $sql = "update tbl_move set mv_time=now(), mv_utm=?i where mv_pl=?i";
        return $this->connect->query( $sql, $time, $uid );
    }

    /**
     * возвращает список всех доступных лобби + информация о владельце, посетителе,
     * таймере
     *
     * @return array
    */
    public function getLobbyList(){
        $sql = "select distinct lb_id as id, lb_name as name, lb_timer as timer, (select u_login from tbl_users where u_id=lb_owner) as owner, u_login as player, gms_state as state from tbl_lobby left outer join tbl_lobby_use on lu_id_lb=lb_id left outer join tbl_users on u_id=lu_id_pl left outer join tbl_games on gms_lb_id=lb_id and gms_pl_id=u_id";
        return $this->connect->getAll( $sql );
    }
    /**
     * возвращает все данные пользователя, кроме hash
     *
     * @param $uid int
     *
     * @return array
     * */
    public function getUserState( $uid ){
        $sql = "select u_id as id,u_login as name, ifnull(u_win, 0) as win, ifnull(u_lose,0) as lose, 
ifnull(u_hint,0) as hint from tbl_users where u_id=?i";
        return $this->connect->getRow( $sql, $uid );
    }
    /**
     * возвращает номер и название выбранного оружия игрока
     *
     * @param $uid
     *
     * @return array
     * */
    public function getMove( $uid ){
        $sql = "select mv_wid as choice, (case mv_wid when 1 then 'rock' when 2 then 'paper' when 3 then 'scissors' 
when 4 then 'lizard' when 5 then 'spock' else null end) as message from tbl_move where mv_pl=?i";
        return $this->connect->getRow($sql, $uid);
    }
    /**
     * возвращает номер выбранного оружия игрока
     *
     * @param $uid int
     *
     * @return int
     * */
    public function getChoice( $uid ){
        $sql = "select mv_wid from tbl_move where mv_pl=?i";
        return (int)$this->connect->getOne($sql, $uid);
    }
    /**
     * возвращает имя игрока по его номеру
     *
     * @param $uid int
     *
     * @return string
     * */
    public function getNameByID( $uid ){
        $sql = "select u_login from tbl_users where u_id=?i";
        return $this->connect->getOne($sql, $uid);
    }
    /**
     * пишет историю, каждому игроку свою
     *
     * @param $uid int
     * @param $gid int
     * @param $event string
     *
     * @return bool
     * */
    public function addWarLog( $uid, $gid, $event ){
        $sql = "insert LOW_PRIORITY ignore into tbl_warlog (wl_pid, wl_gid, wl_event, wl_time) values (?i, ?i, ?s, now())";
        return $this->connect->query($sql, $uid, $gid, $event);
    }

    /**
     * отдает историю игры, для пользователя
     *
     * @param $uid int
     *
     * @return array
     * */
    public function getWarLog( $uid){
        $sql = "select wl_id as id, wl_event as event, wl_gid as gid,
DATE_FORMAT(wl_time, \"%H:%i:%s\") as time from tbl_warlog where wl_pid=?i order by wl_id";
        return $this->connect->getInd('id', $sql, $uid);
    }
    /**
     * удаляет историю игры, для пользователя
     *
     * @param $uid int
     *
     * @return bool
     * */
    public function purgeWarLog( $uid ){
        $sql = "delete from tbl_warlog where wl_pid=?i";
        return $this->connect->query($sql, $uid);
    }

    /**
     * извлекает противника из tbl_hist_game
     *
     * @param $uid int
     * @param $gid int
     *
     * @return string
     * */
    public function getEnemyFromHist( $uid, $gid ){
        $sql = "select u_login from tbl_users where u_id=(select hgm_pl_1 as pl from tbl_hist_game where hgm_id=?i and hgm_pl_1<>?i
union select hgm_pl_2 as pl from tbl_hist_game where hgm_id=?i and hgm_pl_2<>?i)";
        return $this->connect->getOne($sql, $gid, $uid, $gid, $uid);
    }

    /**
     * извлекает результата игры из tbl_hist_game
     *
     * @param $gid int
     *
     * @return array
     * */
    public function getResultFromHist( $gid ){
        $sql = "select (select u_login from tbl_users where u_id=hgm_winner) as winner,
 (select u_login from tbl_users where u_id=hgm_loser) as loser from tbl_hist_game where hgm_id=?i";
        return $this->connect->getRow($sql, $gid);
    }

}
