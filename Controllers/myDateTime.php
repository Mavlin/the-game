<?php
namespace Controllers;

class MyDateTime extends \DateTime
{
    public function setTimezoneFromString ($timezone)
    {
        $tz = new \DateTimeZone($timezone);
        return $this->setTimezone($tz);
    }

    public function formatDB() // запись в БД в формате MySQL
    {
        $this->setTimezoneFromString('UTC');
        return $this->format('Y-m-d H:i:s');
    }

    public static function createFromDB($datetime) // Приведение возвращаемых значений типа DateTime
    {
        $date = new MyDateTime($datetime . ' UTC');
        $date->setTimezoneFromString(date_default_timezone_get());
        return $date;
    }
}

// приоритеты в выборе зоны
//Значение, установленное функцией date_default_timezone_set().
//До PHP 5.4: значение переменной окружения TZ.
//Значение ini опции date.timezone.
//До PHP 5.4: запрос информации у операционной системы (если это поддерживается ОС) с использованием алгоритма для определения названия временной зоны. На этом этапе выдается предупреждение.
//Берется временная зона UTC.