<?php

namespace Controllers;
/* содержит коды ответов
 * */

trait codeState
{
    public $success = [
        'code' => 0,
        'message' => 'Все идет по плану :-))'
    ];

    public $PassNotMatch = [
        'code' => 31,
        'message' => 'passwords did not match'
    ];
    public $SomethingWrong = [
        'code' => 41,
        'message' => 'Something went wrong'
    ];
    public $sseOff = [ // SSE off
        'code' => 103,
        'message' => 'SSE off'
    ];
    public $userRegister = [ // user registered
        'code' => 101
    ];
    public $userNotAuthorized = [
        'code' => 1,
        'message' => 'Пользователь не авторизован'
    ];
    public $WrongUserName = [
        'code' => 3,
        'message' => 'Invalid username and / or password'
    ];
    public $UserExist = [
        'code' => 6,
        'message' => 'A user with that name already exists'
    ];
    public $LobbyExist = [
        'code' => 61,
        'message' => 'A lobby with that name already exists'
    ];
    public $LobbyIsBusy = [
        'code' => 62,
        'message' => 'The lobby is busy, select other place, please'
    ];
    public $LobbyNotExist = [
        'code' => 63,
        'message' => 'A lobby with that name not exists'
    ];
    public $AlreadyInLobby = [
        'code' => 64,
        'message' => 'An user is already in a lobby'
    ];
    public $UserNotInLobby = [
        'code' => 65,
        'message' => 'The user is not in the lobby'
    ];
    public $GameOver = [
        'code' => 67,
        'message' => 'Game is over'
    ];
    public $noField = [
        'code' => 9,
        'message' => "Required field is empty: "
    ];
    public $penaltyNotExpire = [
        'code' => 68,
        'message' => "Penalty did not expire"
    ];
    public $columnNotExist = [
        'code' => 32,
        'message' => "Column not exist"
    ];
    public function dataNotValid( $key )
    {
        return [
            'code' => 30, //
            'message' => 'field ' . $key . ' not valid'
        ];
    }
    public function Weapons( $key ){
        $name = '';
        switch ($key){
            case 1:
                $name = 'Rock';
                break;
            case 2:
                $name = 'Paper';
                break;
            case 3:
                $name = 'Scissors';
                break;
            case 4:
                $name = 'Lizard';
                break;
            case 5:
                $name = 'Spock';
                break;
        }
        return $name;
    }

    public function getDescRound( $k1, $k2 ){
        $res = [
            1 => 'Paper covers Rock',
            2 => 'Rock crushes Scissors',
            3 => 'Rock crushes Lizard',
            4 => 'Spock vaporizes Rock',
            5 => 'Scissors cuts Paper',
            6 => 'Lizard eats Paper',
            7 => 'Paper disproves Spock',
            8 => 'Scissors decapitates Lizard',
            9 => 'Spock smashes Scissors',
            10 => 'Lizard poisons Spock'
        ];
        if ($k1 !== $k2 ) {
            if ($k1 === 1) {
                if ($k2 === 2)
                    return $res[1];
                if ($k2 === 3)
                    return $res[2];
                if ($k2 === 4)
                    return $res[3];
                if ($k2 === 5)
                    return $res[4];
            }
            if ($k1 === 2) {
                if ($k2 === 1)
                    return $res[1];
                if ($k2 === 3)
                    return $res[5];
                if ($k2 === 4)
                    return $res[6];
                if ($k2 === 5)
                    return $res[7];
            }
            if ($k1 === 3) {
                if ($k2 === 1)
                    return $res[3];
                if ($k2 === 2)
                    return $res[5];
                if ($k2 === 4)
                    return $res[8];
                if ($k2 === 5)
                    return $res[9];
            }
            if ($k1 === 4) {
                if ($k2 === 1)
                    return $res[3];
                if ($k2 === 2)
                    return $res[6];
                if ($k2 === 3)
                    return $res[8];
                if ($k2 === 5)
                    return $res[10];
            }
            if ($k1 === 5) {
                if ($k2 === 1)
                    return $res[4];
                if ($k2 === 2)
                    return $res[7];
                if ($k2 === 3)
                    return $res[9];
                if ($k2 === 4)
                    return $res[10];
            }
        }
        return 'No winner this round - dead heat';
    }

}
